
# Prossi Clinic

Project ini menggunakan **PHP 7.4**, Codeigniter 3.1.13 dan MySQL

## Deployment Pada Hosting atau PC Lokal di Xampp

Untuk instalasi, rename file `env-example.php` menjadi `env.php`, lalu atur konfigurasi sesuai dengan environment yang ada.

Untuk `enableCache` pada `env.php` digunakan untuk caching semua tampilan frontend setelah pertama kali diakses, untuk clean cachenya silahkan akses url 'http://localhost/prossi/cron/deleteCache'. **Tidak disarankan** menggunakan cache saat development. Cache juga akan dibersihkan saat CRUD berlangsung pada halaman admin.

Untuk `baseUrl` dan `assetsUrl` pada `env.php` diisi dengan URL website yang ingin dituju, jika domainnya adalah `prossiclinic.com`, maka isi dengan `https://prossiclinic.com`, jika di local maka sesuaikan dengan folder htdocs di pc anda.

Untuk dump database terdapat pada folder database

Untuk file-file yang di upload akan di simpan pada folder assets/uploads

Untuk dump file gambar (dalam format zip) yang sudah di backup terdapat pada folder

1. assets/uploads
2. assets/xdfxcd/vendor/kcfinder

Untuk kedua file tersebut tinggal di extract saja


## User untuk login

Akses untuk login gunakan :
- Username : zul
- Password : admin

## Detail tabel t_gambar

t_gambar digunakan untuk bermacam2 data seperti anggota, testimoni, dll disimpan dalam satu tabel, 

pada tabel t_gambar terdapat kolom `tipe` dan berikut detail nya

1 : UNTUK DATA BANNER HOME

2 : UNTUK DATA TENTANG KAMI

3 : UNTUK DATA ANGGOTA

4 : UNTUK DATA TESTIMONI

5 : UNTUK DATA KENAPA PILIH KAMI

jadi hanya seperti CRUD biasa tetapi yang membedakan adalah **tipe** nya, digunakan untuk bagian di list di atas


