<?php
defined('myEnv') OR exit('No direct script access allowed');
$GLOBALS['env'] = array (
	'baseUrl' => 'http://localhost/prossi',// for url of the website
	'projectType' => 'development',// development/production
	'assetsUrl' => 'http://localhost/prossi',//for admin assets url
	'enableCache' => 1,// 1 or 0, if enabled it's caching all the front page
	'enableCsrf' => TRUE,
	'enableLog' => 1,// 1 or 0, write error logs to application/logs

	'database' => 'prossi',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',

	'playYoutube' => 1,
);
?>
