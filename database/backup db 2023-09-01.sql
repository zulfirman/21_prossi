-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 01, 2023 at 03:22 PM
-- Server version: 10.6.14-MariaDB-cll-lve
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `n1008575_prossi`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_kategori`
--

CREATE TABLE `m_kategori` (
  `KodeKategori` varchar(15) NOT NULL,
  `KodeKategoriParent` varchar(15) DEFAULT NULL,
  `MainImage` varchar(155) NOT NULL,
  `NamaKategori` varchar(40) DEFAULT NULL,
  `NamaKategoriId` varchar(40) DEFAULT NULL,
  `NamaKategoriEn` varchar(40) DEFAULT NULL,
  `DeskripsiId` varchar(255) DEFAULT NULL,
  `DeskripsiEn` varchar(255) DEFAULT NULL,
  `IsService` smallint(11) DEFAULT 0,
  `IsActive` tinyint(1) NOT NULL DEFAULT 1,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` varchar(50) DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `m_kategori`
--

INSERT INTO `m_kategori` (`KodeKategori`, `KodeKategoriParent`, `MainImage`, `NamaKategori`, `NamaKategoriId`, `NamaKategoriEn`, `DeskripsiId`, `DeskripsiEn`, `IsService`, `IsActive`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`) VALUES
('20357323decbf9d', NULL, '', NULL, 'Olahraga', 'Sports', NULL, NULL, 0, 1, NULL, '2021-04-27 10:01:35', NULL, 'zul'),
('206d33f1a06743e', NULL, '', NULL, 'Kencantikan', 'Beauty', NULL, NULL, 0, 1, NULL, '2021-04-27 10:01:18', NULL, 'zul'),
('21163a2dc41bdb2', '21f2862b7bcce43', 'default.jpg', 'hydrating', 'Hydrating', '', NULL, NULL, 2, 1, '2021-09-23 06:43:49', '2021-09-26 14:28:56', 'inessytika', 'farizan'),
('211aa25cb77ada2', '216b5fa6215cbf1', 'default.jpg', 'profood', 'ProFood', '', NULL, NULL, 2, 1, '2021-09-26 14:26:01', NULL, 'farizan', NULL),
('214fb7f129f8eac', '21f2862b7bcce43', 'default.jpg', 'brightening/pigmentation', 'Brightening/Pigmentation', '', NULL, NULL, 2, 1, '2021-09-23 06:43:34', '2021-09-26 14:31:10', 'inessytika', 'farizan'),
('2153fa16746368c', '21f2862b7bcce43', 'default.jpg', 'removal', 'Removal', '', NULL, NULL, 2, 1, '2021-09-23 06:43:56', '2021-09-26 14:30:53', 'inessytika', 'farizan'),
('21688cefce0c091', '216b5fa6215cbf1', 'default.jpg', 'fitgenme', 'FitGenMe', '', NULL, NULL, 2, 1, '2021-09-26 14:26:48', NULL, 'farizan', NULL),
('216b5fa6215cbf1', NULL, '21d37044633e005.gif', 'slimming-program', 'Slimming Program', 'Facial Reshaping', 'Solusi menurunkan berat badan sesuai kebutuhan tubuh yang dilengkapi dengan suplemen dan katering sehat. Konsultasi langsung dengan dokter SpGK.', 'Menjadikan wajah kencang, mengatasi keriput/kerutan, serta membentuk kontur wajah.', 1, 1, '2021-05-01 12:53:31', '2021-09-18 10:17:40', 'zul', 'inessytika'),
('2170ae61421bd56', '21f2862b7bcce43', 'default.jpg', 'scar', 'Scar', '', NULL, NULL, 2, 1, '2021-09-23 06:43:25', '2021-09-26 14:29:55', 'inessytika', 'farizan'),
('21861ed335d27ea', '21f2862b7bcce43', 'default.jpg', 'countouring', 'Countouring', '', NULL, NULL, 2, 1, '2021-09-23 06:43:41', '2021-09-26 22:41:49', 'inessytika', 'inessytika'),
('21ab673f423c2c8', NULL, '21392a9926ff05b.jpg', NULL, 'Facial Reshaping', 'Facial Reshaping', NULL, NULL, 1, 0, '2021-04-27 10:18:23', '2021-04-30 11:45:14', 'zul', 'zul'),
('21b7f27b55df073', '21f2862b7bcce43', 'default.jpg', 'acne', 'Acne', '', NULL, NULL, 2, 1, '2021-09-21 10:15:20', '2021-09-26 13:35:29', 'zul', 'farizan'),
('21cdee524988c3d', '216b5fa6215cbf1', 'default.jpg', 'proslim', 'ProSlim', '', NULL, NULL, 2, 1, '2021-09-26 14:25:31', NULL, 'farizan', NULL),
('21e61bceb35dbdc', '216b5fa6215cbf1', 'default.jpg', 'meso-super', 'Meso Super', '', NULL, NULL, 2, 1, '2021-09-26 14:25:43', NULL, 'farizan', NULL),
('21e64f9536f34e0', NULL, '', NULL, 'Kesehatan', 'Health', NULL, NULL, 0, 1, NULL, '2021-04-27 10:01:25', NULL, 'zul'),
('21f0b7d29a3528e', '216b5fa6215cbf1', 'default.jpg', 'fat-burn-treatment-package-(ftp)', 'Fat-Burn Treatment Package (FTP)', '', NULL, NULL, 2, 1, '2021-09-26 14:25:52', NULL, 'farizan', NULL),
('21f2862b7bcce43', NULL, '21d930a83c08e39.gif', 'skin-treatments', 'Skin Treatments', 'Rejuvenation', 'Solusi untuk berbagai permasalahan kulit. Dilengkapi dengan peralatan canggih seperti IPL, Laser, HIFU, dll. Ditangani langsung oleh dokter SpKK', 'Bertujuan regenerasi sel, meremajakan, melembapkan dan memperbaiki tekstur kulit.', 1, 1, '2021-05-01 12:53:48', '2021-09-18 10:17:49', 'zul', 'inessytika'),
('21f67ae2ef655d4', '21f2862b7bcce43', 'default.jpg', 'rejuvenation', 'Rejuvenation', '', NULL, NULL, 2, 1, '2021-09-21 10:15:03', '2021-09-23 06:42:14', 'zul', 'inessytika'),
('promo', NULL, '', NULL, 'Promo', 'Promo', NULL, NULL, 0, 1, '2021-04-28 08:50:37', NULL, 'zul', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_kategori_user`
--

CREATE TABLE `m_kategori_user` (
  `KodeKategoriUser` varchar(15) NOT NULL,
  `NamaKategoriUser` varchar(155) NOT NULL,
  `TipeUser` tinyint(1) NOT NULL,
  `Trustee` text NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT 1,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` varchar(50) DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `m_kategori_user`
--

INSERT INTO `m_kategori_user` (`KodeKategoriUser`, `NamaKategoriUser`, `TipeUser`, `Trustee`, `IsActive`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`) VALUES
('21128c10473169e', 'Superadmin', 1, '0012,,0006,0004,0010,0001,,0014,,0009,0007,0015,,0008,0005,,,0011,0013,0002,0003', 1, '2021-02-01 18:53:40', '2021-09-21 10:13:10', 'zul', 'zul'),
('218660df7c5b288', 'Admin', 1, '0012,,0006,0004,0010,0001,,0014,,0009,0007,0015,,0008,0005,,,0011,0013,0002', 1, '2021-05-19 20:33:33', '2021-09-21 10:13:07', 'farizan', 'zul');

-- --------------------------------------------------------

--
-- Table structure for table `m_navigasi`
--

CREATE TABLE `m_navigasi` (
  `KodeNavigasi` int(11) NOT NULL,
  `NamaNavigasi` varchar(155) NOT NULL,
  `Url` varchar(155) NOT NULL,
  `ParentId` varchar(2) NOT NULL,
  `Icon` varchar(155) NOT NULL,
  `Trustee` varchar(155) NOT NULL,
  `Sort` varchar(2) DEFAULT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `TipeNavigasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `m_navigasi`
--

INSERT INTO `m_navigasi` (`KodeNavigasi`, `NamaNavigasi`, `Url`, `ParentId`, `Icon`, `Trustee`, `Sort`, `IsActive`, `TipeNavigasi`) VALUES
(1, 'User', '', '', 'fal fa-bolt', '', '3', 1, 1),
(2, 'Data User', 'admin/master-user', '1', '', '0002', NULL, 1, 1),
(3, 'Kategori User', 'admin/master-kategori-user', '1', '', '0003', NULL, 1, 1),
(4, 'Dashboard', 'admin/dashboard', '', 'fal fa-globe', '0001', '1', 1, 1),
(5, 'Artikel', '', '', 'fal fa-book', '', '2', 1, 1),
(6, 'List Artikel', 'admin/artikel', '5', '', '0004', '1', 1, 1),
(7, 'Promo', 'admin/promo', '', 'fal fa-money-bill-alt', '0005', '2', 1, 1),
(8, 'List Pelayanan', 'admin/service', '21', '', '0007', NULL, 1, 1),
(9, 'Kategori Artikel', 'admin/master-kategori', '5', '', '0006', '2', 1, 1),
(10, 'Pengaturan', 'admin/pengaturan', '', 'fal fa-bolt', '0008', '10', 1, 1),
(11, 'Kategori', 'admin/kategori-service', '21', '', '0009', NULL, 1, 1),
(12, 'Home', 'home', '', '', '', '1', 1, 2),
(13, 'Our Doctors', 'tentang-kami', '', '', '', '5', 1, 2),
(14, 'Banner Homepage', 'admin/banner-homepage', '', 'fal fa-bolt', '0010', '1', 1, 1),
(15, 'Tentang Kami', 'admin/tentang-kami', '', 'fal fa-info-circle', '0011', '1', 1, 1),
(16, 'Anggota', 'admin/anggota', '', 'fal fa-universal-access', '0012', '1', 1, 1),
(17, 'Contact Us', 'kontak', '', '', '', '6', 1, 2),
(18, 'Testimoni', 'admin/testimoni', '', 'fal fa-comments', '0013', '1', 1, 1),
(19, 'Promo', 'promo', '', '', '', '2', 1, 2),
(20, 'Articles', 'artikel', '', '', '', '4', 1, 2),
(21, 'Layanan', '', '', 'fal fa-hand-receiving', '', '2', 1, 1),
(22, 'Kenapa Pilih Kami', 'admin/kenapa-pilih-kami', '', 'fal fa-question', '0014', '2', 1, 1),
(23, 'Services', 'pelayanan', '', '', '', '3', 1, 2),
(24, 'Sub Kategori', 'admin/sub-kategori-service', '21', '', '0015', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_pengaturan`
--

CREATE TABLE `m_pengaturan` (
  `KodePengaturan` varchar(15) NOT NULL,
  `NamaWebsite` varchar(50) NOT NULL,
  `TentangId` text NOT NULL,
  `TentangEn` text NOT NULL,
  `Kontak` text NOT NULL,
  `Alamat` text NOT NULL,
  `JamBuka` text NOT NULL,
  `NomorTelepon` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `Email` varchar(60) NOT NULL,
  `Logo` varchar(20) NOT NULL,
  `Icon` varchar(20) NOT NULL,
  `BackgroundHeader` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`BackgroundHeader`)),
  `BackgroundFooter` longtext NOT NULL,
  `Facebook` varchar(155) NOT NULL,
  `Instagram` varchar(155) NOT NULL,
  `Twitter` varchar(155) NOT NULL,
  `Map` text NOT NULL,
  `WarnaTema` varchar(20) NOT NULL,
  `EnableMultiLanguage` tinyint(1) NOT NULL,
  `DateUpdated` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `m_pengaturan`
--

INSERT INTO `m_pengaturan` (`KodePengaturan`, `NamaWebsite`, `TentangId`, `TentangEn`, `Kontak`, `Alamat`, `JamBuka`, `NomorTelepon`, `Email`, `Logo`, `Icon`, `BackgroundHeader`, `BackgroundFooter`, `Facebook`, `Instagram`, `Twitter`, `Map`, `WarnaTema`, `EnableMultiLanguage`, `DateUpdated`, `UpdatedBy`) VALUES
('1', 'Prossi Clinic | Skin, Slimming, and Beauty ', '<p>-</p>', '<p>-</p>', '<p>Email : info@prossiclinic.com</p>\r\n\r\n<p>Phone : <a href=\"tel:+62 856 9338 5800\">+62 821&nbsp;3009&nbsp;1003</a></p>\r\n', '<div class=\"table-responsive\">\r\n	<table class=\"table table-bordered\" style=\"width:100%\">\r\n		<tbody>\r\n			<tr>\r\n				<td>\r\n					<p style=\"text-align:center\"><strong>Prossi Clinic - Tebet</strong><br>\r\n						<span style=\"font-size:11px\">Jl, Lapangan Ros Selatan No.16, RT.5/RW.1, East Tebet, Tebet, South Jakarta City, Jakarta 12820</span><br>\r\n						<a href=\"https://maps.app.goo.gl/zkPkrpRaHWWQkMjG6\">Cek Lokasi</a></p>\r\n				</td>\r\n				<td>\r\n					<p style=\"text-align:center\"><strong>Prossi Clinic - Kebayoran Lama</strong><br>\r\n						<span style=\"font-size:11px\">Jl. Ciputat Raya No.5, RT.9/RW.8, Kby. Lama Utara, Kec. Kby. Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12310</span><br>\r\n						<a href=\"https://maps.app.goo.gl/SKyb42AhW2FggDjQ8\">Cek Lokasi</a></p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<p style=\"text-align:center\"><strong>Prossi Clinic - Bintaro</strong><br>\r\n						<span style=\"font-size:11px\">Jalan Cut Mutia 2 FG-2 No. 42A, Bintaro Sektor 7, Jurang Mangu Barat, Pondok Aren, Jurang Mangu Barat, Kec. Pd. Aren, Kota Tangerang Selatan, Banten 15222</span><br>\r\n						<a href=\"https://maps.app.goo.gl/vB3ixYnFp4fwymYx9\">Cek Lokasi</a></p>\r\n				</td>\r\n				<td>\r\n					<p style=\"text-align:center\"><strong>Prossi Clinic - Bekasi</strong><br>\r\n						<span style=\"font-size:11px\">Taman Galaxy Ruko Atlantis Blok AE, Jl. Pulo Sirih Utama No. 141B, Pekayon Jaya, Bekasi Selatan, Jawa Barat 17148</span><br>\r\n						<a href=\"https://maps.app.goo.gl/FWEQNG6EkbLqRbmC9\">Cek Lokasi</a></p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td colspan=\"2\">\r\n					<p style=\"text-align:center\"><strong>Prossi Clinic Malang</strong><br>\r\n						<span style=\"font-size:11px\">Jl. Buring No. 25, Oro-oro Dowo, Klojen, Malang</span><br>\r\n						<a href=\"https://maps.app.goo.gl/G4NViTH3zVcx623r7\">Cek Lokasi</a></p>\r\n				</td>\r\n			</tr>\r\n		</tbody>\r\n	</table>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n', '<div class=\"table-responsive\">\r\n	<p><strong>Senin - Minggu</strong><br>\r\n		pkl. 09.30 - 17.00 WIB</p>\r\n</div>\r\n', '<p>Daftar Kontak :&nbsp;</p>\r\n\r\n<table class=\"table table-bordered table-hover\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td><span style=\"color:#000000; font-family:Arial\"><span style=\"font-size:14.6667px\">CS Prossi Clinic</span></span></td>\r\n			<td>\r\n				<p>&nbsp;<span style=\"font-size:14px\"><a href=\"https://wa.link/536ben\" target=\"_blank\"><span style=\"color:#000000\">Klik Disini</span></a></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'prossiclinictbt@gmail.com', '2175fd91343ddf7.png', '2175fd91343ddf7.png', '{\"bPelayanan\":\"21270f02847718d.gif\",\"bPromo\":\"211517134fd1bc1.gif\",\"bArtikel\":\"211d43485710c55.gif\",\"bTentang\":\"2113e94c97ca9f0.gif\",\"bKontak\":\"218077743ad5f25.gif\"}', '{\"tPelayanan\":\"<h2><strong>Layanan Kami<\\/strong><\\/h2>\\r\\n\\r\\n<p>Temukan perawatan yang sesuai dengan kebutuhanmu<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\",\"tPromo\":\"<h2><strong>SPECIAL FOR YOU<\\/strong><\\/h2>\\r\\n\\r\\n<p>Temukan promo spesial untuk perawatan yang kamu butuhkan<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\",\"tArtikel\":\"<h2><strong>KEEPING UP WITH PROSSI<\\/strong><\\/h2>\\r\\n\\r\\n<p>Temukan berbagai artikel menarik seputar kesehatan dan kecantikan<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\",\"tTentang\":\"<h2><strong>WE&#39;RE HERE FOR YOU<\\/strong><\\/h2>\\r\\n\\r\\n<p>Kami siap membantu masalah kulit dan berat&nbsp;badanmu. Buat janji dengan doktermu sekarang!<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\",\"tKontak\":\"<h2><strong>ANYTIME FOR YOU<\\/strong><\\/h2>\\r\\n\\r\\n<p>Jangan ragu untuk menghubungi kami<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\"}', 'Prossi-Clinic-Tebet-107588878220638', 'prossi.clinic', 'prossiclinic', '<iframe width=\"100%\" height=\"600\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Jl.%20MH.%20Thamrin%20Kav.%203%20Kebon%20Sirih%20Menteng%20Jakarta%20Pusat%20DKI%20Jakarta%20RT.2,%20RT.8/RW.1,%20Kb.%20Sirih,%20Kec.%20Menteng,%20Kota%20Jakarta%20Pusat,%20Daerah+(PROTECT)&amp;t=&amp;z=16&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe><a href=\"https://www.maps.ie/route-planner.htm\">Driving Route Planner</a>', '#0033FF', 1, '2023-03-06 11:54:45', 'farizan');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `KodeUser` varchar(50) NOT NULL,
  `MainImage` varchar(20) NOT NULL,
  `Username` varchar(65) NOT NULL,
  `NamaUser` varchar(40) NOT NULL,
  `Password` varchar(35) NOT NULL,
  `KodeKategoriUser` varchar(15) NOT NULL,
  `AboutMeId` text NOT NULL,
  `AboutMeEn` text DEFAULT NULL,
  `Pekerjaan` varchar(50) NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT 1,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` varchar(50) DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`KodeUser`, `MainImage`, `Username`, `NamaUser`, `Password`, `KodeKategoriUser`, `AboutMeId`, `AboutMeEn`, `Pekerjaan`, `IsActive`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`) VALUES
('farizan', '21050889c24cee1.png', 'farizan@itik.co.id', 'Farizan Ramadhan', '10f9914634eff386f237777a441f0d76', '21128c10473169e', '-', '', 'IT Consultant', 1, '2021-05-18 02:12:07', NULL, 'zul', ''),
('inessytika', 'default.jpg', 'inessytika@gmail.com', 'Inessy TIka', '39b7c6f73de4e453520673c7919c143d', '21128c10473169e', 'PROSSI', '', 'PROSSI', 1, '2021-05-19 20:35:49', '2021-09-11 19:11:28', 'farizan', 'farizan'),
('melyarna.putri', 'default.jpg', 'melyarna.putri@gmail.com', 'Melyarna Putri', '39b7c6f73de4e453520673c7919c143d', '218660df7c5b288', '-', '', '-', 1, '2021-05-19 20:37:25', NULL, 'farizan', ''),
('zul', '2128df3f8a286df.jpg', 'zul', 'Zulfirman', '0c7540eb7e65b553ec1ba6b20de79608', '21128c10473169e', 'A free, once–weekly round-up of responsive design articles, tools, tips, tutorials and inspirational links.\r\n\r\nWe spend hours curating the best content, interview industry leaders and send it to you every Friday.', '', 'Apa Aja Boleh', 1, NULL, '2021-05-24 11:17:03', NULL, 'zul');

-- --------------------------------------------------------

--
-- Table structure for table `t_anggota`
--

CREATE TABLE `t_anggota` (
  `KodeAnggota` varchar(15) NOT NULL,
  `MainImage` varchar(20) NOT NULL,
  `NamaAnggota` varchar(100) NOT NULL,
  `Jabatan` varchar(255) NOT NULL,
  `Urutan` int(11) NOT NULL,
  `TempatLahir` varchar(50) DEFAULT NULL,
  `TanggalLahir` date DEFAULT NULL,
  `EmailAnggota` varchar(70) DEFAULT NULL,
  `Quote` varchar(255) DEFAULT NULL,
  `DateAdded` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_artikel`
--

CREATE TABLE `t_artikel` (
  `KodeArtikel` varchar(15) NOT NULL,
  `UrlArtikel` varchar(155) NOT NULL,
  `JudulId` varchar(255) NOT NULL,
  `JudulEn` varchar(255) NOT NULL,
  `MainImage` varchar(20) NOT NULL,
  `ContentId` text NOT NULL,
  `ContentEn` text NOT NULL,
  `KodeKategori` varchar(100) NOT NULL,
  `EnableComment` tinyint(1) NOT NULL,
  `Tipe` smallint(6) NOT NULL,
  `Lainnya1` varchar(255) DEFAULT NULL,
  `Lainnya2` text DEFAULT NULL,
  `Lainnya3` varchar(255) DEFAULT NULL,
  `Lainnya4` text DEFAULT NULL,
  `DateCreated` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `t_artikel`
--

INSERT INTO `t_artikel` (`KodeArtikel`, `UrlArtikel`, `JudulId`, `JudulEn`, `MainImage`, `ContentId`, `ContentEn`, `KodeKategori`, `EnableComment`, `Tipe`, `Lainnya1`, `Lainnya2`, `Lainnya3`, `Lainnya4`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`) VALUES
('2103644fd157724', 'selain-sunscreen--warna-baju-juga-bisa-melindungi-anda-dari-sinar-uv', 'Selain Sunscreen, Warna Baju Juga Bisa Melindungi Anda Dari Sinar UV', 'Coronavirus disease COVID-19 pandemic', '2164e5be9a8cc33.gif', '<p>Melindungi kulit bisa dengan cara memilih pakaian yang tepat.</p>\r\n\r\n<p>Ya, pakaian pun bisa melindungi Anda dari bahaya sinar UV.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Sinar UV, baik UVA maupun UVB dapat membuat kulit kusam, terlihat lebih gelap, berkerut, timbul bercak kehitaman, hingga menyebabkan kanker kulit.</p>\r\n\r\n<p>Oleh sebab itu, selain menggunakan sunscreen, Anda bisa melindungi kulit dari paparan sinar UV dengan cara:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Menggunakan Pakaian Dengan UPF</strong></p>\r\n\r\n<p>Ultraviolet Protection Factor (UPF) merupakan pengukuran yang dilakukan untuk menilai kemampuan suatu produk garmen dalam melindingi dari sinar ultraviolet .</p>\r\n\r\n<p>Kemampuannya untuk memantulkan sinar UVB lebih baik dibandingkan dengan UVA.</p>\r\n\r\n<p>Di Amerika Serikat, garmen digolongkan memiliki perlindungan baik (UPF 15-24), sangat baik (UPF 25-39) dan luar biasa (UPF 40-50+).</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Menggunakan Pakaian Yang Berwarna</strong></p>\r\n\r\n<p>Berdasarkan jurnal Industrial &amp; Engineering Chemistry&nbsp; Research,&nbsp;<strong>&nbsp;pakaian merah&nbsp; memberikan perlindungan lebih maksimal dari bahaya sinar matahari daripada warna kuning atau ungu.</strong></p>\r\n\r\n<p>Hal ini tergantung jenis pewarna yang digunakan.</p>\r\n\r\n<p>Pakaian basah (terutama putih) pun cenderung menurunkan UV proteksinya.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Sumber:</strong></p>\r\n\r\n<p><a href=\"https://instagram.com/perdoski.id?utm_source=ig_profile_share&amp;igshid=1avcyhyfugl5s\">https://instagram.com/perdoski.id</a></p>\r\n\r\n<p>(Perhimpunan Dokter Spesialis Kulit dan Kelamin Indonesia)</p>\r\n', '<p>The coronavirus COVID-19 pandemic is the defining global health crisis of our time and the greatest challenge we have faced since World War Two. Since its emergence in Asia in 2019, the virus has spread to&nbsp;<a href=\"https://www.undp.org/content/undp/en/home/covid-19-pandemic-response.html#covid19dashboard\">every continent</a>&nbsp;except Antarctica.</p>\r\n\r\n<p>We have now reached the tragic milestone of more than two million deaths, and the human family is suffering under an almost intolerable burden of loss.</p>\r\n\r\n<p>&ldquo;The climbing death toll is staggering, and we must work together to slow the spread of this virus.&rdquo; - UNDP Administrator Achim Steiner.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img class=\"img-responsive lazy\" loading=\"lazy\" src=\"https://www.undp.org/content/dam/undp/img/stories/coronavirus/microscope.svg\" style=\"float:left; width:150px\">But the pandemic is much more than a health crisis, it&#39;s also an unprecedented socio-economic crisis. Stressing every one of the countries it touches, it has the potential to create devastating social, economic and political effects that will leave deep and longstanding scars. UNDP is the technical lead in the UN&rsquo;s socio-economic recovery, alongside the health response, led by WHO, and the Global Humanitarian Response Plan, and working under the leadership of the UN Resident Coordinators.</p>\r\n\r\n<p>Every day, people are losing jobs and income, with no way of knowing when normality will return. Small island nations, heavily dependent on tourism, have empty hotels and deserted beaches.&nbsp;<a href=\"https://www.ilo.org/global/about-the-ilo/newsroom/news/WCMS_749398/lang--en/index.htm\" target=\"_blank\">The International Labour Organization</a>&nbsp;estimates that 400 million jobs could be lost.<a id=\"undpresponse\"></a></p>\r\n\r\n<p>The World Bank projects a US$110 billion decline in remittances this year, which could mean 800 million people will not be able to meet their basic needs.</p>\r\n', '206d33f1a06743e', 1, 1, NULL, NULL, NULL, NULL, '2021-01-26 12:05:37', '2023-01-15 15:23:09', 'zul', 'zul'),
('2103e3919d4c48d', 'chemical-peeling', 'Chemical Peeling', '', 'default.jpg', 'Rasakan kulit wajah cantik bersinar dan lebih cerah', '', '214fb7f129f8eac', 0, 3, '21378de8f6ca713.gif', '<p><span style=\"color:#000000\">Tindakan Peeling adalah proses pengelupasan kulit dengan bantuan larutan kimia berbentuk cairan yang dioleskan pada permukaan kulit. Akibat dari pengaplikasian cairan kimia ini, lapisan kulit yang mati akan luruh (ter eksfoliasi) dan kulit baru yang muda pun akan terlihat (rejuvenasi/peremajaan kulit). Proses ini merupakan salah satu cara tercepat untuk mendapatkan tampilan kulit yang ideal dan dapat digunakan untuk mengatasi masalah jerawat ringan, flek/hiperpigmentasi, skar, dan penuaan kulit.</span></p>\r\n', '217d8b76b9e15f5.gif', '<p><span style=\"color:#000000\">Untuk treatment Peeling yang terdapat di Prossi Clinic ada dua jenis Peeling, yaitu:</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">Peeling Radiant Glow</span></strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">Peeling Platinum</span></strong></span></li>\r\n</ul>\r\n', '2021-09-26 13:59:00', '2021-09-26 22:22:13', 'farizan', 'inessytika'),
('21078a7a1da2410', 'meso-super', 'Meso Super', '', 'default.jpg', 'Solusi menurunkan berat badan dan menghilangkan lemak di tubuh', '', '21e61bceb35dbdc', 0, 3, '21395fadc9ce27e.gif', 'Meso super merupakan treatment  yang efektif karena mampu membakar lemak sekaligus menahan nafsu makan sehinga dapat menunjang program diet. Prosedur treatment ini adalah dengan cara menginjeksikan serum khusus untuk penurunan berat badan ke bagian tubuh yang ditargetkan. ', '21395fadc9ce27e.gif', 'Treatment ini dapat digunakan 1-3 bulan tergantung anjuran dokter.', '2021-09-26 14:47:14', NULL, 'farizan', ''),
('21164f33b9e95e7', 'promo-happy-hour-prossi-', 'Promo Happy Hour Prossi ', '', '22fc5254f7559dc.png', '<p>Hanya Berlaku Setiap Senin-Kamis&nbsp;jam 9.30-13.00 WIB</p>\r\n\r\n<p>Kamu bisa pilih treatment berikut hanya dengan Rp200.000 aja:</p>\r\n\r\n<p>- IPL rejuvenation untuk mencerahkan dan membuat kulit glowing,</p>\r\n\r\n<p>- Peeling Radiant Glow untuk menghilangkan sel kulit mati dan membuat wajah terlihat lebih cerah dan glowing</p>\r\n\r\n<p>- Infuse whitening yang bisa mencerahkan kulitmu</p>\r\n\r\n<p>Atau kamu bisa pilih treatment berikut dengan Rp300.000 aja:</p>\r\n\r\n<p>- Brightening Laser untuk mencerahkan dan membuat wajah lebih glowing,</p>\r\n\r\n<p>- Peeling body (upper arm/lower arm/upper leg/lower leg/buttock/upper back/lower back/tummy)* yang bisa menghilangkan masalah flek hitam, jerawat, dan mencerahkan kulit mu.</p>\r\n\r\n<p>- RF (face/neck/upper arm/thigh/calf/tummy) yang bisa mengencangkan kulitmu.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"https://wa.link/c98sw0\">Reservasi Sekarang</a></p>\r\n', '', 'promo', 0, 2, '0', NULL, NULL, NULL, '2021-09-13 11:33:23', '2023-03-02 17:20:12', 'inessytika', 'inessytika'),
('2116e7e2eb80d89', 'oxygeneo', 'Oxygeneo', '', 'default.jpg', 'Rasakan sensasi segar dan bebas sakit saat perawatan pembersihan wajah', '', '214fb7f129f8eac', 0, 3, '21ee54412b9d1d6.gif', '<p>OxyGeneo adalah treatment facial super dengan teknologi inovatif yang menyediakan tiga perawatan wajah dalam satu perawatan. Tretament ini terdiri dari eksfoliasi ringan, oksigenasi kulit, dan peremajaan wajah. Teknologi OxyGeneo ini terbukti secara klinis menghasilkan nutrisi dan oksigenasi kulit terbaik, sehingga membuat kulit menjadi lebih halus dan tampak lebih muda.</p>\r\n', '21b039de7e51517.gif', '<p>Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 45 menit. Hasilnya pun dapat secara langsung terlihat setelah satu kali perawatan.</p>\r\n', '2021-09-27 09:28:12', '2021-09-27 09:32:43', 'inessytika', 'inessytika'),
('2124c608fc92ce2', 'high-intensity-focused-ultrasound-hifu', 'High Intensity Focused Ultrasound (HIFU)', '', 'default.jpg', 'Dapatkan kulit yang lebih kencang dengan hasil yang maksimal.', '', '21861ed335d27ea', 0, 3, '21ed068082eb29d.gif', '<p>HIFU merupakan singkatan dari High Intensity Focused Ultrasound atau yang lebih dikenal dengan ulteraphy. HIFU berfungsi untuk mengencangkan dan mengangkat kulit yang kendur. Manfaat lainnya dari treatment ini diantaranya: &bull; Meniruskan wajah (V shape) &bull; Menghilangkan double chin &bull; Mentimulasi kolagen &bull; Mengurangi kerutan halus (area mata, garis senyum) &bull; Meningkatkan elastisitas dan kekenyalan kulit</p>\r\n', '2115a4a0daf7163.gif', '<p>Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 60 menit. Tindakan ini dilengkapi dengan pengolesan krim anastesi untuk meningkatkan kenyamanan pasien saat treatment.</p>\r\n', '2021-09-27 09:55:07', '2021-09-27 10:05:13', 'inessytika', 'inessytika'),
('21250072cc10d17', 'proslim', 'ProSlim', '', 'default.jpg', 'Program penurunan berat badan eksklusif bersama dokter spesialis gizi klinik', '', '21cdee524988c3d', 0, 3, '219261aa840827f.gif', '<p><span style=\"color:#000000\">ProSlim adalah paket perawatan lengkap dengan jangka waktu mulai dari 3 sampai 6 bulan yang disupervisi langsung oleh dokter spesialis gizi klinik. program ini dilengkai dengan suplemen penurunan berat badan yang aman, sesi konsultasi dengan dokter spesialis gizi klinik, perawatan penurunan berat badan, hingga dilengkapi dengan pemeriksaan laboratorium untuk menentukan pola diet dan olahraga yang tepat sesuai dengan gen tubuh.</span></p>\r\n', '2151eb360584d61.gif', '<p><span style=\"color:#000000\">Ada 3 pilihan ProSlim yang tersedia, diantaranya</span></p>\r\n\r\n<ul>\r\n	<li><strong><span style=\"color:#000000\"><span style=\"line-height:3\">ProSlim Signature</span></span></strong></li>\r\n	<li><strong><span style=\"color:#000000\"><span style=\"line-height:3\">ProSlim Extra</span></span></strong></li>\r\n	<li><strong><span style=\"color:#000000\"><span style=\"line-height:3\">Private ProSlim</span></span></strong></li>\r\n</ul>\r\n', '2021-09-26 14:45:56', '2021-09-26 22:35:15', 'farizan', 'inessytika'),
('21271cb60960c30', 'chemical-peeling', 'Chemical Peeling', '', 'default.jpg', 'Rasakan kulit wajah cantik bersinar dan lebih cerah', '', '21f67ae2ef655d4', 0, 3, '219f216f318e433.gif', '<p><span style=\"color:#000000\">Tindakan Peeling adalah proses pengelupasan kulit dengan bantuan larutan kimia berbentuk cairan yang dioleskan pada permukaan kulit. Akibat dari pengaplikasian cairan kimia ini, lapisan kulit yang mati akan luruh (ter eksfoliasi) dan kulit baru yang muda pun akan terlihat (rejuvenasi/peremajaan kulit). Proses ini merupakan salah satu cara tercepat untuk mendapatkan tampilan kulit yang ideal dan dapat digunakan untuk mengatasi masalah jerawat ringan, flek/hiperpigmentasi, skar, dan penuaan kulit.</span></p>\r\n', '218a1e2d5a23ef1.gif', '<p><span style=\"color:#000000\">Untuk treatment Peeling yang terdapat di Prossi Clinic ada dua jenis Peeling, yaitu:</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:3\"><strong>Peeling Radiant Glow</strong></span></span></li>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:3\"><strong>Peeling Platinum</strong></span></span></li>\r\n</ul>\r\n', '2021-09-26 22:48:38', '2021-09-26 22:50:05', 'inessytika', 'inessytika'),
('212941e6fb1989d', 'acne-injection', 'Acne Injection', '', 'default.jpg', 'Atasi jerawat dengan cara yang cepat dan tepat', '', '21b7f27b55df073', 0, 3, '2177ffbdf55b187.gif', '<p><span style=\"color:#000000\">Tindakan injeksi yang dimaksudkan untuk mengatasi keluhan jerawat yang sedang / akan tumbuh pada satu daerah kulit tertentu, terutama wajah, atau juga membantu mengatasi keluhan jerawat yang meradang agar menjadi lekas mengering dan berangsur sembuh. Injeksi ini menghambat kerja bakteri P-Acne penyebab jerawat</span></p>\r\n', '21eac2e4262cda2.gif', '<p><span style=\"color:#000000\">Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 15 menit.</span></p>\r\n', '2021-09-26 13:34:56', '2021-09-26 22:13:59', 'farizan', 'inessytika'),
('212d1d467a6caf3', 'chemical-peeling', 'Chemical Peeling', '', 'default.jpg', 'Rasakan kulit wajah cantik bersinar dan lebih cerah', '', '2170ae61421bd56', 0, 3, '21f0c17d769eeb2.gif', '<p><span style=\"color:#000000\">Tindakan Peeling adalah proses pengelupasan kulit dengan bantuan larutan kimia berbentuk cairan yang dioleskan pada permukaan kulit. Akibat dari pengaplikasian cairan kimia ini, lapisan kulit yang mati akan luruh (ter eksfoliasi) dan kulit baru yang muda pun akan terlihat (rejuvenasi/peremajaan kulit). Proses ini merupakan salah satu cara tercepat untuk mendapatkan tampilan kulit yang ideal dan dapat digunakan untuk mengatasi masalah jerawat ringan, flek/hiperpigmentasi, skar, dan penuaan kulit.</span></p>\r\n', '219f696c19d6b79.gif', '<p>Untuk treatment Peeling yang terdapat di Prossi Clinic ada dua jenis Peeling, yaitu:</p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:3\">Peeling Radiant Glow </span></span></li>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:3\">Peeling Platinum</span></span></li>\r\n</ul>\r\n', '2021-09-26 22:51:19', '2021-09-26 22:51:53', 'inessytika', 'inessytika'),
('2131a150dae3258', 'oxygeneo', 'Oxygeneo', '', 'default.jpg', 'Rasakan sensasi segar dan bebas sakit saat perawatan pembersihan wajah', '', '21f67ae2ef655d4', 0, 3, '21bc7eff9fe6572.gif', '<p>OxyGeneo adalah treatment facial super dengan teknologi inovatif yang menyediakan tiga perawatan wajah dalam satu perawatan. Tretament ini terdiri dari eksfoliasi ringan, oksigenasi kulit, dan peremajaan wajah. Teknologi OxyGeneo ini terbukti secara klinis menghasilkan nutrisi dan oksigenasi kulit terbaik, sehingga membuat kulit menjadi lebih halus dan tampak lebih muda.</p>\r\n', '21169a97846e6c3.gif', '<p>Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 45 menit. Hasilnya pun dapat secara langsung terlihat setelah satu kali perawatan.</p>\r\n', '2021-09-27 09:30:24', '2021-09-27 09:30:51', 'inessytika', 'inessytika'),
('213506b3e20ad84', 'cauter', 'Cauter', '', 'default.jpg', 'Solusi mengatasi kelainan pada kulit', '', '2153fa16746368c', 0, 3, '21fd9087d5fcd2b.gif', '<p><span style=\"color:#000000\">Cauter adalah perawatan yang digunakan untuk menghilangkan berbagai kelainan pada kulit. Treatment ini menggunakan mesin elektrocauter yang mampu mengatasi berbagai masalah kulit seperti: titik-titik hitam pada kulit, milia, kutil, atau tahi lalat.</span></p>\r\n', '2168551cd0a8b50.gif', '<p><span style=\"color:#000000\">Treatment ini terbagi dalam 3 kategori, yaitu:</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">Cauter ringan</span></strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">Cauter sedang</span></strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">Cauter berat</span></strong></span></li>\r\n</ul>\r\n', '2021-09-26 13:40:46', '2021-09-27 09:13:42', 'farizan', 'inessytika'),
('21397b55481af58', 'infuse-whitening', 'Infuse Whitening', '', 'default.jpg', 'Dapatkan kulit yang lebih cerah dan tubuh yang lebih sehat dalam satu treatment.', '', '214fb7f129f8eac', 0, 3, '21cc82c718b8139.gif', '<p><span style=\"color:#000000\">Treatment ini merupakan tindakan infuse yang dilakukan tenaga medis yang kompeten, memiliki berbagai manfaat, seperti; meningkatkan daya tahan tubuh, menutrisi kulit dan tubuh, meratakan warna kulit, mencerahkan kulit, dan membuat kulit menjadi kenyal, halus dan lembab.</span></p>\r\n', '21e2ad7c1f2fbb0.gif', '<p><span style=\"color:#000000\">Treatment ini mempunyai versi platinum yaitu Infuse Magnificent Beauty Booster. Perbedaan yang didapat adalah dari kandungan obat yang jauh lebih banyak memiliki whitening agent sehingga hasil lebih terlihat bagus dan cepat.</span></p>\r\n', '2021-09-27 09:48:40', '2021-09-27 09:49:23', 'inessytika', 'inessytika'),
('213c05dd9b7dd1b', 'dna-salmon', 'DNA Salmon', '', 'default.jpg', 'Rasakan berbagai manfaat ekstrak DNA ikan salmon yang kaya akan nutrisi, vitamin, dan growth factor untuk kulit yang lebih awet muda, bersinar dan cerah.', '', '214fb7f129f8eac', 0, 3, '21d686ac40bbdfe.gif', '<p><span style=\"color:#000000\">Skin Booster DNA Salmon adalah treatment dengan prosedur penyuntikkan serum ke dalam kulit yang mampu menstimulasi kolagen, meregenerasi sel kulit baru dan mengembalikan keelastisan kulit. Perawatan ini membuat tekstur kulit menjadi lebih halus, cerah, lembab, dan membuat kulit terlihat lebih glowing.</span></p>\r\n', '21b6123083fd675.gif', '<p><span style=\"color:#000000\">Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 45 menit. Hasil akan terlihat setelah 2-3 minggu.</span></p>\r\n', '2021-09-26 13:46:47', '2021-09-27 09:20:10', 'farizan', 'inessytika'),
('21407a96745031e', 'oxygeneo', 'Oxygeneo', '', 'default.jpg', 'Rasakan sensasi segar dan bebas sakit saat perawatan pembersihan wajah', '', '21163a2dc41bdb2', 0, 3, '2136dd1314c42a3.gif', '<p>OxyGeneo adalah treatment facial super dengan teknologi inovatif yang menyediakan tiga perawatan wajah dalam satu perawatan. Tretament ini terdiri dari eksfoliasi ringan, oksigenasi kulit, dan peremajaan wajah. Teknologi OxyGeneo ini terbukti secara klinis menghasilkan nutrisi dan oksigenasi kulit terbaik, sehingga membuat kulit menjadi lebih halus dan tampak lebih muda.</p>\r\n', '21fcb3691b3f86b.gif', '<p>Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 45 menit. Hasilnya pun dapat secara langsung terlihat setelah satu kali perawatan.</p>\r\n', '2021-09-27 09:32:06', '2021-09-27 09:32:28', 'inessytika', 'inessytika'),
('2140a344c7c7eff', 'subsicion-subsisi', 'Subsicion (Subsisi)', '', 'default.jpg', 'Perawatan dari dokter spesialis kulit untuk mengatasi masalah bekas jerawat dan bopeng.', '', '2170ae61421bd56', 0, 3, '2179cd9de32bb7b.gif', '<p>Subsisi adalah prosedur bedah kecil di klinik yang digunakan untuk mengobati bekas jerawat. Prosedur treatment ini adalah dengan memasukkan jarum kecil ke kulit untuk melepaskan bekas jerawat dari jaringan di bawahnya, membuat kulit agak naik dan mengurangi munculnya bekas jerawat.</p>\r\n', '21fb0710ef83a07.gif', '<p>Biasanya pasien akan mengalami downtime pasca treatment ini selama 1 minggu. Jumlah sesi perawatan yang diperlukan tergantung pada tingkat keparahan bekas luka, serta kemampuan regenerasi kulit.</p>\r\n', '2021-09-27 09:43:20', '2021-09-27 09:43:49', 'inessytika', 'inessytika'),
('21431d1ed4899f1', 'acne-injection', 'Acne injection', '', 'default.jpg', 'Atasi jerawat dengan cara yang cepat dan tepat', '', '21b7f27b55df073', 0, 3, '2161f4d18a22712.gif', '<p>Tindakan injeksi yang dimaksudkan untuk mengatasi keluhan jerawat yang sedang / akan tumbuh pada satu daerah kulit tertentu, terutama wajah, atau juga membantu mengatasi keluhan jerawat yang meradang agar menjadi lekas mengering dan berangsur sembuh. Injeksi ini menghambat kerja bakteri P-Acne penyebab jerawat</p>\r\n', '213216cf9cc3f4a.gif', '<p>Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 15 menit.</p>\r\n', '2021-09-27 09:33:48', '2021-09-27 09:34:06', 'inessytika', 'inessytika'),
('214a59d725dee41', 'radio-frequency-rf', 'Radio Frequency (RF)', '', 'default.jpg', 'Dapatkan kulit yang lebih kencang dengan tampilan tubuh yang lebih langsing dengan treatment yang nyaman.', '', '21861ed335d27ea', 0, 3, '210052e1ba9367b.gif', '<p><span style=\"color:#000000\">Radio Frequency (RF) adalah metode pengencangan kulit non-bedah. Prosedur ini melibatkan penggunaan gelombang energi untuk memanaskan lapisan dalam kulit. Panas ini akan merangsang produksi kolagen sehingga membuat kulit menjadi lebih kencang dan membentuk tubuh lebih baik. RF ini termasuk treatment yang sangat aman karena dilakukan dengan peralatan berteknologi canggih yang menargetkan jaringan tertentu pada tubuh pada suhu optimal. Jadi kulit tidak akan merasakan suhu yang terlalu panas namun tetap memberikan hasil yang terbaik.</span></p>\r\n', '21097319293cb13.gif', '<p><span style=\"color:#000000\">Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 30 menit. Tersedia untuk beberapa bagian tubuh, seperti: </span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:2\">- Wajah </span></span></li>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:2\">- Lengan </span></span></li>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:2\">- Perut </span></span></li>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:2\">- Paha</span></span></li>\r\n</ul>\r\n', '2021-09-27 09:51:49', '2021-09-27 09:52:45', 'inessytika', 'inessytika'),
('2154d375e8364a4', 'mengapa-perlu-cleansing--amp--exfoliating-', 'Mengapa Perlu Cleansing & Exfoliating?', 'Mongolia receives new $23.1 million GCF grant to strengthen climate resilience 1', '2145c6ec32b89a1.gif', '<p>Sering dengar istilah 2 tahap membersihkan wajah: cleansing &amp; exfoliating?</p>\r\n\r\n<p>Simak yang berikut ini:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tahapan awal penggunaan skincare biasanya berupa cleansing dan exfoliating.</p>\r\n\r\n<p>Keduanya memiliki fungsi yang berbeda.</p>\r\n\r\n<p>Cleansing biasanya hanya membersihkan bagian permukaan kulit sedangkan exfoliating dilakukan untuk membersihkan sampai ke bagian kulit yang lebih dalam.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"https://4.bp.blogspot.com/-YxkM_Tl4Lps/XEvEXsRttQI/AAAAAAAAAFw/VCvuVZ73kmQkP8wdXRjfSmeE2-fKQWknQCLcBGAs/s1600/IMG_20190126_091515.jpg\" imageanchor=\"1\"><img border=\"0\" data-original-height=\"923\" data-original-width=\"930\" src=\"https://4.bp.blogspot.com/-YxkM_Tl4Lps/XEvEXsRttQI/AAAAAAAAAFw/VCvuVZ73kmQkP8wdXRjfSmeE2-fKQWknQCLcBGAs/s320/IMG_20190126_091515.jpg\" style=\"height:317px; width:320px\" /></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>CLEANSING</strong>&nbsp;adalah tahapan membersihkan, yaitu menghapus sisa krim yang digunakan di malam hari atau menghapus make up mang menempel di wajah setelah digunakan seharian</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>PRODUK YANG BISA DIGUNAKAN:</strong></p>\r\n\r\n<p>Pembersih wajah yang berbahan dasar oil atau yang berbentuk lotion pembersih (cleansing milk), pembersih wajah berbahan dasar air, seperti micellar water, atau menggunakan sabun khusus untuk mencuci wajah.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"https://1.bp.blogspot.com/-Y7e86DfyOBk/XEvFl_kESzI/AAAAAAAAAF8/sGE2j4x71iInUCbD0ly2C46azQGR8t_5gCLcBGAs/s1600/IMG_20190126_091636.jpg\" imageanchor=\"1\"><img border=\"0\" data-original-height=\"926\" data-original-width=\"925\" src=\"https://1.bp.blogspot.com/-Y7e86DfyOBk/XEvFl_kESzI/AAAAAAAAAF8/sGE2j4x71iInUCbD0ly2C46azQGR8t_5gCLcBGAs/s320/IMG_20190126_091636.jpg\" style=\"height:320px; width:319px\" /></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>EXFOLIATING</strong>&nbsp;adalah tahapan membersihkan wajah dengan mengangkat sel-sel kulit mati.</p>\r\n\r\n<p>Proses ini tidak perlu terlalu sering dilakukan, cukup satu kali dalam seminggu.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>PRODUK YANG BISA DIGUNAKAN:</strong></p>\r\n\r\n<p>Toner, lotion, krim yang mengandung AHA, BHA, atau PHA.</p>\r\n\r\n<p>Hati-hati menggunakan produk berbentuk scrub, terutama apabila sedang menggunakan krim malam.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>CATATAN:</strong></p>\r\n\r\n<p>Meski bermanfaat bagi kulit, namun sebelum melakukan proses eksfoliasi ini sebaiknya konsultasikan terlebih dahulu pada dokter SpKK, khususnya bagi Anda yang memiliki jenis kulit sensitif.</p>\r\n\r\n<p>Melakukan eksfoliasi secara sembarangan seperti menggunakan produk dan alat yang tidak tepat hanya akan membuat kondisi kulit menjadi lebih buruk.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Sumber:</strong></p>\r\n\r\n<p><a href=\"https://instagram.com/perdoski.id?utm_source=ig_profile_share&amp;igshid=1avcyhyfugl5s\">https://instagram.com/perdoski.id</a></p>\r\n\r\n<p>(Perhimpunan Dokter Spesialis Kulit dan Kelamin Indonesia)</p>\r\n', '<p><strong>New York, Nov 11</strong>&nbsp;&ndash;&nbsp;The Green Climate Fund approved a&nbsp;<a href=\"https://www.greenclimate.fund/\">new US$23.1million grant</a>&nbsp;to UNDP supported&nbsp;<a href=\"https://www.adaptation-undp.org/projects/improving-adaptive-capacity-and-risk-management-rural-communities-mongolia-0\">project</a>&nbsp;aimed at strengthening climate resilience in&nbsp;<strong>Mongolia</strong>. The grant comes at a time when the country is facing a range of natural disasters, climate change is&nbsp;<a href=\"https://unfccc.int/resource/docs/natc/mongnc2.pdf\">multiplying the challenges</a>&nbsp;with estimated economic costs around $10-15 million annually, and COVID-19 further exposing vulnerability of livestock sector.</p>\r\n\r\n<p>Mongolia&rsquo;s economy relies heavily on the agriculture and livestock husbandry sectors, with a high dependency on natural resources. Livestock accounts for 90 percent of the agriculture sector and represents the lion&rsquo;s share of provincial economies (around 85 percent).&nbsp; Herder households are particularly vulnerable to climate change impacts. Adaptation needs are expected to increase significantly as climate change impacts intensify.</p>\r\n\r\n<p>The new grant reinforces the importance of green recovery and resilient growth. Approximately 26,000 households (130,000 people) living across four of the country&rsquo;s most remote and vulnerable Western and Eastern provinces are set to benefit, with a further 160,000 households (800,000 people) to benefit indirectly &ndash; which is about one quarter of Mongolia&rsquo;s entire population.</p>\r\n\r\n<p>The project&rsquo;s design links closely with Mongolia&rsquo;s&nbsp;<a href=\"https://policy.asiapacificenergy.org/sites/default/files/National%20Action%20Programme%20on%20Climate%20Change%20%28NAPCC%29%20%28MN%29.pdf\">National Action Program on Climate Change</a>, vision 2050, and policies related to the livestock sector, as well as its&nbsp;<a href=\"https://www4.unfccc.int/sites/submissions/INDC/Published%20Documents/Mongolia/1/150924_INDCs%20of%20Mongolia.pdf\">Nationally Determined Contribution</a>s under the global Paris Agreement. Implementation of the project activities is expected to begin in mid-2021.</p>\r\n', '206d33f1a06743e', 1, 1, NULL, NULL, NULL, NULL, '2021-01-26 12:07:07', '2021-09-25 14:40:54', 'zul', 'inessytika'),
('2155f72ee784dee', 'meso-brightening', 'Meso Brightening', '', 'default.jpg', 'Dapatkan kulit yang lebih cerah, sehat dan cantik.', '', '214fb7f129f8eac', 0, 3, '21ad7df916283c0.gif', '<p>Meso Brightening adalah tindakan yang dilakukan dengan teknik mesoterapi menggunakan serum yang dirancang untuk perawatan pemutihan kulit yang aman dan intensif. Direkomendasikan untuk orang-orang dengan warna kulit gelap dan juga untuk mengurangi munculnya bintik-bintik dan perubahan warna kulit yang disebabkan oleh jerawat dan photo-aging (bintik-bintik penuaan).</p>\r\n', '21bf23228aefe60.gif', '<p>Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 60 menit. Tindakan ini dilengkapi dengan pengolesan krim anastesi untuk meningkatkan kenyamanan pasien saat treatment.</p>\r\n', '2021-09-27 09:45:49', '2021-09-27 09:46:16', 'inessytika', 'inessytika'),
('215821c69ee5f51', 'facial', 'Facial', '', 'default.jpg', 'Relaksasi wajah yang sekaligus membersihkan dan meremajakan kulit.', '', '21f67ae2ef655d4', 0, 3, '215fd76b6e00b09.gif', '<p>Treatment ini bertujuan untuk membersihkan komedo pada area tertentu di kulit wajah secara intense dan menyeluruh. Plus point yang diberikan untuk treatment ini adalah efek dari serum tambahan yang di berikan dengan manfaat tertentu seperti mengontrol kadar minyak, mencerahkan kulit, anti aging, keluhan jerawat dan detox</p>\r\n', '21604b028db33ec.gif', '<p>Jenis treatment Facial :</p>\r\n\r\n<ul>\r\n	<li><strong>&bull; Facial Hydrating</strong></li>\r\n	<li><strong>&bull; Facial Anti Acne</strong></li>\r\n	<li><strong>&bull; Facial Brightening</strong></li>\r\n	<li><strong>&bull; Facial Anti Aging</strong></li>\r\n	<li><strong>&bull; Facial Super Anti Aging</strong></li>\r\n</ul>\r\n', '2021-09-26 22:53:50', '2021-09-27 09:12:52', 'inessytika', 'inessytika'),
('216235572b64941', 'pdt-light-therapy', 'PDT / Light Therapy', '', 'default.jpg', 'Rasakan wajah lebih bersih bebas bakteri penyebab jerawat hingga kulit terdalam', '', '214fb7f129f8eac', 0, 3, '213912dcad2f1a5.gif', '<p>PDT/Light Therapy adalah terapi kecantikan menggunakan sinar yang difilter untuk menghasilkan panjang gelombang tertentu sehingga mampu mengatasi berbagai masalah kulit, diantaranya; mengurangi produksi minyak yang berlebih, mengurangi peradangan jerawat, mengurangi bekas kemerahan karena jerawat, mencerahkan dan menghaluskan kulit.</p>\r\n', '21feed673f3fd83.gif', '<p>Light therapy akan dibutuhkan setelah melakukan mikrodermabrasi, PRP, scar removal, dan treatment lainnya yang menyebabkan kemerahan pasca treatment untuk mengurangi peradangan. Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 15 menit.</p>\r\n', '2021-09-27 09:25:37', '2021-09-27 09:26:01', 'inessytika', 'inessytika'),
('216cd6e9a7f9c15', 'pdt-light-therapy', 'PDT / Light Therapy', '', 'default.jpg', 'Rasakan wajah lebih bersih bebas bakteri penyebab jerawat hingga kulit terdalam', '', '21b7f27b55df073', 0, 3, '21ff144d56c1c6e.gif', '<p>PDT/Light Therapy adalah terapi kecantikan menggunakan sinar yang difilter untuk menghasilkan panjang gelombang tertentu sehingga mampu mengatasi berbagai masalah kulit, diantaranya; mengurangi produksi minyak yang berlebih, mengurangi peradangan jerawat, mengurangi bekas kemerahan karena jerawat, mencerahkan dan menghaluskan kulit.</p>\r\n', '2194c79d9a41d2f.gif', '<p>Light therapy akan dibutuhkan setelah melakukan mikrodermabrasi, PRP, scar removal, dan treatment lainnya yang menyebabkan kemerahan pasca treatment untuk mengurangi peradangan. Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 15 menit.</p>\r\n', '2021-09-27 09:21:26', '2021-09-27 09:26:48', 'inessytika', 'inessytika'),
('2172a481e840dfd', 'intense-pulsed-light-ipl', 'Intense Pulsed Light (IPL)', '', '21efe2d926b8d6d.png', 'Rasakan Kulit yang Lebih Cerah dan Mulus', '', '214fb7f129f8eac', 0, 3, '211b2f2ab304005.gif', '<p><span style=\"color:#000000\">Terapi IPL non-ablative adalah prosedur fototerapi yang digunakan untuk mengatasi masalah jerawat, pori-pori kulit besar, kemerahan wajah, pelebaran pembuluh darah tekstur kulit kasar dan juga untuk masalah pigmentasi karena paparan sinar ultraviolet dan bertambahnya usia, tanpa menimbulkan kerusakan atau luka pada kulit bagian atas (epidermis) dan juga bisa memperlambat pertumbuhan rambut (hair removal).</span></p>\r\n\r\n<p><br />\r\n<span style=\"color:#000000\">Prosedur ini hanya memerlukan waktu sekitar 10 - 30 menit, tergantung pada luas area.</span></p>\r\n', '2134bcd5a3a0d02.gif', '<p><span style=\"color:#000000\">Untuk treatment IPL yang terdapat di Prossi Clinic ada tiga jenis, diantaranya:</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:3\"><strong>Rejuvenation</strong></span></span></li>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:3\"><strong>Hair removal</strong></span></span></li>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:3\"><strong>IPL Vaskular</strong></span></span></li>\r\n</ul>\r\n', '2021-06-08 12:13:15', '2021-09-26 22:28:36', 'farizan', 'inessytika'),
('217b4e08409ccf8', 'chemical-peeling', 'Chemical Peeling', '', '21540281a33ad7b.gif', 'Rasakan kulit wajah cantik bersinar dan lebih cerah', '', '21b7f27b55df073', 0, 3, '21d14c47b9030f2.gif', '<p><span style=\"color:#000000\">Tindakan Peeling adalah proses pengelupasan kulit dengan bantuan larutan kimia berbentuk cairan yang dioleskan pada permukaan kulit. Akibat dari pengaplikasian cairan kimia ini, lapisan kulit yang mati akan luruh (ter eksfoliasi) dan kulit baru yang muda pun akan terlihat (rejuvenasi/peremajaan kulit). Proses ini merupakan salah satu cara tercepat untuk mendapatkan tampilan kulit yang ideal dan dapat digunakan untuk mengatasi masalah jerawat ringan, flek/hiperpigmentasi, skar, dan penuaan kulit.</span></p>\r\n', '21963e85cee6c9e.gif', '<p><span style=\"color:#000000\">Untuk treatment Peeling yang terdapat di Prossi Clinic ada dua jenis Peeling, yaitu:</span></p>\r\n\r\n<ul>\r\n	<li><strong><span style=\"line-height:3\">Peeling Radiant Glow</span></strong></li>\r\n	<li><strong><span style=\"line-height:3\"><span style=\"color:#000000\">Peeling Platinum</span></span></strong></li>\r\n</ul>\r\n', '2021-06-08 12:14:58', '2021-09-26 22:22:42', 'farizan', 'inessytika'),
('2180731127ce3ba', 'fat-burn-treatment-package-ftp', 'Fat-Burn Treatment Package (FTP)', '', 'default.jpg', 'Solusi maksimal mengurangi lemak di tubuh', '', '21f0b7d29a3528e', 0, 3, '2149cb1f15ff446.gif', '<p><span style=\"color:#000000\">Fat-Burn Treatment Package adalah paket perawatan penurunan berat badan berupa injeksi meso slimming. Dilengkapi dengan sesi pengukuran analisis komposisi tubuh untuk mengetahui secara tepat kebutuhan program penurunan berat badan.</span></p>\r\n', '21d846d47755ffa.gif', '<p>Ada 3 pilihan FTP yang tersedia, diantaranya -</p>\r\n\r\n<ul>\r\n	<li><strong><span style=\"line-height:3\"><span style=\"color:#000000\">FTP 1</span></span></strong></li>\r\n	<li><strong><span style=\"line-height:3\"><span style=\"color:#000000\">FTP 2</span></span></strong></li>\r\n	<li><strong><span style=\"line-height:3\"><span style=\"color:#000000\">FTP 3</span></span></strong></li>\r\n</ul>\r\n', '2021-09-26 22:31:49', '2021-10-05 10:18:33', 'inessytika', 'zul'),
('21864759d051ca6', 'scar-removal', 'Scar Removal', '', 'default.jpg', 'Dapatkan kulit mulus bebas bopeng dan bekas jerawat ', '', '2170ae61421bd56', 0, 3, '213ed8dadd7dbda.gif', '<p><span style=\"color:#000000\">Prinsip treatment ini yaitu melalui penggunaan jarum yang disposable (sekali pakai) di permukaan kulit wajah dan pengolesan serum khusus. Efeknya, terjadi luka baru. Dengan dilukai, otomatis tubuh akan bereaksi dan memperbaikinya dengan menumbuhkan jaringan baru. Tretment ini (microneedle) digerakan dengan bantuan tenaga listrik secara otomatis, intensitas kedalaman tusukannya dapat diatur pada tombol yang terdapat pada alat tersebut.</span></p>\r\n', '21dfaece81cd870.gif', '<p><span style=\"color:#000000\">Treatment ini memiliki versi platinum bernama Super Flawless Scar Removal. Perbedaannya terdapat dari serum yang digunakan dapat meningkatkan pertumbuhan jaringan baru dengan lebih cepat.</span></p>\r\n', '2021-09-27 09:35:23', '2021-09-27 09:35:56', 'inessytika', 'inessytika'),
('218cfac786a22f4', 'dna-salmon', 'DNA Salmon', '', 'default.jpg', 'Rasakan berbagai manfaat ekstrak DNA ikan salmon yang kaya akan nutrisi, vitamin, dan growth factor untuk kulit yang lebih awet muda, bersinar dan cerah.', '', '21f67ae2ef655d4', 0, 3, '2142c9a079050aa.gif', '<p>Skin Booster DNA Salmon adalah treatment dengan prosedur penyuntikkan serum ke dalam kulit yang mampu menstimulasi kolagen, meregenerasi sel kulit baru dan mengembalikan keelastisan kulit. Perawatan ini membuat tekstur kulit menjadi lebih halus, cerah, lembab, dan membuat kulit terlihat lebih glowing.</p>\r\n', '21afe44dcceec68.gif', '<p>Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 45 menit. Hasil akan terlihat setelah 2-3 minggu.</p>\r\n', '2021-09-27 09:17:25', '2021-09-27 09:18:01', 'inessytika', 'inessytika'),
('2192cb93921a93c', 'facial', 'Facial', '', 'default.jpg', 'Relaksasi wajah yang sekaligus membersihkan dan meremajakan kulit.', '', '21163a2dc41bdb2', 0, 3, '21ba0051293bfca.gif', '<p><span style=\"color:#000000\">Treatment ini bertujuan untuk membersihkan komedo pada area tertentu di kulit wajah secara intense dan menyeluruh. Plus point yang diberikan untuk treatment ini adalah efek dari serum tambahan yang di berikan dengan manfaat tertentu seperti mengontrol kadar minyak, mencerahkan kulit, anti aging, keluhan jerawat dan detox</span></p>\r\n', '21023398776c330.gif', '<p><span style=\"color:#000000\">Jenis treatment Facial :</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><strong>&bull; Facial Hydrating</strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong>&bull; Facial Anti Acne</strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong>&bull; Facial Brightening</strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong>&bull; Facial Anti Aging</strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong>&bull; Facial Super Anti Aging</strong></span></li>\r\n</ul>\r\n', '2021-09-26 22:58:41', '2021-09-26 23:02:07', 'inessytika', 'inessytika'),
('2198025fd5ad9d9', 'intense-pulsed-light-ipl', 'Intense Pulsed Light (IPL)', '', 'default.jpg', 'Rasakan Kulit yang Lebih Mulus dan Cerah', '', '21b7f27b55df073', 0, 3, '21501733a0868e7.gif', '<p><span style=\"color:#000000\">Terapi IPL non-ablative adalah prosedur fototerapi yang digunakan untuk mengatasi masalah jerawat, pori-pori kulit besar, kemerahan wajah, pelebaran pembuluh darah tekstur kulit kasar dan juga untuk masalah pigmentasi karena paparan sinar ultraviolet dan bertambahnya usia, tanpa menimbulkan kerusakan atau luka pada kulit bagian atas (epidermis) dan juga bisa memperlambat pertumbuhan rambut (hair removal). Prosedur ini hanya memerlukan waktu sekitar 10 - 30 menit, tergantung pada luas area&nbsp;</span></p>\r\n', '21569156a519414.gif', '<p><span style=\"color:#000000\">Untuk treatment IPL yang terdapat di Prossi Clinic ada 3 jenis,&nbsp;diantaranya: </span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">&bull; Rejuvenation </span></strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">&bull; Hair Removal </span></strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">&bull; IPL Vaskular</span></strong></span></li>\r\n</ul>\r\n', '2021-09-26 22:45:46', '2021-09-26 22:46:36', 'inessytika', 'inessytika'),
('21a395b89083c15', 'pdt-light-therapy', 'PDT / Light Therapy', '', 'default.jpg', 'Rasakan wajah lebih bersih bebas bakteri penyebab jerawat hingga kulit terdalam', '', '21f67ae2ef655d4', 0, 3, '21e000613f2779a.gif', '<p>PDT/Light Therapy adalah terapi kecantikan menggunakan sinar yang difilter untuk menghasilkan panjang gelombang tertentu sehingga mampu mengatasi berbagai masalah kulit, diantaranya; mengurangi produksi minyak yang berlebih, mengurangi peradangan jerawat, mengurangi bekas kemerahan karena jerawat, mencerahkan dan menghaluskan kulit.</p>\r\n', '214eda8878155c8.gif', '<p>Light therapy akan dibutuhkan setelah melakukan mikrodermabrasi, PRP, scar removal, dan treatment lainnya yang menyebabkan kemerahan pasca treatment untuk mengurangi peradangan. Treatment ini membutuhkan waktu pengerjaan selama kurang lebih 15 menit.</p>\r\n', '2021-09-27 09:19:50', '2021-09-27 09:23:32', 'inessytika', 'inessytika'),
('21be4060ad31cf0', 'laser', 'Laser', '', 'default.jpg', 'Rasakan wajah yang lebih cerah, warna kulit yang lebih merata, dan bebas flek hitam', '', '21b7f27b55df073', 0, 3, '21aeba4c80cc97e.gif', '<p>Laser NdYag yang tersedia di Prossi Clinic merupakan salah satu jenis teknologi laser canggih yang ada saat ini. Menggunakan gelombang sinar panjang bertenaga tinggi untuk menembus lapisan kulit yang lebih dalam tanpa merusak permukaan kulit. Treatment ini mampu mengatasi berbagai masalah kulit seperti meratakan warna kulit wajah, mengurangi pigmentasi pada kulit wajah, mencerahkan wajah.</p>\r\n', '21ae30dac87e5cd.gif', '<p>Untuk treatment Laser yang terdapat di Prossi Clinic ada dua jenis, diantaranya:</p>\r\n\r\n<ul>\r\n	<li><span style=\"line-height:3\"><strong>Melasma laser</strong></span></li>\r\n	<li><span style=\"line-height:3\"><strong>Brightening laser</strong></span></li>\r\n</ul>\r\n', '2021-09-26 23:06:18', '2021-09-26 23:06:56', 'inessytika', 'inessytika'),
('21d12b9e2fd3e12', 'facial', 'Facial', '', 'default.jpg', 'Relaksasi wajah yang sekaligus membersihkan dan meremajakan kulit.', '', '21b7f27b55df073', 0, 3, '21f2c04700761b9.gif', '<p>Treatment ini bertujuan untuk membersihkan komedo pada area tertentu di kulit wajah secara intense dan menyeluruh. Plus point yang diberikan untuk treatment ini adalah efek dari serum tambahan yang di berikan dengan manfaat tertentu seperti mengontrol kadar minyak, mencerahkan kulit, anti aging, keluhan jerawat dan detox</p>\r\n', '2170991992065db.gif', '<p>Jenis treatment Facial :</p>\r\n\r\n<ul>\r\n	<li><strong>&bull; Facial Hydrating</strong></li>\r\n	<li><strong>&bull; Facial Anti Acne</strong></li>\r\n	<li><strong>&bull; Facial Brightening</strong></li>\r\n	<li><strong>&bull; Facial Anti Aging</strong></li>\r\n	<li><strong>&bull; Facial Super Anti Aging</strong></li>\r\n</ul>\r\n', '2021-09-26 22:56:08', '2021-09-27 09:12:30', 'inessytika', 'inessytika'),
('21d37083686333a', 'intense-pulsed-light-ipl', 'Intense Pulsed Light (IPL)', '', 'default.jpg', 'Rasakan Kulit yang Lebih Mulus dan Cerah', '', '2153fa16746368c', 0, 3, '215bc212cb47e7a.gif', '<p>Terapi IPL non-ablative adalah prosedur fototerapi yang digunakan untuk mengatasi masalah jerawat, pori-pori kulit besar, kemerahan wajah, pelebaran pembuluh darah tekstur kulit kasar dan juga untuk masalah pigmentasi karena paparan sinar ultraviolet dan bertambahnya usia, tanpa menimbulkan kerusakan atau luka pada kulit bagian atas (epidermis) dan juga bisa memperlambat pertumbuhan rambut (hair removal). Prosedur ini hanya memerlukan waktu sekitar 10 - 30 menit, tergantung pada luas area&nbsp;</p>\r\n', '215bc212cb47e7a.gif', '<p><span style=\"color:#000000\">Untuk treatment IPL yang terdapat di Prossi Clinic ada 3 jenis,&nbsp;diantaranya: </span></p>\r\n\r\n<ul>\r\n	<li><strong><span style=\"line-height:3\"><span style=\"color:#000000\">&bull; Rejuvenation </span></span></strong></li>\r\n	<li><strong><span style=\"line-height:3\"><span style=\"color:#000000\">&bull; Hair Removal </span></span></strong></li>\r\n	<li><strong><span style=\"line-height:3\"><span style=\"color:#000000\">&bull; IPL Vaskular</span></span></strong></li>\r\n</ul>\r\n', '2021-09-26 22:39:40', '2021-09-26 22:40:45', 'inessytika', 'inessytika'),
('21d834881c7043f', 'profood', 'ProFood', '', 'default.jpg', 'Solusi catering sehat untuk menunjang program penurunan berat badan', '', '211aa25cb77ada2', 0, 3, '21db26b160eb263.gif', '<p><span style=\"color:#000000\">ProFood adalah layanan catering sehat yang bekerjasama dengan catering yang sudah mendapatkan sertifikat laik higiene sanitasi dan lulus uji higienitas dari laboratorium kesehatan daerah.</span></p>\r\n', '21103cb7a22fb9c.gif', '<p><span style=\"color:#000000\">Ada 2 pilihan ProFood yang tersedia, diantaranya</span></p>\r\n\r\n<ul>\r\n	<li><strong><span style=\"line-height:3\"><span style=\"color:#000000\">ProFood 1</span></span></strong></li>\r\n	<li><strong><span style=\"line-height:3\"><span style=\"color:#000000\">ProFood 2</span></span></strong></li>\r\n</ul>\r\n', '2021-09-26 14:43:22', '2021-09-27 09:14:13', 'farizan', 'inessytika'),
('21dcc396faac0fd', 'laser', 'Laser', '', '21efe2d926b8d6d.png', 'Rasakan wajah yang lebih cerah, warna kulit yang lebih merata, dan bebas flek hitam', '', '21f67ae2ef655d4', 0, 3, '213110ba81ea480.gif', '<p><span style=\"color:#000000\">Laser NdYag yang tersedia di Prossi Clinic merupakan salah satu jenis teknologi laser canggih yang ada saat ini. Menggunakan gelombang sinar panjang bertenaga tinggi untuk menembus lapisan kulit yang lebih dalam tanpa merusak permukaan kulit. Treatment ini mampu mengatasi berbagai masalah kulit seperti meratakan warna kulit wajah, mengurangi pigmentasi pada kulit wajah, mencerahkan wajah.</span></p>\r\n', '21b139538d7af3f.gif', '<p><span style=\"color:#000000\">Untuk treatment Laser yang terdapat di Prossi Clinic ada dua jenis, diantaranya:</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">Melasma laser</span></strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">Brightening laser</span></strong></span></li>\r\n</ul>\r\n', '2021-06-08 12:11:51', '2021-09-26 22:25:20', 'farizan', 'inessytika'),
('21dee49248fccd2', 'inject-keloid', 'Inject keloid', '', 'default.jpg', 'Dapatkan kulit yang lebih mulus dan rata tanpa bekas luka', '', '2170ae61421bd56', 0, 3, '2156dd10f304250.gif', '<p>Keloid adalah jaringan parut yang sering muncul pada luka akibat kecelakaan atau tindakan operasi. Pengobatan keloid kadang tidak mudah, namun bukan berarti keloid tidak bisa diobati.</p>\r\n', '216e595bb97f465.gif', '<p>Suntik keloid bisa dilakukan dalam beberapa kali tindakan yang diulang setiap 3-6 minggu sekali, sampai keloid jauh berkurang hingga mencapai hasil akhir.</p>\r\n', '2021-09-27 09:37:37', '2021-09-27 09:41:33', 'inessytika', 'inessytika'),
('21eedcc78176cb7', 'laser', 'Laser', '', 'default.jpg', 'Rasakan wajah yang lebih cerah, warna kulit yang lebih merata, dan bebas flek hitam', '', '214fb7f129f8eac', 0, 3, '21d67e1bbbdc655.gif', '<p>Laser NdYag yang tersedia di Prossi Clinic merupakan salah satu jenis teknologi laser canggih yang ada saat ini. Menggunakan gelombang sinar panjang bertenaga tinggi untuk menembus lapisan kulit yang lebih dalam tanpa merusak permukaan kulit. Treatment ini mampu mengatasi berbagai masalah kulit seperti meratakan warna kulit wajah, mengurangi pigmentasi pada kulit wajah, mencerahkan wajah.</p>\r\n', '2198abfdbad105e.gif', '<p><span style=\"color:#000000\">Untuk treatment Laser yang terdapat di Prossi Clinic ada dua jenis, diantaranya:</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:3\"><strong>Melasma laser</strong></span></span></li>\r\n	<li><span style=\"color:#000000\"><span style=\"line-height:3\"><strong>Brightening laser</strong></span></span></li>\r\n</ul>\r\n', '2021-09-26 23:08:43', '2021-09-27 09:12:09', 'inessytika', 'inessytika');
INSERT INTO `t_artikel` (`KodeArtikel`, `UrlArtikel`, `JudulId`, `JudulEn`, `MainImage`, `ContentId`, `ContentEn`, `KodeKategori`, `EnableComment`, `Tipe`, `Lainnya1`, `Lainnya2`, `Lainnya3`, `Lainnya4`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`) VALUES
('21f2ab90ac20bd2', 'tentang-barrier-kulit', 'Tentang Barrier Kulit', 'New Harris Bugg design for Bridgewater', '212a007a3572a4a.gif', '<p>Supaya bisa menjalani fungsinya sebagai barrier / pelindung dari berbagai faktor eksternal, kulit perlu selalu kita jaga.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Barrier kulit merupakan lapisan kulit terluar yang terdiri atas:</p>\r\n\r\n<p>-Sel kulit yang sudah mati dan lipids.</p>\r\n\r\n<p>-Natural moisturizing factors yang diproduksi oleh tubuh sendiri.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Barrier kulit memiliki peran yang sangat penting yaitu sebagai pelindung kulit dari berbagai macam faktor yang memicu kerusakan pada lapisan kulit terluar, seperti:</p>\r\n\r\n<p>-Sinar UVA</p>\r\n\r\n<p>-Sinar UVB</p>\r\n\r\n<p>-Berbagai Macam Kuman</p>\r\n\r\n<p>-Debu/Polusi</p>\r\n\r\n<p>-Bahan Kimia</p>\r\n\r\n<p>-Trauma Mekanik &amp; Fisik</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Namun, jika ada gangguan pada barrier kulit maka kulit akan mengalami kerusakan yang ditimbulkan dari masalah kulit, seperti:</p>\r\n\r\n<p>-Terbakar</p>\r\n\r\n<p>-Kemerahan</p>\r\n\r\n<p>-Infeksi Bakteri/Virus/Jamur</p>\r\n\r\n<p>-Eksim</p>\r\n\r\n<p>-Gatal-gatal</p>\r\n\r\n<p>-Kekeringan Kulit</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table cellpadding=\"0\" cellspacing=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td><a href=\"https://1.bp.blogspot.com/-mDrJqtav_xQ/XFKpguKYKhI/AAAAAAAAAGQ/vfMeJWn_3oYGFGAz9HpxJeMG4Z5lESw7gCLcBGAs/s1600/IMG_20190131_144638.jpg\" imageanchor=\"1\"><img border=\"0\" data-original-height=\"940\" data-original-width=\"927\" src=\"https://1.bp.blogspot.com/-mDrJqtav_xQ/XFKpguKYKhI/AAAAAAAAAGQ/vfMeJWn_3oYGFGAz9HpxJeMG4Z5lESw7gCLcBGAs/s320/IMG_20190131_144638.jpg\" style=\"height:320px; width:315px\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td><a href=\"https://instagram.com/perdoski.id\">Sumber: https://instagram.com/perdoski.id</a></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bila sudah mengalami kerusakan, butuh waktu untuk mengembalikan keadaan barrier kulit untuk kembali normal.</p>\r\n\r\n<p>Karena itu, kulit butuh penggunaan skincare yang&nbsp; tepat sesuai dengan tipe kulit.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Dan didukung dengan gaya hidup yang sehat, seperti:</p>\r\n\r\n<p>-Rajin Olahraga</p>\r\n\r\n<p>-Konsumsi Sayuran</p>\r\n\r\n<p>-Konsumsi Buah-buahan</p>\r\n\r\n<p>-Tidur Minimal 6 Jam</p>\r\n\r\n<p>-Tidak Mengonsumsi Alkohol</p>\r\n\r\n<p>-Tidak Merokok</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Sumber:</strong></p>\r\n\r\n<p>IG:&nbsp;<a href=\"https://instagram.com/perdoski.id\">https://instagram.com/perdoski.id</a></p>\r\n\r\n<p>(Perhimpunan Dokter Spesialis Kulit dan Kelamin Indonesia)</p>\r\n', '<p>Integer luctus diam ac scerisque consectetur. Vimus ottawas euismod nec lacus sit amet. Aenean interdus midu vitae, uttah mattis augue fermentum. Donec auctor massa orci, quis condimentum odio eleifended. Orci varius natoque penatibuset magnis discount parturient montes, nascetur ridiculus mus. Ut felis lectus, sagittis in turpis sit amet, ornare interdu ligula. Proin sed dolor eu nulla fermentum fermentum. Suspendisse eget mollis diam. Nulla non mauris et eros accumsan imperdit sed ut turpis. Ut aliquam et sapien at convallis. Integer eu porttitor lacus. Curabitur id aliquam mauris.</p>\r\n\r\n<p>Nullam lectus elit, volutpat velo justo sit damet, tincidunt dapibus turpis. Vivamus idelit nec enim tristique blandit in sit down metunc. Maecenas accumsan nunc quis nisl porttitor varius sed luctus ligula. Aeneamana pellentesque enim eu magna vehicula suada.</p>\r\n\r\n<p>Quisque suscipit elit sit ametz pellentesque scelerisque. Integer actioner cursu quam, estina portitor cant. Vestibulum luctus libero urna gamora scelerisque laoret. Quisque nect facilisis neque. Integer vitaer dapibus purus, fames turpis egestas. Nullam vulputa nisl tempus luctus.</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/myblog/raw/img/bg-img/36.jpg\" /></p>\r\n\r\n<p>Mauris nisi arcu, consectetur convallis fringilla quis, posuere ac mauris. Ut in placerat lorem. Donec cursus malesuada nibhem, eget consectetur posuere sed. Suspendisse auctor nec diamet consectetur. Etiam ac maurised nisib tincidunt viverra. Sed nulla lacus, convallis vel nunc sed, fringilla venenatis neque.</p>\r\n', '21e64f9536f34e0', 1, 1, NULL, NULL, NULL, NULL, '2021-05-12 10:42:08', '2021-09-25 14:37:17', 'zul', 'inessytika'),
('21fb7fc7706c321', 'intense-pulsed-light-ipl', 'Intense Pulsed Light (IPL)', '', '21efe2d926b8d6d.png', 'Rasakan Kulit yang Lebih Mulus dan Cerah', '', '21f67ae2ef655d4', 0, 3, '216a530bca8c217.gif', '<p><span style=\"color:#000000\">Terapi IPL non-ablative adalah prosedur fototerapi yang digunakan untuk mengatasi masalah jerawat, pori-pori kulit besar, kemerahan wajah, pelebaran pembuluh darah tekstur kulit kasar dan juga untuk masalah pigmentasi karena paparan sinar ultraviolet dan bertambahnya usia, tanpa menimbulkan kerusakan atau luka pada kulit bagian atas (epidermis) dan juga bisa memperlambat pertumbuhan rambut (hair removal).</span></p>\r\n\r\n<p><br />\r\n<span style=\"color:#000000\">Prosedur ini hanya memerlukan waktu sekitar 10 - 30 menit, tergantung pada luas area&nbsp;</span></p>\r\n', '21eeed4abeb7734.gif', '<p><span style=\"color:#000000\"><span style=\"line-height:1\">Untuk treatment IPL yang terdapat di Prossi Clinic ada 3 jenis,&nbsp;diantaranya:</span></span></p>\r\n\r\n<ul>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">&bull; Rejuvenation</span></strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">&bull; Hair Removal</span></strong></span></li>\r\n	<li><span style=\"color:#000000\"><strong><span style=\"line-height:3\">&bull; IPL Vaskular</span></strong></span></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2021-08-28 12:42:04', '2021-09-26 22:28:20', 'zul', 'inessytika'),
('2286ecf21463e93', 'cinta-diri-menurut-para-ahli-di-perayaan-ulang-tahun-prossi-clinic-yang-ke-3', 'Cinta Diri Menurut Para Ahli di Perayaan Ulang Tahun Prossi Clinic yang ke-3', '', '227ea23acb0255a.png', '<p><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Dalam rangka ulang tahun ketiganya, Prossi Clinic mengadakan perayaan dan talkshow dengan tema &ldquo;Mencintai Diri Sendiri&rdquo; di Twin House Cipete, Jakarta Selatan, pada 25 Oktober 2022. Dalam rangka menyebarkan kesadaran #CintaDiri, Prossi Clinic juga turut mengajak serta beberapa brand&nbsp;dan media seperti, Tropicana Slim, Candelula, Yellow Fit Kitchen, Evergreen Salon, <a href=\"https://www.suara.com/lifestyle/2022/09/26/091220/umur-nyaris-50-tahun-ternyata-ini-rahasia-wanda-hamidah-tetap-bugar-dan-awet-muda\">Suara.com</a>, dan <a href=\"https://hellosehat.com/penyakit-kulit/perawatan-kulit/manfaat-perawatan-kulit/\">HelloSehat</a> untuk bekerjasama menjalankan rangkaian acara ini.&nbsp;</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Tema tersebut dipilih untuk sedikit banyak menggambarkan visi dan semangat Prossi Clinic yang setia menemani para pelanggan untuk mencintai diri sendiri dengan cara rutin merawat tubuh. Selain itu, Prossi Clinic selalu berpegang pada prinsip bahwa jalan terbaik dalam mencintai diri sendiri adalah mensyukuri dan mengoptimalkan tubuh pemberian Tuhan tanpa mengubah bentuk dan fungsinya. Prinsip tersebut diharapkan dapat membuka sudut pandang masyarakat bahwa setiap orang berhak tampil sehat dan menawan sesuai dengan keadaan masing-masing.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Prossi Clinic menghadirkan beberapa narasumber talkshow yaitu dr. Erika Nurhandayani Zoulba, Sp.KK sebagai spesialis kulit dan dr. Prinsa Raudha Acna, Sp.AK sebagai spesialis akupunktur.</span></span></span></p>\r\n\r\n<p><img alt=\"dr. Erika Nurhandayani Zoulba, Sp.KK sedang memberikan pendapat mengenai bentuk cintai diri dari menjaga kesehatan kulit\" class=\"img-responsive lazy newsleft\" loading=\"lazy\" src=\"/assets/xdfxcd/vendor/kcfinder/upload/files/FE851189-C148-4A33-835E-619A05D43305.jpeg\" style=\"display:block; margin-left:auto; margin-right:auto; width:90%\"></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Dokter Erika menjelaskan bahwa tidak semua orang terlahir dengan fisik yang &lsquo;sempurna&rsquo; menurut stereotip orang, seperti hidung mancung, kulit putih, rambut lurus, badan langsing, dan sebagainya. &ldquo;Tapi semua orang bisa memunculkan sisi terbaik masing-masing. Kalau kulitnya gak putih, ya minimalnya gak kusam. Kalau dari lahirnya udah gemuk, minimalnya gak obesitas,&rdquo; jelasnya. Ia menambahkan setidaknya seseorang harus memenuhi standar kriteria kulit sehat, apalagi kulit adalah organ terluar dan yang paling terlihat.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Turut hadir dalam acara tersebut Wanda Hamidah yang menceritakan pengalaman perawatan kulitnya. &ldquo;Sebagai seorang anak, ibu, dan istri, ada orang-orang yang harus kita rawat. Gimana kita bisa merawat orang lain kalau kita gak memulai dengan merawat diri sendiri,&rdquo; ucapnya. Ia juga menjelaskan bahwa dirinya&nbsp; telah menjaga kesehatan diri sejak umur 30 mulai dari tidak memforsir fisik, cukup istirahat, menjaga pola makan, serta rutin berolahraga.</span></span></span></p>\r\n\r\n<p><img alt=\"Wanda Hamidah menceritakan pengalaman merawat kulitnya\" class=\"img-responsive lazy newsleft\" loading=\"lazy\" src=\"/assets/xdfxcd/vendor/kcfinder/upload/files/6B111018-F631-46FC-9C2E-D6C89A643D12.jpeg\" style=\"height:auto; width:auto\"></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Untuk mempertahankan kesehatan kulitnya, Wanda mengaku rutin lakukan perawatan sebagai tindakan pencegahan sebelum masalah kulit muncul seperti injeksi DNA Salmon, IPL Hair Removal, dan PRP. &ldquo;Itu sudah terasa untuk regenerasi kulit saya. Jadi walaupun masuk ambang usia, tapi tidak kalah dengan ponakan,&quot; ia menambahkan.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Selain itu, Dokter Prinsa memberikan edukasi tentang treatment akupuntur yang diadaptasi dari kedokteran Cina lalu masuk ke Indonesia melalui integrasi dengan pengobatan barat dan dibuat menjadi akupuntur medik. </span></span></span></p>\r\n\r\n<p><img alt=\"dr. Prinsa Raudha Acna, Sp.AK memberikan edukasi tentang treatment akupuntur\" class=\"img-responsive lazy newsleft\" loading=\"lazy\" src=\"/assets/xdfxcd/vendor/kcfinder/upload/files/5670812D-E213-4DBA-BAE7-61F5A4B0505A.jpeg\" style=\"height:auto; width:auto\"></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">&ldquo;Akupunktur dapat dilakukan untuk tujuan kecantikan dengan cara memasukkan jarum ke jaringan kulit lalu masuk ke elastin untuk menambah kekenyalan kulit,&rdquo; ujarnya. Spesialis akupunktur tersebut juga menjelaskan bahwa akupunktur dapat menghalangi rasa nyeri yang umumnya terjadi di punggung dan pinggang, serta mengurangi kadar asam lambung untuk mengatasi maag dan migrain.</span></span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Prossi Clinic selalu berkomitmen untuk menjadi sahabat dalam mencintai diri sendiri dengan cara mensyukuri dan mengoptimalkan tubuh pemberian Tuhan tanpa mengubah bentuk dan fungsinya &mdash; tanpa harus mempedulikan stereotip masyarakat tentang standar-standar kecantikan.</span></span></span></p>\r\n\r\n<p><br>\r\n	&nbsp;</p>\r\n', '', '206d33f1a06743e,21e64f9536f34e0', 1, 1, NULL, NULL, NULL, NULL, '2022-10-13 11:43:22', '2022-10-24 13:09:20', 'inessytika', 'zul'),
('22a68240a61494f', 'promo-agustus-bundling-----', 'PROMO AGUSTUS BUNDLING 🎉', '', '23ca7b4c66336a4.jpeg', '<p>🎉&nbsp;<strong>Promo Agustus&nbsp;Bundling&nbsp;</strong>🎉</p>\r\n\r\n<p>Setiap ambil treatment minimal Rp199,000 kamu berhak memilih tambahan bundling treatment dengan harga spesial, diantaranya:</p>\r\n\r\n<p>Light Therapy Rp49,000</p>\r\n\r\n<p>Hydro/Microdermabrasion Rp49,000</p>\r\n\r\n<p>Infuse whitening Rp149,000</p>\r\n\r\n<p>Fatburn Wave / Ultra Fatburn Rp149,000</p>\r\n\r\n<p>Acne Injection (per 0,5 ml, untuk 3-5 titik) Rp169,000</p>\r\n\r\n<p>IPL Rejuvenation Rp199,000</p>\r\n\r\n<p>Brightening laser&nbsp; Rp299,000</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Setiap ambil treatment minimal Rp299,000 kamu berhak memilih tambahan bundling treatment dengan harga spesial, diantaranya:</p>\r\n\r\n<p>Lip rejuvenation Rp99,000</p>\r\n\r\n<p>Peeling Radiant Glow&nbsp; Rp109,000</p>\r\n\r\n<p>Meso Slimming Lite Rp149,000</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Yuk, buruan reservasi sekarang juga karena promo ini hanya berlangsung sampai 31 Agustus&nbsp;2023!&nbsp;</p>\r\n\r\n<p><a href=\"https://wa.link/iyobez\">Reservasi Sekarang</a></p>\r\n', '', 'promo', 1, 2, '0', NULL, NULL, NULL, '2022-10-06 14:38:14', '2023-08-03 14:27:27', 'inessytika', 'inessytika'),
('22e5243ef5d7c39', '----------merdeka-flash-sale-------', '🇮🇩  MERDEKA FLASH SALE ⚡️', '', '23edd779c5afb17.jpeg', '<p><strong>🇮🇩 &nbsp;MERDEKA&nbsp;Flash Sale&nbsp;</strong></p>\r\n\r\n<p>Bulan ini kamu bebas pilih treatment yang kamu mau hanya dengan membayar mulai dari&nbsp;90ribuan kamu udah bisa treatment di Prossi Clinic,&nbsp;karena di Prossi Clinic lagi ada promo super spesial&nbsp;potongan harga sampai 80%+ +! 😱</p>\r\n\r\n<p>Ini dia pilihan treatmentnya:</p>\r\n\r\n<p>Light Therapy <s>Rp573,000</s> &gt;&gt; Rp99,000</p>\r\n\r\n<p>Peeling Mata <s>Rp695,750</s> &gt;&gt; Rp157,800</p>\r\n\r\n<p>Lip rejuvenation <s>Rp399,000</s> &gt;&gt; Rp214,780</p>\r\n\r\n<p>Peeling Ketiak&nbsp; <s>Rp878,000</s> &gt;&gt; Rp224,780</p>\r\n\r\n<p>Peeling Radiant Glow&nbsp; <s>Rp898,000</s> &gt;&gt; Rp224,780</p>\r\n\r\n<p>Acne Injection (per 0,5 ml, untuk 3-5 titik) <s>Rp925,000</s> &gt;&gt; Rp234,780</p>\r\n\r\n<p>Meso slimming <s>Rp979,000</s> &gt;&gt; Rp278,000</p>\r\n\r\n<p>Infuse whitening <s>Rp632,500</s> &gt;&gt; Rp257,800</p>\r\n\r\n<p>IPL Rejuvenation <s>Rp1,199,000</s> &gt;&gt; Rp291,780</p>\r\n\r\n<p>IPL Acne Treatment* <s>Rp1,199,000</s> &gt;&gt; Rp291,780</p>\r\n\r\n<p>Peeling Platinum Melasma <s>Rp947,500</s> &gt;&gt; Rp314,780</p>\r\n\r\n<p>Peeling Platinum Brightening <s>Rp959,500</s> &gt;&gt; Rp314,780</p>\r\n\r\n<p>Peeling Platinum Acne <s>Rp947,500</s> &gt;&gt; Rp324,780</p>\r\n\r\n<p>Peeling body (per area) <s>Rp1,975,000</s> &gt;&gt; Rp347,800</p>\r\n\r\n<p>IPL Vaskular (spider vein removal)* <s>Rp1,152,690</s> &gt;&gt; Rp364,780</p>\r\n\r\n<p>Brightening laser&nbsp; <s>Rp2,100,000</s> &gt;&gt; Rp397,800</p>\r\n\r\n<p>Scar Subsicion <s>Rp674,500</s> &gt;&gt; Rp467,800</p>\r\n\r\n<p>Scar Removal + Light Therapy <s>Rp674,500</s> &gt;&gt; Rp467,800</p>\r\n\r\n<p>Meso brightening <s>Rp979,000</s> &gt;&gt; Rp547,800</p>\r\n\r\n<p>Hair Grow Booster Extra <s>Rp1,850,000</s> &gt;&gt; Rp647,800</p>\r\n\r\n<p>PRP + Light therapy <s>Rp2,400,000</s> &gt;&gt; Rp747,800</p>\r\n\r\n<p>New DNA Salmon Magnificent <s>Rp2,950,000</s> &gt;&gt; Rp947,800</p>\r\n\r\n<p>*tidak tersedia di Malang</p>\r\n\r\n<p>Yuk, buruan reservasi sekarang juga karena promo ini hanya berlangsung sampai 31 Agustus&nbsp;2023!&nbsp;</p>\r\n\r\n<p><a href=\"https://wa.link/8jrol4\"><strong>Reservasi Sekarang</strong></a></p>\r\n', '', 'promo', 1, 2, '0', NULL, NULL, NULL, '2022-10-06 14:42:56', '2023-08-03 14:11:59', 'inessytika', 'inessytika');

-- --------------------------------------------------------

--
-- Table structure for table `t_comment`
--

CREATE TABLE `t_comment` (
  `KodeKomen` varchar(15) NOT NULL,
  `KodeArtikel` varchar(15) NOT NULL,
  `Nama` varchar(25) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Website` varchar(60) NOT NULL,
  `IsiKomen` text NOT NULL,
  `Tanggal` datetime NOT NULL,
  `HasSubKomen` int(11) NOT NULL,
  `StatusKomen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `t_comment`
--

INSERT INTO `t_comment` (`KodeKomen`, `KodeArtikel`, `Nama`, `Email`, `Website`, `IsiKomen`, `Tanggal`, `HasSubKomen`, `StatusKomen`) VALUES
('216022ac54e026d', '2154d375e8364a4', 'Zul', 'zulfirman098@gmail.com', '', 'Tes', '2021-02-09 22:37:56', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_gambar`
--

CREATE TABLE `t_gambar` (
  `KodeGambar` varchar(15) NOT NULL,
  `MainImage` varchar(20) NOT NULL,
  `ContentId` text DEFAULT NULL,
  `ContentEn` text DEFAULT NULL,
  `Lainnya1` text DEFAULT NULL,
  `Lainnya2` varchar(255) DEFAULT NULL,
  `Lainnya3` text DEFAULT NULL,
  `Tipe` smallint(6) NOT NULL,
  `Kategori` varchar(155) DEFAULT NULL,
  `Tanggal` date DEFAULT NULL,
  `DateCreated` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `t_gambar`
--

INSERT INTO `t_gambar` (`KodeGambar`, `MainImage`, `ContentId`, `ContentEn`, `Lainnya1`, `Lainnya2`, `Lainnya3`, `Tipe`, `Kategori`, `Tanggal`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`) VALUES
('1', '21dce62a18a1a94.png', '', '<h3><span style=\"color:#3498db\">#1 Plastic Surgery Clinic</span></h3>\r\n\r\n<h1><span style=\"color:#ffffff\">Love the new you</span></h1>\r\n\r\n<p><span style=\"color:#ffffff\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus.</span></p>\r\n', '/assets/xdfxcd/vendor/kcfinder/upload/files/(Compressed%20)Highlight%20Video%20Company%20Profile%20Prossi%20intro_480p.mp4', '', NULL, 1, NULL, NULL, '2021-04-26 16:04:34', '2021-11-15 10:52:17', 'zulfirman098', 'zul'),
('2', '21a90c97e0a3999.jpg', '<h3><strong>Welcome to Prossi Clinic</strong></h3>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\"><span style=\"font-size:11pt\"><span style=\"color:#000000\">Prossi&nbsp;Clinic merupakan klinik kecantikan yang menangani perawatan kulit dan kecantikan serta pelangsingan tubuh secara profesional karena ditangani langsung oleh dokter spesialis yang berpengalaman di bidangnya.</span></span></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\"><span style=\"font-size:11pt\"><span style=\"color:#000000\">Prossi&nbsp;Clinic berdiri sejak 1 September&nbsp;2019 dengan lokasi pertama di Tebet, Jakarta Selatan.&nbsp;Pelayanan yang diberikan berupa pelayanan jasa konsultasi dokter, tindakan, serta penyediaan produk untuk perawatan kulit dan program pelangsingan yang aman.</span></span></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-family:Trebuchet MS,Helvetica,sans-serif\"><span style=\"font-size:11pt\"><span style=\"color:#000000\">Dengan menerapkan harga yang terjangkau untuk setiap pelayanannya, Prossi&nbsp;Clinic ingin menyampaikan pesan bahwa untuk menjadi lebih cantik, langsing dan sehat tidak perlu mengeluarkan biaya banyak. Walaupun dengan harga terjangkau, hasil dan perawatan yang didapatkan tetap maksimal</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '<h4>This is Dr Pro</h4>\r\n\r\n<h1>Welcome to our Clinic<img alt=\"\" class=\"img-responsive lazy\" loading=\"lazy\" src=\"/21_prossi/assets/xdfxcd/vendor/kcfinder/upload/files/intro_1.jpg\" style=\"float:right; height:298px; margin-left:20px; margin-right:20px; width:200px\"></h1>\r\n\r\n<p style=\"text-align:justify\">Integer aliquet congue libero, eu gravida odio ultrces ut. Etiam ac erat ut enim maximus accumsan vel ac nisl. Duis feugiat bibendum orci, non elementum urna vestibulum in. Nulla facilisi.Nulla egestas vel lacus sed interdum. Sed mollis, orci elementum. Etiam ac erat ut enim maximus accumsan vel acnisl. Duis feugiat bibendum orci, non elementum urna vestibulum in. Nulla facilisi. Nulla egestas vel lacus sed interdum.</p>\r\n\r\n<p style=\"text-align:justify\">Integer aliquet congue libero, eu gravida odio ultrces ut. Etiam ac erat ut enim maximus accumsan vel ac nisl. Duis feugiat bibendum orci, non elementum urna vestibulum in. Nulla facilisi.Nulla egestas vel lacus sed interdum. Sed mollis, orci elementum. Etiam ac erat ut enim maximus accumsan vel acnisl. Duis feugiat bibendum orci, non elementum urna vestibulum in. Nulla facilisi. Nulla egestas vel lacus sed interdum.</p>\r\n', '[{\"Id\":\"63589054b2718\",\"CabangImage\":\"\\/assets\\/xdfxcd\\/vendor\\/kcfinder\\/upload\\/files\\/Shared%20from%20Lightroom%20mobile2.jpg\",\"NamaCabang\":\"Prossi Clinic - Kebayoran\"},{\"Id\":\"63589054b271a\",\"CabangImage\":\"\\/assets\\/xdfxcd\\/vendor\\/kcfinder\\/upload\\/files\\/Shared%20from%20Lightroom%20mobile.jpg\",\"NamaCabang\":\"Prossi Clinic - Tebet\"}]', '', NULL, 2, NULL, NULL, '2021-04-26 16:04:34', '2022-10-26 08:41:40', 'zulfirman098', 'zul'),
('21070e571c38071', '213a55ac37ad1f1.gif', 'Ditangani oleh Profesional', '', 'Dengan dokter spesialis sebagai penanggung jawab, semua perawatan dipastikan sudah memenuhi standar operasional kesehatan nasional', '', NULL, 5, NULL, NULL, '2021-05-24 09:25:34', '2021-09-13 10:08:33', 'zul', 'inessytika'),
('210b8c32a569c10', '210b8c32a569c10.gif', NULL, NULL, 'dr. Melyarna Putri SpGK', 'Dokter Spesialis Gizi Prossi - Tebet', '<p>Praktek di Prossi - Tebet.</p>\r\n\r\n<p>Jadwal sesuai perjanjian&nbsp;</p>\r\n\r\n<p><a href=\"https://wa.link/6eha7h\">Reservasi di sini</a></p>\r\n', 3, NULL, NULL, '2021-09-18 16:48:12', '2022-02-24 11:18:59', 'inessytika', 'inessytika'),
('210f292e02266ae', '2105f3623282b8b.png', 'Aman', '', 'Selain ruangan yang disterilisasi secara berkala, seluruh karyawan juga selalu diperhatikan kesehatannya demi menjaga keamanan pasien saat berkunjung', '', '5', 5, NULL, NULL, '2021-05-24 09:02:54', '2021-09-15 16:06:38', 'zul', 'farizan'),
('212d5dcbc06b441', '212d5dcbc06b441.gif', NULL, NULL, 'dr. Pretty Grace Zalukhu', 'Dokter Prossi - Kebayoran', '<p style=\"text-align:center\">Praktek di Prossi - Kebayoran</p>\r\n\r\n<p style=\"text-align:center\">Jadwal Praktek Kebayoran</p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">Kamis</td>\r\n			<td style=\"text-align:center\">09.30 - 17.00 WIB</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align:center\">Sabtu</td>\r\n			<td style=\"text-align:center\">09.30 - 17.00 WIB</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align:center\">Minggu</td>\r\n			<td style=\"text-align:center\">09.30 - 17.00 WIB</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><a href=\"https://wa.link/c0y4yh\">Reservasi di sini</a></p>\r\n', 3, NULL, NULL, '2021-09-18 17:33:20', '2022-02-24 11:06:58', 'inessytika', 'inessytika'),
('213161178dfa536', '21e97819a4926a3.gif', NULL, NULL, 'dr. Stevy Harman', 'Dokter Prossi - Kebayoran', '<p>Praktek di Prossi - Kebayoran</p>\r\n\r\n<p>Jadwal Praktek Kebayoran</p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Senin</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Selasa</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Minggu</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><a href=\"https://wa.link/lsoers\">Reservasi di sini</a></p>\r\n', 3, NULL, NULL, '2021-09-18 17:17:44', '2022-02-24 11:16:15', 'inessytika', 'inessytika'),
('2132fdcf09d04fb', '2132fdcf09d04fb.jpg', NULL, NULL, 'dr. Erika Nurhandayani Zoulba, Sp.KK', 'Dokter Spesialis Kulit & Kelamin Prossi - Kebayoran', '<p>Praktek di Prossi - Kebayoran</p>\r\n\r\n<p><strong>Selasa</strong> 13.00-16.00 WIB</p>\r\n\r\n<p><strong>Sabtu</strong> 11.30-14.30 WIB</p>\r\n\r\n<p>atau sesuai perjanjian</p>\r\n\r\n<p><a href=\"https://wa.link/qrgdcp\">Reservasi di sini</a></p>\r\n', 3, NULL, NULL, '2021-10-25 09:02:10', '2022-02-24 11:05:14', 'inessytika', 'inessytika'),
('213608ef416d3f6', '213608ef416d3f6.png', NULL, NULL, 'Fidha Riani', 'Blogger', 'Sejujurnya aku gak pernah perawatan wajah. Dan ini perawatan pertama aku karena emang kerjaannya naik gunung terpapar matahari terus. Tapi di sini tuh enak banget ruangannya steril. Dokter dan perawatnya pun sangat lembut memberikan pengetahuan baru buat aku', 4, NULL, NULL, '2021-06-08 11:48:04', NULL, 'farizan', NULL),
('2137fdcd319ae41', '213ec89cf538975.gif', 'Nyaman', '', 'Tersedia ruangan yang bersih dan nyaman untuk dikunjungi. Juga menjamin kenyamanan pasien dengan meminimalisir rasa sakit saat perawatan', '', '3', 5, NULL, NULL, '2021-05-24 09:11:27', '2021-09-18 09:25:41', 'zul', 'inessytika'),
('213c28edf39f400', '2133ec76943e526.gif', 'Harga Terbaik', '', 'Memberikan perawatan terbaik dengan harga bersaing. Selalu ada penawaran menarik setiap bulannya', '', '4', 5, NULL, NULL, '2021-05-24 09:04:52', '2021-09-18 09:25:27', 'zul', 'inessytika'),
('213f7539f7665be', '213f7539f7665be.png', NULL, NULL, 'Fauzan', 'Business Man', 'The best sih! Pelayanannya Prossi Clinic, dokter & sus ramah, dan super super cozy tempatnya dan higienis parah!', 4, NULL, NULL, '2021-06-08 11:49:36', NULL, 'farizan', NULL),
('2158f50f27755c1', '21978df1052f310.gif', NULL, NULL, ' dr. Siti Nurani SpKK', 'Dokter Spesialis Kulit & Kelamin Prossi - Tebet', '<p><strong>Praktik di Prossi - Tebet</strong></p>\r\n\r\n<p>Selasa 13.00-15.00 WIB</p>\r\n\r\n<p>atau sesuai perjanjian</p>\r\n\r\n<p><a href=\"https://wa.link/lowlis\">Reservasi di sini</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', 3, NULL, NULL, '2021-05-03 11:48:22', '2022-02-24 11:56:03', 'zul', 'inessytika'),
('2160c77355c5e8b', '2160c77355c5e8b.png', NULL, NULL, 'Rinanti Astari', 'Guru', 'Seneng banget abis peeling + laser di Prossi Clinic, udah cuma skincare plus sunblock aja glowing banget', 4, NULL, NULL, '2021-06-08 11:46:12', NULL, 'farizan', NULL),
('217bdb4a84d295c', '21032029712e006.gif', NULL, NULL, 'dr. Mifta Wirasweti', 'Dokter Prossi - Tebet', '<p style=\"text-align:center\">Praktek di Prossi - Tebet</p>\r\n\r\n<p style=\"text-align:center\">Jadwal Praktek</p>\r\n\r\n<div class=\"table-responsive\">\r\n<table class=\"table table-bordered\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Rabu</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Jumat</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n\r\n<p><a href=\"https://wa.link/3c82kn\">Reservasi di sini</a></p>\r\n', 3, NULL, NULL, '2021-04-30 16:35:33', '2022-02-24 11:57:46', 'zul', 'inessytika'),
('21820af7ca7ead3', '21820af7ca7ead3.png', NULL, NULL, 'Yenny Kusumawardani', 'Entrepreneur', 'Gegara promo terus jadi moving dari klinik skincare sebelah', 4, NULL, NULL, '2021-06-08 11:47:12', NULL, 'farizan', NULL),
('21876460d5ee03b', '2106db2a25fc84e.gif', NULL, NULL, 'dr. Theresia Silalahi', 'Dokter Prossi - Tebet dan Kebayoran', '<p style=\"text-align:center\">Praktek di Prossi - Tebet dan Kebayoran</p>\r\n\r\n<p style=\"text-align:center\">Jadwal Praktek Tebet</p>\r\n\r\n<div class=\"table-responsive\">\r\n<table class=\"table table-bordered\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Hari</td>\r\n			<td>Pukul</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Senin</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Kamis</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sabtu</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"text-align:center\">Jadwal Praktek Kebayoran</p>\r\n\r\n<div class=\"table-responsive\">\r\n<table class=\"table table-bordered\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Hari</td>\r\n			<td>Pukul</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Rabu</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Jumat</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n\r\n<p><a href=\"https://wa.link/4880ut\">Reservasi di&nbsp;</a><a href=\"https://wa.link/i6qczq\">sini</a></p>\r\n', 3, NULL, NULL, '2021-04-30 16:29:55', '2022-02-24 11:58:35', 'zul', 'inessytika'),
('219828ab9db775d', '219828ab9db775d.gif', NULL, NULL, 'dr. Claresta Diella SpGK', 'Dokter Spesialis Gizi Prossi - Kebayoran', '<p>Praktek di Prossi - Kebayoran</p>\r\n\r\n<p><strong>Senin</strong> 13.00-17.00 WIB</p>\r\n\r\n<p>atau sesuai perjanjian&nbsp;</p>\r\n\r\n<p><a href=\"https://wa.link/bdhvgy\">Reservasi di sini</a></p>\r\n', 3, NULL, NULL, '2021-09-18 16:42:15', '2022-02-24 11:52:56', 'inessytika', 'inessytika'),
('21d16f678bba261', '21d16f678bba261.gif', NULL, NULL, 'dr. Aulia Ajrina', 'Dokter Prossi - Tebet', '<p><strong>Praktik di Prossi - Tebet</strong></p>\r\n\r\n<p>Jadwal Praktik Tebet</p>\r\n\r\n<div class=\"table-responsive\">\r\n<table class=\"table table-bordered table-condensed table-hover table-striped\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Rabu</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Jumat</td>\r\n			<td>09.30 - 17.00 WIB</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n\r\n<p><a href=\"https://wa.link/bs6fau\">Reservasi di sini</a></p>\r\n', 3, NULL, NULL, '2021-09-18 16:23:43', '2022-02-24 11:54:23', 'inessytika', 'inessytika'),
('21d3b972f4c7a1a', '2104365266ea507.gif', 'Hasil Nyata', '', 'Memberikan solusi dan hasil perawatan yang nyata karena didukung oleh peralatan berkualitas dan tenaga kesehatan berpengalaman', '', '2', 5, NULL, NULL, '2021-05-24 09:11:55', '2021-09-18 09:25:52', 'zul', 'inessytika'),
('21d9290c726c3de', '21d9290c726c3de.png', NULL, NULL, 'Yuni Purwati', 'Ibu Rumah Tangga', 'Pertama kalinya treatment di Prossi Clinic, enak tempatnya, dan pelayanannya juga bagus. Thanks Prossi Clinic!', 4, NULL, NULL, '2021-06-08 11:48:53', NULL, 'farizan', NULL),
('21d9566b82b9fe9', '21d9566b82b9fe9.png', NULL, NULL, 'Rizka Qurniati', 'Ibu Rumah Tangga', 'Selalu senang balik ke sini lagi. Banyak promo dan dokter sama susternya ramah-ramah banget!', 4, NULL, NULL, '2021-06-08 11:44:56', NULL, 'farizan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_notifikasi`
--

CREATE TABLE `t_notifikasi` (
  `IdNotification` varchar(15) NOT NULL,
  `Type` smallint(6) NOT NULL,
  `Content` varchar(155) NOT NULL,
  `Idnya` varchar(155) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `IsRead` tinyint(1) NOT NULL,
  `IsOpen` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `t_notifikasi`
--

INSERT INTO `t_notifikasi` (`IdNotification`, `Type`, `Content`, `Idnya`, `DateAdded`, `IsRead`, `IsOpen`) VALUES
('215ff51eb8d1f31', 1, '08988769398 mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:21:44', 1, 0),
('215ff51ebe30e76', 1, '08988769398 mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:21:50', 1, 0),
('215ff51ec30f7f4', 1, '08988769398 mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:21:55', 1, 0),
('215ff524871679b', 1, 'Mira mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:46:31', 1, 0),
('215ff524f1145b4', 1, 'Kalsa mengomentari Pita Putih Indonesia Dukung Keselamatan Ibu Sehat', 'pita-putih-indonesia-dukung-keselamatan-ibu-sehat', '2021-01-06 09:48:17', 1, 0),
('215ff525ac28b69', 1, '08988769398assas mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:51:24', 1, 0),
('215ff525b170d45', 1, '08988769398 mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:51:29', 1, 0),
('215ff525b69743b', 1, '08988769398 mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:51:34', 1, 0),
('215ff525ba6b077', 1, '08988769398 mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:51:38', 1, 0),
('215ff525c2db07d', 1, '08988769398 mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:51:46', 1, 0),
('215ff525c66319c', 1, '08988769398 mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:51:50', 1, 0),
('215ff525cbd505e', 1, '08988769398 mengomentari Menteri PPPA Menerima Kunjungan 2', 'menteri-pppa-menerima-kunjungan-2', '2021-01-06 09:51:55', 1, 0),
('215ffd148637c05', 1, 'asfas mengomentari Pita Putih Indonesia Dukung Keselamatan Ibu Sehat', 'pita-putih-indonesia-dukung-keselamatan-ibu-sehat', '2021-01-12 10:16:22', 1, 0),
('216022ac54e18df', 1, 'Zul mengomentari Mongolia menerima hibah baru senilai $ 23,1 juta dari GCF untuk memperkuat ketahanan iklim', 'mongolia-menerima-hibah-baru-senilai---23-1-juta-dari-gcf-untuk-memperkuat-ketahanan-iklim', '2021-02-09 22:37:56', 1, 0),
('21609b6992a737b', 1, 'Zulfirman mengomentari Mongolia menerima hibah baru senilai $ 23,1 juta dari GCF untuk memperkuat ketahanan iklim', 'mongolia-menerima-hibah-baru-senilai---23-1-juta-dari-gcf-untuk-memperkuat-ketahanan-iklim', '2021-05-12 12:37:22', 1, 0),
('21609b69a995505', 1, 'Zulfirman mengomentari Mongolia menerima hibah baru senilai $ 23,1 juta dari GCF untuk memperkuat ketahanan iklim', 'mongolia-menerima-hibah-baru-senilai---23-1-juta-dari-gcf-untuk-memperkuat-ketahanan-iklim', '2021-05-12 12:37:45', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_sejarah`
--

CREATE TABLE `t_sejarah` (
  `KodeGambar` varchar(15) NOT NULL,
  `MainImage` varchar(20) NOT NULL,
  `Content` text NOT NULL,
  `Language` char(5) NOT NULL,
  `Tanggal` date NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_sub_comment`
--

CREATE TABLE `t_sub_comment` (
  `KodeSubKomen` varchar(15) NOT NULL,
  `KodeKomen` varchar(15) NOT NULL,
  `KodeArtikel` varchar(15) NOT NULL,
  `Nama` varchar(25) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Website` varchar(60) NOT NULL,
  `IsiKomen` text NOT NULL,
  `Tanggal` datetime NOT NULL,
  `StatusKomen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `t_sub_comment`
--

INSERT INTO `t_sub_comment` (`KodeSubKomen`, `KodeKomen`, `KodeArtikel`, `Nama`, `Email`, `Website`, `IsiKomen`, `Tanggal`, `StatusKomen`) VALUES
('21609b69a991b42', '216022ac54e026d', '2154d375e8364a4', 'Zulfirman', 'zulfirman098@gmail.com', '', '@Zul Tes Reply', '2021-05-12 12:37:45', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_kategori`
--
ALTER TABLE `m_kategori`
  ADD PRIMARY KEY (`KodeKategori`),
  ADD UNIQUE KEY `NamaKategori` (`NamaKategori`);

--
-- Indexes for table `m_kategori_user`
--
ALTER TABLE `m_kategori_user`
  ADD PRIMARY KEY (`KodeKategoriUser`);

--
-- Indexes for table `m_navigasi`
--
ALTER TABLE `m_navigasi`
  ADD PRIMARY KEY (`KodeNavigasi`) USING BTREE;

--
-- Indexes for table `m_pengaturan`
--
ALTER TABLE `m_pengaturan`
  ADD PRIMARY KEY (`KodePengaturan`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`KodeUser`);

--
-- Indexes for table `t_anggota`
--
ALTER TABLE `t_anggota`
  ADD PRIMARY KEY (`KodeAnggota`);

--
-- Indexes for table `t_artikel`
--
ALTER TABLE `t_artikel`
  ADD PRIMARY KEY (`KodeArtikel`);

--
-- Indexes for table `t_comment`
--
ALTER TABLE `t_comment`
  ADD PRIMARY KEY (`KodeKomen`),
  ADD KEY `It_comment` (`KodeArtikel`);

--
-- Indexes for table `t_gambar`
--
ALTER TABLE `t_gambar`
  ADD PRIMARY KEY (`KodeGambar`);

--
-- Indexes for table `t_notifikasi`
--
ALTER TABLE `t_notifikasi`
  ADD PRIMARY KEY (`IdNotification`);

--
-- Indexes for table `t_sejarah`
--
ALTER TABLE `t_sejarah`
  ADD PRIMARY KEY (`KodeGambar`);

--
-- Indexes for table `t_sub_comment`
--
ALTER TABLE `t_sub_comment`
  ADD PRIMARY KEY (`KodeSubKomen`),
  ADD KEY `It_sub_comment` (`KodeKomen`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_navigasi`
--
ALTER TABLE `m_navigasi`
  MODIFY `KodeNavigasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
