<?php
function cekajax(){
	if ($GLOBALS['env']['projectType'] === "production")
		if (!isset($_POST) or !isset($_SERVER['HTTP_X_REQUESTED_WITH']) or strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
			header('HTTP/1.0 403 Forbidden');
			header('Content-Type: application/json');
			echo json_encode(array(
				"statusCode" => 403,
				'text' => 'Fck off',
				'type' => '???',
				'ip' => getMyIp()
			));
			exit;
		}
}

function cleanTitle($string){
	$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	$string = preg_replace('/-+/', '-', $string);// Replaces multiple hyphens with single one.
	if (substr($string, -1) === '-') {
		$string = substr_replace($string, "", -1);
	}
	return strtolower($string);

}

function getPengaturan(){
	$ci = get_instance();
	return $ci->cache->model('view_model', 'pengaturan');
}

function listNavigasi($isFront = 0){
	$ci = get_instance();
	if ($isFront === 0) {
		return $ci->cache->model('m_navigasi', 'listNavigasi', array($_SESSION['s_tipeuser'], $_SESSION['s_trustee']));
	} else {
		$ci->load->model('m_navigasi');
		return $ci->m_navigasi->listNavigasi(2, '');
	}
}

function notifSukses($Text = 0, $Tipe = 0){
	if ($Text === 0) {
		$Text = 'Sukses Menyimpan Data';
	}
	if ($Tipe === 0) {
		$Tipe = 'success';
	}
	echo "<div class=\"alert alert-$Tipe alert-dismissible fade show\" role=\"alert\" style=\"text-align: center;\">
							<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
								<span aria-hidden=\"true\"><i class=\"fal fa-times\"></i></span>
							</button>
							<strong>$Text.</strong>
						</div>";
}

function cTUp($a = ''){
	if ($a !== '') {
		$a = $a . '.';
	}
	return 'concat(' . $a . 'DateCreated," | ", ' . $a . 'CreatedBy) as Ct,concat(' . $a . 'DateUpdated," | ", ' . $a . 'UpdatedBy) as Up';
}

function uploadGambar($uniqId, $name = 'MainImage'){
	$ci = get_instance();
	$_link = './assets/uploads/';
	$config['upload_path'] = $_link;
	$config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|PNG';
	$config['file_name'] = $uniqId;
	$config['overwrite'] = true;
	$config['max_size'] = 4242; // 4MB
	$ci->load->library('upload', $config);
	if ($ci->upload->do_upload($name)) {
		return $ci->upload->data("file_name");
	} else {
		//echo $ci->upload->display_errors();exit;
		return "default.jpg";
	}
}

function editGambar($po, $nameInput = 'MainImage', $name = 'OldImage'){
	$oldImage = $po[$name];
	if (!empty($_FILES[$nameInput]['name'])) {
		if ($oldImage !== 'default.jpg' && $oldImage !== 'icon_4.svg') {
			if (file_exists("./assets/uploads/$oldImage")) {
				unlink("./assets/uploads/$oldImage");
			}
		}
		$val = uploadGambar(myUniqId(), $nameInput);
	} else {
		$val = $oldImage;
	}
	return $val;
}


function uploadCrop($uniqId, $name, $resolution){
	//$resolution is for width and height
	//print_r($_FILES);exit;
	$ci = get_instance();
	$_link = './assets/uploads/';
	$config['upload_path'] = $_link;
	$config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|PNG';
	$config['file_name'] = $uniqId;
	$config['overwrite'] = true;
	$config['max_size'] = 4242; // 4MB
	$ci->load->library('upload', $config);
	$doUpload = $ci->upload->do_upload($name);
	if ($doUpload) {
		$file_data = $ci->upload->data();
		$resize['image_library'] = 'gd2';
		$resize['maintain_ratio'] = FALSE;
		$resize['width'] = $resolution[0];
		$resize['height'] = $resolution[1];
		$resize['source_image'] = $file_data['full_path'];
		$ci->load->library('image_lib', $resize);
		if (!$ci->image_lib->resize()) {
			//echo $ci->image_lib->display_errors();exit;
		}
		return $ci->upload->data("file_name");
	} else {
		echo $ci->upload->display_errors();exit;
		return "default.jpg";
	}
}

function editCrop($po, $nameInput, $oldImageInput, $resolution){
	$oldImage = $po[$oldImageInput];
	if (!empty($_FILES[$nameInput]['name'])) {
		if ($oldImage !== 'default.jpg' && $oldImage !== 'icon_4.svg') {
			if (file_exists("./assets/uploads/$oldImage")) {
				unlink("./assets/uploads/$oldImage");
			}
		}
		$result = uploadCrop(myUniqId(), $nameInput, $resolution);
	} else {
		$result = $oldImage;
	}
	return $result;
}

function cekAkses($Trustee){
	$isContain = 0;
	if (isset($_SESSION['s_tipeuser']) && isset($_SESSION['s_trustee'])) {
		foreach ($Trustee as $Trusteev) {
			if (in_array($Trusteev, $_SESSION['s_trustee'])) {
				$isContain = 1;
			}
		}
	}
	if ($isContain === 0) {
		$ci = get_instance();
		echo $ci->load->view("errors/403.php", '', true);
		exit;
	}
}

function todec($x){
	if (isWhite($x)) {
		return 0;
	}
	return str_replace(',', '.', $x);
}

function rdes($x){
	//mengubah angka ke desimal
	if ($x === "" || $x === null) {
		return 0;
	}
	return (int)saniz(str_replace(' ', '', str_replace('Rp', '', str_replace('.', '', str_replace(',', '', $x)))));
}

function rupi($x){
	// mengubah angka ke rupiah
	return 'Rp. ' . str_replace(',', '.', number_format($x));
}

function rsep($x){
	// mengubah angka ke rupiah
	return str_replace(',', '.', number_format($x));
}

function deleteDbCaches(){
	foreach (array('database', 'pages') as $folder) {
		$dirPath = APPPATH . 'cache/' . $folder . '/';
		rrmdir($dirPath);
		mkdir($dirPath, 0777, true);
	}
}

function deleteViewCaches(){
	foreach (array('Id', 'En') as $bahasa) {
		$dirPath = APPPATH . "cache/pages/$bahasa/view_model/";
		rrmdir($dirPath);
		mkdir($dirPath, 0777, true);
	}
}

function cet($str){
	return htmlentities($str, ENT_QUOTES, 'UTF-8');
}

function saniz($string){
	$string = str_replace(array('[\', \']'), '', $string);
	$string = preg_replace('/\[.*\]/U', '', $string);
	$string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
	$string = htmlentities($string, ENT_COMPAT, 'utf-8');
	$string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string);
	$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/'), ' ', $string);
	return filter_var($string, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}

function isWhite($text){
	return ctype_space($text) || $text === "" || $text === null;
}

function isInt($v){
	return ctype_digit($v) && (int)$v > 0;
}

function rrmdir($dir){
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (filetype($dir . "/" . $object) == "dir")
					rrmdir($dir . "/" . $object);
				else unlink($dir . "/" . $object);
			}
		}
		reset($objects);
		rmdir($dir);
	}
}

function toJson($a){
	header('Content-Type: application/json');
	echo json_encode($a);
}

function jsonSucces(){
	$ci = get_instance();
	return $ci->output
		->set_content_type('application/json')
		->set_status_header(200)
		->set_output(json_encode(array(
			"statusCode" => 200,
			'text' => 'Success 200',
			'type' => 'success',
		)));
}

function jsonError($text = 'Error 500'){
	$ci = get_instance();
	return $ci->output
		->set_content_type('application/json')
		->set_status_header(500)
		->set_output(json_encode(array(
			"statusCode" => 500,
			'text' => $text,
			'type' => 'danger',
			'ip' => getMyIp()
		)));
}

function json403(){
	header('HTTP/1.0 403 Forbidden');
	toJson(array(
		"statusCode" => 403,
		'text' => 'Error 403',
		'type' => 'warning',
		'ip' => getMyIp()
	));
	exit(0);
}

function getMyIp(){
	return getenv('HTTP_CLIENT_IP') ?:
		getenv('HTTP_X_FORWARDED_FOR') ?:
			getenv('HTTP_X_FORWARDED') ?:
				getenv('HTTP_FORWARDED_FOR') ?:
					getenv('HTTP_FORWARDED') ?:
						getenv('REMOTE_ADDR');
}

function inCsrf(){
	$ci = get_instance();
	$name = $ci->security->get_csrf_token_name();
	$value = $ci->security->get_csrf_hash();
	return "<input type='hidden' name='$name' value='$value'>";
}

function myFaktur(){
	$unq = substr(md5(uniqid(rand(), true)), 10, 13);
	$fi = substr($unq, 0, 7);
	$se = substr($unq, 7, 6);
	return strtoupper("$fi-$se");
}

function tglSekarang($tipe){
	if ($tipe === 1) {
		return date('Y-m-d');
	} else if ($tipe === 2) {
		return date('H:i:s');
	} else if ($tipe === 3) {
		return date('Y-m-d H:i:s');
	}
}

function myUniqId(){
	return date('y') . substr(md5(uniqid(rand(), true)), 10, 13);
}

function cleanJudul($string){
	$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	return strtolower($string); // Removes special chars.
}

function enableCors(){
	if (isset($_SERVER['HTTP_ORIGIN'])) {
		// Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
		// you want to allow, and if so:
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
	}

	// Access-Control headers are received during OPTIONS requests
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
			// may also be using PUT, PATCH, HEAD etc
			header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
	}
}

?>
