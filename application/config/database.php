<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$head=$GLOBALS['env'];
$database=$head['database'];
$hostname=$head['hostname'];
$username=$head['username'];
$password=$head['password'];

$active_group = 'default';
$query_builder = TRUE;
$db['default'] = array(
	'dsn' => '',
	'hostname' => $hostname,
	'username' => $username,
	'password' => $password,
	'database' => $database,
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => APPPATH . 'cache/database/',
	'char_set' => 'utf8mb4',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => TRUE,
	'failover' => array(),
	'save_queries' => TRUE
);
