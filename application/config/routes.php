<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = 'My404';
$route['translate_uri_dashes'] = TRUE;
//$route[''] = '_fr/home';

foreach (array('artikel','promo') as $val) {
	$route[$val] = "$val/home";
	$route["$val/(:num)"] = "$val/home";
	$route["$val/(:any)"] = "$val/$val"."_detail/$1";
}

$route['kasir/pos/meja'] = 'kasir/modul_pos/meja';
$route['kasir/pos/(:any)'] = 'kasir/modul_pos/pos/$1';

$route["admin"] = "admin/dashboard/index";


$route["pelayanan/(:any)"] = "pelayanan/main/$1";

$route["admin/kategori-service"] = "admin/master-kategori/index";
$route["admin/kategori-service/tambah"] = "admin/master-kategori/tambah";
$route["admin/kategori-service/edit/(:any)"] = "admin/master-kategori/edit/$1";
