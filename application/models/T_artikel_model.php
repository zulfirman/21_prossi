<?php
class T_artikel_model extends CI_Model
{
	/*LIST TIPE
	1 : Artikel
	2 : Acara
	*/
	function __construct()
	{
		parent::__construct();
	}

	/*
	 * Get t_artikel by KodeArtikel
	 */
	function get_t_artikel($KodeArtikel)
	{
		return $this->db->get_where('t_artikel',array('KodeArtikel'=>$KodeArtikel))->first_row();
	}

	function get_uniq($KodeArtikel, $Name)
	{
		if($KodeArtikel===1) {
			return $this->db->get_where('t_artikel', array('UrlArtikel' => $Name))->row_array();
		}
		else{
			$this->db->where('KodeArtikel <>', $KodeArtikel);
			$this->db->where('UrlArtikel', $Name);
			return $this->db->get('t_artikel')->row_array();
		}
	}

	/*
	 * Get all t_artikel
	 */
	function get_all_t_artikel($Tipe)
	{
		$this->db->select('*, '.cTUp());
		$this->db->where('Tipe', $Tipe);
		$this->db->order_by('DateCreated', 'desc');
		return $this->db->get('t_artikel')->result();
	}

	/*
	 * function to add new t_artikel
	 */
	function add_t_artikel($params)
	{
		$this->db->insert('t_artikel',$params);
		return $this->db->insert_id();
	}

	/*
	 * function to update t_artikel
	 */
	function update_t_artikel($KodeArtikel,$params)
	{
		$this->db->where('KodeArtikel',$KodeArtikel);
		return $this->db->update('t_artikel',$params);
	}

	// Fetch records
	public function getData($rowno,$rowperpage,$Tipe) {
		$post=$this->input->post();
		$searched=false;
		$this->db->select('a.KodeArtikel as myId, a.ContentId,a.ContentEn, a.UrlArtikel, a.JudulId, a.JudulEn, a.MainImage, a.DateCreated, b.NamaUser, b.Pekerjaan');
		$this->db->join('m_user b', 'a.CreatedBy = b.KodeUser','left');
		$this->db->where('a.Tipe', $Tipe);
		$this->db->order_by('a.DateCreated','desc');
		$this->db->limit($rowperpage, $rowno);
		$this->db->from('t_artikel a');
		$query = $this->db->get();
		return $query->result();
	}
	// Select total records
	public function getrecordCount($Tipe) {
		$post=$this->input->post();
		$searched=false;
		$this->db->select('count(Tipe) as allcount');
		if($searched==true){
			$this->db->like('Title', $post['Title'],'both');
		}
		$this->db->join('m_user b', 'a.CreatedBy = b.KodeUser','left');
		$this->db->where('a.Tipe', $Tipe);
		$this->db->from('t_artikel a');
		$query = $this->db->get();
		$result = $query->first_row();
		return $result->allcount;
	}
}
