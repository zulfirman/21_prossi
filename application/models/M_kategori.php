<?php
class M_kategori extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function getById($KodeKategori)
	{
		return $this->db->get_where('m_kategori',array('KodeKategori'=>$KodeKategori, 'IsActive' => 1))->first_row();
	}

	function getUnique($KodeKategori, $Name)
	{
		if($KodeKategori===1) {
			return $this->db->get_where('m_kategori', array('NamaKategoriId' => $Name, 'IsActive' => 1))->first_row();
		}
		else{
			$this->db->where('KodeKategori <>', $KodeKategori);
			$this->db->where('NamaKategoriId', $Name);
			$this->db->where('IsActive', 1);
			return $this->db->get('m_kategori')->first_row();
		}
	}

	function getCategAndSub(){
		$this->db->select('a.KodeKategori, b.NamaKategoriId as NamaKategoriParent, a.NamaKategoriId');
		$this->db->where('a.IsService', 2);
		$this->db->join('m_kategori b', 'a.KodeKategoriParent = b.KodeKategori','left');
		$this->db->where('a.IsActive', 1);
		$this->db->where('a.KodeKategori <>', 'promo');
		return $this->db->get('m_kategori a')->result();
	}

	function getAll()
	{
		$this->db->select('*, '.cTUp());
		if($this->uri->segment(2)==="kategori-service"){
			$this->db->where('IsService', 1);
		}
		else{
			$this->db->where('IsService', 0);
		}
		$this->db->where('IsActive', 1);
		$this->db->where('KodeKategori <>', 'promo');
		return $this->db->get('m_kategori')->result();
	}

	function add($params)
	{
		$this->db->insert('m_kategori',$params);
		return $this->db->insert_id();
	}

	function update($KodeKategori,$params)
	{
		$this->db->where('KodeKategori',$KodeKategori);
		return $this->db->update('m_kategori',$params);
	}

	function getSelect($isService=0){
		$this->db->where('IsService', $isService);
		$this->db->where('IsActive', 1);
		$this->db->where('KodeKategori <>', 'promo');
		return $this->db->get('m_kategori')->result();
	}

	function getSelect2($searchTerm)
	{
		$this->db->select('KodeKategori, NamaKategori');
		$this->db->like("NamaKategori", $searchTerm, 'both');
		$qry = $this->db->get('m_kategori', 6)->result();
		// Initialize Array with fetched data
		$data = array();
		foreach ($qry as $val) {
			$data[] = array(
				"id" => $val->KodeKategori,
				"text" => $val->NamaKategori,
			);
		}
		return $data;
	}
}
