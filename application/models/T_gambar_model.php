<?php
class T_gambar_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	/*
	 * Get t_gambar by KodeGambar
	 */
	function get_t_gambar($KodeGambar)
	{
		return $this->db->get_where('t_gambar',array('KodeGambar'=>$KodeGambar))->first_row();
	}

	/*
	 * Get all t_gambar
	 */
	function get_all_t_gambar($Tipe)
	{
		$this->db->select('*, '.cTUp());
		$this->db->order_by('DateCreated', 'desc');
		$this->db->where('Tipe', $Tipe);
		return $this->db->get('t_gambar')->result();
	}

	/*
	 * function to add new t_gambar
	 */
	function add_t_gambar($params)
	{
		$this->db->insert('t_gambar',$params);
		return $this->db->insert_id();
	}

	/*
	 * function to update t_gambar
	 */
	function update_t_gambar($KodeGambar,$params)
	{
		$this->db->where('KodeGambar', $KodeGambar);
		return $this->db->update('t_gambar',$params);
	}

	/*
	 * function to delete t_gambar
	 */
	function delete_t_gambar($KodeGambar)
	{
		return $this->db->delete('t_gambar',array('KodeGambar'=>$KodeGambar));
	}

	// Fetch records
	public function getDataGallery($rowno,$rowperpage) {
		$this->db->order_by('Tanggal','desc');
		$this->db->where('Tipe', 2);
		$this->db->limit($rowperpage, $rowno);
		$this->db->from('t_gambar');
		$query = $this->db->get();
		return $query->result();
	}

	// Select total records
	public function getrecordGallery() {
		$this->db->select('count(Tipe) as allcount');
		$this->db->where('Tipe', 2);
		$this->db->from('t_gambar');
		$query = $this->db->get();
		$result = $query->first_row();
		return $result->allcount;
	}
}
