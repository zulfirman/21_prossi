<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class T_comment extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	private $_db='t_comment';

	function comments($id){
		$post=$this->input->post();
		$limit=$post['limitKomen'];
		$offSet=$post['offSet'];
		$this->db->select('KodeKomen,KodeArtikel,Nama,Website,IsiKomen,Tanggal,HasSubKomen,Email');
		$this->db->order_by('Tanggal', 'desc');
		$this->db->where('StatusKomen', 1);
		$this->db->limit($limit);
		if($limit>10){
			$this->db->offset($offSet);
		}
		$this->db->where('KodeArtikel',$id);
		$qry = $this->db->get($this->_db)->result_array();
		return $qry;
	}
}
