<?php
class M_navigasi extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function getAll()
	{
		return $this->db->get('m_navigasi')->result();
	}

	function add($params)
	{
		$this->db->insert('m_navigasi',$params);
		return $this->db->insert_id();
	}

	function update($KodeNavigasi,$params)
	{
		$this->db->where('KodeNavigasi',$KodeNavigasi);
		return $this->db->update('m_navigasi',$params);
	}

	function getTrustee($TipeNavigasi){
		$this->db->cache_on();
		$mainArray=array();
		$this->db->select('KodeNavigasi,NamaNavigasi,Trustee,Url');
		$this->db->where('TipeNavigasi', $TipeNavigasi);
		$this->db->where('ParentId', '');
		$this->db->where('IsActive', 1);

		$this->db->where('TipeNavigasi', $TipeNavigasi);
		$this->db->or_where('Trustee', '');
		$this->db->where('ParentId', '');
		$this->db->where('IsActive', 1);
		
		$qry=$this->db->get('m_navigasi')->result();
		foreach ($qry as $qryv){
			if($qryv->Url!==""){
				$mainArray[]=array(
					'NamaNavigasi'=>$qryv->NamaNavigasi,
					'Trustee'=>$qryv->Trustee
				);
			}
			else{
				$this->db->select('NamaNavigasi,Trustee');
				$this->db->where('TipeNavigasi', $TipeNavigasi);
				$this->db->where('ParentId', $qryv->KodeNavigasi);
				$this->db->where('IsActive', 1);
				
				$parentNav = $this->db->get('m_navigasi')->result();
				foreach ($parentNav as $parentNavv)
				$mainArray[]=array(
					'NamaNavigasi'=>$qryv->NamaNavigasi.' - '.$parentNavv->NamaNavigasi,
					'Trustee'=>$parentNavv->Trustee
				);
			}
		}
		return $mainArray;
	}

	function listNavigasi($TipeNavigasi, $Trustee){
		$this->db->where('TipeNavigasi', $TipeNavigasi);
		$this->db->where('ParentId', '');
		//$this->db->where_in('Trustee', $Trustee);
		$this->db->where('IsActive', 1);

		$this->db->or_where('Trustee', '');
		$this->db->where('TipeNavigasi', $TipeNavigasi);
		$this->db->where('ParentId', '');
		$this->db->where('IsActive', 1);

		$this->db->order_by('(Sort * -1)', 'DESC' );
		$data1= $this->db->get('m_navigasi')->result();
		$i=-1;
		foreach ($data1 as $nav){
			$i++;
			//$this->db->where_in('Trustee', $Trustee);
			$this->db->where('TipeNavigasi', $TipeNavigasi);
			$this->db->where('ParentId', $nav->KodeNavigasi);
			$this->db->where('IsActive', 1);
			$this->db->order_by('(Sort * -1)', 'DESC' );
			$data1[$i]->subNav = $this->db->get('m_navigasi')->result();
		}
		return $data1;
	}
}
