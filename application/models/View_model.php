<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View_model extends CI_Model
{
	function view($view, $data){
		return $this->ci_minifier->outputEcho($this->load->view($view, $data, true));
	}

	function pengaturan(){
		return $this->db->get('m_pengaturan')->first_row();
	}

	//view list artikel
	function list_artikel($rowno)
	{
		/*if($rowno!==0) {// jika row no bukan nomor maka 404
			if (!isInt($rowno)) {
				return $this->ci_minifier->outputEcho($this->load->view('404/li404', '', true));
			}
		}*/
		$data['titlePage']='Article';
		$data['listArtikel']=$this->get_list_article($rowno, 1);// ambil data
		return $this->ci_minifier->outputEcho($this->load->view('front/artikel', $data, true));
	}

	public function get_list_article($rowno, $tipe){
		$this->load->model('t_artikel_model');
		$this->load->library('pagination');

		$pageof= $rowno;
		$rowperpage = 30;// Row per page
		// Row position
		if($rowno != 0){$rowno = ($rowno-1) * $rowperpage;}
		// All records count
		$allcount = $this->t_artikel_model->getrecordCount($tipe);
		// Get records
		$users_record = $this->t_artikel_model->getData($rowno,$rowperpage,$tipe);
		// Pagination Configuration
		$config['base_url'] = site_url($this->uri->segment(1).'/');
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $allcount;
		$config['per_page'] = $rowperpage;
		$config['num_links']=1;
		// Initialize
		$ofpage=round($allcount/$rowperpage);
		if($pageof===0){$pageof=1;}
		$config['next_link'] = '>';
		$config['prev_link'] = '<';
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		//$config['full_tag_open'] = "<ul class='pagination'><li class='centerText'><span>Page $pageof of $ofpage:</span></li>";
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"> <a id="pageActive" class="page-link active-pagination" style="background-color: #CDAC65; border-color: #CDAC65" href="'.current_url().'">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['attributes'] = array('class' => 'page-link');
		$this->pagination->initialize($config);
		// Initialize $data Array
		$data['pagination'] = $this->pagination->create_links();
		$data['result'] = $users_record;
		$data['row'] = $rowno;
		$data['allcount']=$allcount;
		return $data;
	}
	// end view artikel

	//view list promo
	function list_promo($rowno)
	{
		$data['titlePage']='Promo';
		$data['listArtikel']=$this->get_list_article($rowno, 2);// ambil data
		return $this->ci_minifier->outputEcho($this->load->view('front/promo', $data, true));
	}

}
?>
