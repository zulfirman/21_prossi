<?php
class M_user extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	var $_table='m_user';

	function getById($KodeUser)
	{
		return $this->db->get_where($this->_table, array('KodeUser'=>$KodeUser, 'IsActive'=>1))->first_row();
	}

	function getUnique($KodeUser, $Name)
	{
		if($KodeUser===1) {
			return $this->db->get_where($this->_table, array('Username' => $Name, 'IsActive'=>1))->first_row();
		}
		else{
			$this->db->where('KodeUser <>', $KodeUser);
			$this->db->where('Username', $Name);
			$this->db->where('IsActive', 1);
			return $this->db->get($this->_table)->first_row();
		}
	}

	function getAll()
	{
		$this->db->select('a.KodeUser as A_I,a.Username as User,a.MainImage , a.NamaUser as Nama, 
		 a.DateCreated as Da, a.DateUpdated as Du, a.CreatedBy as Cb, a.UpdatedBy as Ub, c.NamaKategoriUser');
		$this->db->where('a.IsActive', 1);
		$this->db->join('m_kategori_user c', 'a.KodeKategoriUser = c.KodeKategoriUser','left');
		return $this->db->get($this->_table.' a')->result();
	}

	function add($params)
	{
		$this->db->insert($this->_table,$params);
		return $this->db->insert_id();
	}

	function update($KodeUser,$params)
	{
		$this->db->where('KodeUser',$KodeUser);
		return $this->db->update($this->_table,$params);
	}

	// Fetch records
	public function getData($rowno,$rowperpage,$Tipe) {
		$post=$this->input->post();
		$searched=false;
		$this->db->select('a.KodeUser as myId, a.Content, a.Username, a.Judul, a.MainImage, a.DateCreated, b.Nama as bNama');
		if($searched==true){
			$this->db->like('Title', $post['Title'], 'both');
			if(isset($post['Genres'])) {
				$genre= implode(',,', $post['Genres']);
				$this->db->like('Genres', $genre, 'both');
			}
			if($post['sYearFrom']!==""&&$post['sYearTo']!=="") {
				$from =$post['sYearFrom']-1;
				$to =$post['sYearTo']+1;
				$this->db->where("ReleaseDate >", $from);
				$this->db->where("ReleaseDate <", $to);
			}
		}
		/*if($TypeMovie!=="all") {
			$this->db->where('TypeMovie', $TypeMovie);
		}*/
		$this->db->join('m_user b', 'a.CreatedBy = b.KodeUser');
		$this->db->where('a.Language', $_SESSION['lang']);
		$this->db->where('a.Tipe', $Tipe);
		$this->db->order_by('a.DateCreated','desc');
		$this->db->limit($rowperpage, $rowno);
		$this->db->from('m_user a');
		$query = $this->db->get();
		return $query->result();
	}
	// Select total records
	public function getrecordCount($Tipe) {
		$post=$this->input->post();
		$searched=false;
		$this->db->select('count(Tipe) as allcount');
		if($searched==true){
			$this->db->like('Title', $post['Title'],'both');
		}
		/*if($TypeMovie!=="all") {
			$this->db->where('TypeMovie', $TypeMovie);
		}*/
		$this->db->join('m_user b', 'a.CreatedBy = b.KodeUser');
		$this->db->where('a.Language', $_SESSION['lang']);
		$this->db->where('a.Tipe', $Tipe);
		$this->db->order_by('a.DateCreated','desc');
		$this->db->from('m_user a');
		$query = $this->db->get();
		$result = $query->first_row();
		return $result->allcount;
	}

	function getSelect(){
		$this->db->select('a.KodeUser, a.NamaUser');
		if($_SESSION['s_tipeuser']==="2") {
			$this->db->where('c.TipeUser <>', 1);
		}
		$this->db->where('a.IsActive', 1);
		$this->db->join('m_kategori_user c', 'a.KodeKategoriUser = c.KodeKategoriUser','left');
		return $this->db->get('m_user a')->result();
	}
}
