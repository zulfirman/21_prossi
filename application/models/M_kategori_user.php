<?php
class M_kategori_user extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	var $_table= 'm_kategori_user';
	
	function getById($KodeKategoriUser)
	{
		return $this->db->get_where($this->_table,array('KodeKategoriUser'=>$KodeKategoriUser))->first_row();
	}

	function getUnique($KodeKategoriUser, $Name)
	{
		if($KodeKategoriUser===1) {
			return $this->db->get_where($this->_table, array('NamaKategoriUser' => $Name, 'IsActive' => 1))->first_row();
		}
		else{
			$this->db->where('KodeKategoriUser <>', $KodeKategoriUser);
			$this->db->where('NamaKategoriUser', $Name);

			$this->db->where('IsActive', 1);
			return $this->db->get($this->_table)->first_row();
		}
	}
	
	function getAll()
	{
		$this->db->select('*, a.DateCreated as Da, a.DateUpdated as Du, a.CreatedBy as Cb, a.UpdatedBy as Ub');
		$this->db->where('a.IsActive', 1);
		return $this->db->get($this->_table.' a')->result();
	}

	function add($params)
	{
		$this->db->insert($this->_table,$params);
		return $this->db->insert_id();
	}

	function update($KodeKategoriUser,$params)
	{
		$this->db->where('KodeKategoriUser',$KodeKategoriUser);
		return $this->db->update($this->_table,$params);
	}

	function getSelect(){
		$this->db->select('KodeKategoriUser, NamaKategoriUser');
		return $this->db->get($this->_table)->result();
	}
}
