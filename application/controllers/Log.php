<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$files = scandir(APPPATH . 'logs/', SCANDIR_SORT_DESCENDING);
		if($files[0]!=="index.html") {
			ob_start();
			include APPPATH . "logs/$files[0]";
			$string = ob_get_clean();
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'mail.sumurmonyo.com',
				'smtp_port' => 465,
				'smtp_crypto'   => 'ssl',
				'smtp_user' => 'no.reply@sumurmonyo.com',
				'smtp_pass' => 'passno.reply',
				'smtp_timeout' => '4',
				'mailtype' => 'html',
				'charset' => 'iso-8859-1'
			);
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('no.reply@sumurmonyo.com', 'No Reply');
			$this->email->to('maulanaiman811@gmail.com');
			$this->email->subject('Error Log');
			$this->email->message($string);
			$this->email->send();
			return jsonSucces();
		}
	}

}
