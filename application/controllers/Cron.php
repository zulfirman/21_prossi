<?php

class Cron extends CI_Controller
{
	function index(){
		if (php_sapi_name() == 'cli') {
			if (isset($_SERVER['TERM'])) {
				return json403();
				//echo "The script was run from a manual invocation on a shell";
			} else {
				//echo "The script was run from the crontab entry";
				$mainJson=array(
					'deleteCache' => 	$this->deleteCache(),
					'deleteLog' => 	$this->deleteLog(),
				);
				$loading_time=0;
				foreach ($mainJson as $key => $val){
					$loading_time+=$mainJson[$key]['time'];
				}
				$insert=array(
					'A_I' => date('y').uniqid(),
					'Date' => tglSekarang(3),
					'Response' => json_encode($mainJson),
					'LoadingTime' => doubleval(substr($loading_time,0,30))
				);
				$this->db->insert('m_log_cron', $insert);
				$insert['Response']=json_decode($insert['Response']);
				return toJson($insert);
			}
		} else {
			return json403();
			//echo "The script was run from a webserver, or something else";
		}
	}

	function deleteCache(){
		$starttime = $_SERVER["REQUEST_TIME_FLOAT"];
		$dirPath=APPPATH .'cache/database/';
		rrmdir($dirPath);
		mkdir($dirPath, 0777, true);
		$dirPath=APPPATH .'cache/pages/';
		rrmdir($dirPath);
		mkdir($dirPath, 0777, true);
		$endtime = microtime(true);
		$mainjson['time']=$endtime - $starttime;
		return $mainjson;
	}

	function deleteLog(){
		$starttime = $_SERVER["REQUEST_TIME_FLOAT"];
		$folderName = APPPATH."logs";
		if (file_exists($folderName)) {
			foreach (new DirectoryIterator($folderName) as $fileInfo) {
				if ($fileInfo->isDot()) {
					continue;
				}
				if ($fileInfo->isFile() && time() - $fileInfo->getCTime() > 14*24*60*60) {
					unlink($fileInfo->getRealPath());
				}
			}
			$fp = fopen("$folderName/index.html","wb");
			fwrite($fp,'');
			fclose($fp);
		}
		$endtime = microtime(true);
		$mainjson['time']=$endtime - $starttime;
		return $mainjson;
	}

	function updateStatusPembayaran(){
		$listKode=array();
		$this->db->where('Tanggal', tglSekarang(1));
		$this->db->where('Checked', 0);
		$this->db->order_by('Tanggal', 'asc');
		$this->db->order_by('Jam', 'asc');
		$qry = $this->db->get('t_log_payment')->result();
		foreach ($qry as $val) {
			$this->db->where('KodePenjualan', $val->KodePenjualan);
			$this->db->update('t_penjualan', array(
				'BayarMenggunakan' => $val->BayarMenggunakan,
				'LogBayar' => $val->LogBayar,
				'StatusPembayaran' => $val->StatusPembayaran,
			));
			array_push($listKode, $val->KodeLogPayment);
		}
		if(count($listKode)>0) {
			$this->db->where_in('KodeLogPayment', $listKode);
			$this->db->update('t_log_payment', array(
				'Checked' => 1
			));
		}
		return jsonSucces();
	}
}
