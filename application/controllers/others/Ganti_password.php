<?php

class Ganti_password extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$method = $this->router->fetch_method();if ($method!=="index") {if(!isset($_SESSION['s_kodeuser'])){redirect(site_url());}}
		$this->load->model('m_user');//load model
	}

	private $blade='others/';//folder

	function index(){
		$namaView='ganti-password';
		$data['val'] = $this->m_user->getById($_SESSION['s_kodeuser']);
		return $this->load->view("$this->blade$namaView", $data);
	}
}
