<?php

class Master_kategori_user extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$this->load->model('m_kategori_user');//load model
		//cekAkses(array('0003'));
	}

	private $blade='admin/master-kategori-user/';//folder

	function index(){
		$namaView='index';
		$data['getAll'] = $this->m_kategori_user->getAll();
		return $this->load->view("$this->blade$namaView", $data);
	}

	function tambah()
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			$po=$this->input->post();
			$data['val'] = $this->m_kategori_user->getUnique(1, $po['NamaKategoriUser']);//get unique
			if(isset($data['val']->KodeKategoriUser))
			{
				show_error('Data Duplikat Terdeteksi');
			}
			$po['Trustee'] = implode(',', $po['Trustee'])??'';
			$params['KodeKategoriUser']= myUniqId();
			$params['DateCreated'] = tglSekarang(3);
			$params['DateUpdated'] = null;
			$params['CreatedBy'] = $_SESSION['s_kodeuser'];
			$params['UpdatedBy'] = null;
			$this->m_kategori_user->add($po+$params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect(current_url());
		}
		else{
			$namaView='tambah';
			return $this->load->view("$this->blade$namaView");
		}
	}

	function edit($KodeKategoriUser)
	{
		if(isset($_POST) && count($_POST) > 0) {
			$po=$this->input->post();
			$data['val'] = $this->m_kategori_user->getUnique($po['KodeKategoriUser'], $po['NamaKategoriUser']);
			if(isset($data['val']->KodeKategoriUser))
			{
				show_error('Duplicate Data Detected');
			}
			$po['Trustee'] = implode(',', $po['Trustee'])??'';

			$params['DateUpdated'] = tglSekarang(3);
			$params['UpdatedBy'] = $_SESSION['s_kodeuser'];
			$this->m_kategori_user->update($po['KodeKategoriUser'], $po+$params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2)));
		}
		else {
			$data['val'] = $this->m_kategori_user->getById($KodeKategoriUser);
			if (isset($data['val']->KodeKategoriUser)) {
				$namaView = 'edit';
				return $this->load->view("$this->blade$namaView", $data);
			} else{
				show_error('Data Tidak Dapat Ditemukan.');
			}
		}
	}

	function get_select2(){
		if(!isset($_POST['searchTerm'])){
			return jsonError();
		}
		return toJson($this->m_kategori_user->getSelect2($_POST['searchTerm']));
	}
}
