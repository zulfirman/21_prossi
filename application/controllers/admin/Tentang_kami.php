<?php
class Tentang_kami extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('t_gambar_model');
		//cekAkses(array('0011'));
	}

	var $_tipe=2;
	var $_link='./assets/uploads/';
	var $_folder='admin/tentang-kami/';
	/*
	 * Listing of t_gambar
	 */
	function index()
	{
		$data['getAll'] = $this->t_gambar_model->get_all_t_gambar($this->_tipe);
		$this->load->view($this->_folder.'index',$data);
	}

	/*
	 * Editing a t_gambar
	 */
	function edit($KodeGambar)
	{
		if(isset($_POST) && count($_POST) > 0) {
			$po=$this->input->post();
			if (isset($po['CabangImage'])) {
				foreach ($po['CabangImage'] as $key => $val) {
					$Lainnya1[]=array(
						'Id' => uniqid(),
						'CabangImage' =>$po['CabangImage'][$key],
						'NamaCabang' =>$po['NamaCabang'][$key],
					);
				}
			}else{
				$Lainnya1=array();
			}
			unset($po['CabangImage']);
			unset($po['NamaCabang']);
			$params['Lainnya1']=json_encode($Lainnya1);
			$params['DateUpdated'] = tglSekarang(3);
			$params['UpdatedBy'] = $_SESSION['s_kodeuser'];
			$this->t_gambar_model->update_t_gambar($po['KodeGambar'], $po+$params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect($this->_folder);
		}else {
			// check if the t_gambar exists before trying to edit it
			$data['val'] = $this->t_gambar_model->get_t_gambar($KodeGambar);
			$listFoto=json_decode($data['val']->Lainnya1);
			if (isset($data['val']->KodeGambar)) {
				$this->load->view($this->_folder . 'edit', $data);
			} else
				show_error('Data Not Found.');
		}
	}

}
