<?php

class Pengaturan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
	}

	var $_link='./assets/uploads/';

	function index(){
		if(isset($_POST) && count($_POST) > 0)
		{
			$po = $this->input->post();
			$paramsImage = array();
			if (isset($_POST) && count($_POST) > 0) {
				foreach (array('bPelayanan','bPromo','bArtikel','bTentang','bKontak') as $val) {
					$backgroundHeader[$val]=editCrop($po, $val, "oldImage$val", array(1299,622));
					$keyText='t'.substr($val,1);
					$backgroundFooter[$keyText]=$po[$keyText];
					unset($po["oldImage$val"]);
					unset($po[$keyText]);
				}
				foreach (array('Logo', 'Icon') as $val) {
					$paramsImage[$val]=editGambar($po, $val, "oldImage$val");
					unset($po["oldImage$val"]);
				}
				$po['BackgroundHeader']=json_encode($backgroundHeader);
				$po['BackgroundFooter']=json_encode($backgroundFooter);
				$po['DateUpdated']=tglSekarang(3);
				$po['UpdatedBy']=$_SESSION['s_kodeuser'];
				unset($po["jsonBackground"]);
				$params = $po + $paramsImage;
				$this->db->where('KodePengaturan', $po['KodePengaturan']);
				$this->db->update('m_pengaturan',$params);
				$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
				redirect(current_url());
			}
		}else {
			$data['val'] = getPengaturan();
			return $this->load->view("admin/pengaturan", $data);
		}
	}
}
