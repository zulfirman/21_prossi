<?php

class Dashboard extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
	}

	function index(){
		return $this->load->view("admin/dashboard");
	}

	function get_data()
	{
		if(isset($_POST['tgl1'])&&isset($_POST['tgl2'])&&isset($_POST['KodeCabang'])){
			$tgl1 = new DateTime($_POST['tgl1']);
			$tgl1 = $tgl1->modify('-1 day')->format('Y-m-d');
			$tgl2 = new DateTime($_POST['tgl2']);
			$tgl2 = $tgl2->modify('+1 day')->format('Y-m-d');

			$this->db->select('sum(a.Jumbel) as TotalJumbel');
			$this->db->where('a.Tanggal >', $tgl1);
			$this->db->where('a.Tanggal <', $tgl2);
			$this->db->where_in('a.KodeCabang', $_POST['KodeCabang']);
			$stok=$this->db->get('t_detail_penjualan a')->first_row();

			$this->db->select('count(a.KodePenjualan) as TotalPenjualan,  sum(a.TotalHarga) as TotalTransaksi');
			$this->db->where('a.Tanggal >', $tgl1);
			$this->db->where('a.Tanggal <', $tgl2);
			$this->db->where_in('a.KodeCabang', $_POST['KodeCabang']);
			$penjualan=$this->db->get('t_penjualan a')->first_row();

			$this->db->select('count(distinct a.NamaCustomer) as TotalCustomer');
			$this->db->where('a.Tanggal >', $tgl1);
			$this->db->where('a.Tanggal <', $tgl2);
			$this->db->where_in('a.KodeCabang', $_POST['KodeCabang']);
			$customer=$this->db->get('t_penjualan a')->first_row();

			return toJson(array_merge((array) $stok,(array) $penjualan,(array) $customer));
		}
		else{
			return jsonError();
		}
	}
}
