<?php

class Sub_kategori_service extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$this->load->model('m_kategori');//load model
		//cekAkses(array('0015'));
	}

	private $blade='admin/sub-kategori-service/';//folder

	function index(){
		$namaView='index';
		$this->db->select('a.KodeKategori, b.NamaKategoriId as NamaKategoriParent, a.NamaKategoriId, a.NamaKategoriEn, '.cTUp('a'));
		$this->db->where('a.IsService', 2);
		$this->db->join('m_kategori b', 'a.KodeKategoriParent = b.KodeKategori','left');
		$this->db->where('a.IsActive', 1);
		$this->db->where('a.KodeKategori <>', 'promo');
		$data['getAll'] = $this->db->get('m_kategori a')->result();
		return $this->load->view("$this->blade$namaView", $data);
	}

	function parentKategori(){
		$this->db->select('KodeKategori, NamaKategoriId');
		$this->db->where('IsService', 1);
		$this->db->where('IsActive', 1);
		$this->db->where('KodeKategori <>', 'promo');
		return $this->db->get('m_kategori a')->result();
	}
	function tambah()
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			$po=$this->input->post();
			$uniqId=myUniqId();
			$data['val'] = $this->m_kategori->getUnique(1, $po['NamaKategoriId']);//get unique
			if(isset($data['val']->KodeKategori))
			{
				show_error('Data Duplikat Terdeteksi');
			}
			$params['KodeKategori'] = $uniqId;
			if($this->uri->segment(2)==="master-kategori-service"){
				$params['MainImage'] = uploadGambar($uniqId);
			}else {
				$po["MainImage"] = 'default.jpg';
			}
			$po["NamaKategori"]=str_replace(' ','-', strtolower($po["NamaKategoriId"]));
			$params['DateCreated'] = tglSekarang(3);
			$params['DateUpdated'] = null;
			$params['CreatedBy'] = $_SESSION['s_kodeuser'];
			$params['UpdatedBy'] = null;
			$this->m_kategori->add($po+$params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect(current_url());
		}
		else{
			$namaView='tambah';
			$data['parentKategori']= $this->parentKategori();
			return $this->load->view("$this->blade$namaView", $data);
		}
	}

	var $_link='./assets/uploads/';
	function edit($KodeKategori)
	{
		if(isset($_POST) && count($_POST) > 0) {
			$po=$this->input->post();
			$data['val'] = $this->m_kategori->getUnique($po['KodeKategori'], $po['NamaKategoriId']);
			if(isset($data['val']->KodeKategori))
			{
				show_error('Duplicate Data Detected');
			}

			if($this->uri->segment(2)==="master-kategori-service") {
				$params['MainImage']=editGambar($po);
				unset($po["OldImage"]);
			}
			$po["NamaKategori"]=str_replace(' ','-', strtolower($po["NamaKategoriId"]));
			$params['DateUpdated'] = tglSekarang(3);
			$params['UpdatedBy'] = $_SESSION['s_kodeuser'];
			$this->m_kategori->update($po['KodeKategori'], $po+$params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2)));
		}
		else {
			$data['val'] = $this->m_kategori->getById($KodeKategori);
			if (isset($data['val']->KodeKategori)) {
				$namaView = 'edit';
				$data['parentKategori']= $this->parentKategori();
				return $this->load->view("$this->blade$namaView", $data);
			} else{
				show_error('Data Tidak Dapat Ditemukan.');
			}
		}
	}

	function get_select2(){
		if(!isset($_POST['searchTerm'])){
			return jsonError();
		}
		return toJson($this->m_kategori->getSelect2($_POST['searchTerm']));
	}
}
