<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends CI_Controller {

	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$this->load->model('t_comment');
	}

	function manage($KodeArtikel){
		$this->db->select('JudulId');
		$this->db->where('KodeArtikel', $KodeArtikel);
		$data['artikel']=$this->db->get('t_artikel', 1)->first_row();

		$this->db->where('KodeArtikel', $KodeArtikel);
		$this->db->select('KodeKomen,Nama,IsiKomen,Tanggal,Email, "t_comment" as tabel, "KodeKomen" as kolom');
		$this->db->order_by('Tanggal', 'desc');
		$this->db->where('StatusKomen', 1);
		$data['listKomen']=$this->db->get('t_comment', $KodeArtikel)->result();

		$this->db->where('KodeArtikel', $KodeArtikel);
		$this->db->select('KodeSubKomen as KodeKomen,Nama,IsiKomen,Tanggal,Email, "t_sub_comment" as tabel, "KodeSubKomen" as kolom');
		$this->db->order_by('Tanggal', 'desc');
		$this->db->where('StatusKomen', 1);
		$data['listKomen']=array_merge($data['listKomen'],$this->db->get('t_sub_comment', $KodeArtikel)->result());
		$this->load->view('admin/comment/manage', $data);
	}

	function submit(){
		$post = $this->input->post();
		cekajax();
		$KodeArtikel = $post['KodeArtikel'];
		$value = array(
			'KodeKomen' => date('y') . uniqid(),
			'KodeArtikel' => $KodeArtikel,
			'Nama' => $post['Nama'],
			'Email' => $post['Email'],
			'Website' =>'',
			'IsiKomen' => $post['IsiKomen'],
			'Tanggal' => date('Y-m-d H:i:s'),
			'HasSubKomen' => 0,
			'StatusKomen' => 1,
		);
		$this->db->insert('t_comment', $value);

		$this->db->select('UrlArtikel,JudulId');
		$this->db->where('KodeArtikel', $KodeArtikel);
		$qry=$this->db->get('t_artikel', 1)->first_row();
		$value=array(
			'IdNotification' => date('y') . uniqid(),
			'Type' => $post['Type'],
			'Content' => $post['Nama'].' mengomentari '.$qry->JudulId,
			'Idnya' => $qry->UrlArtikel,
			'DateAdded' => date('Y-m-d H:i:s'),
			'IsRead' => 0,
			'IsOpen' => 0,
		);
		$this->db->insert('t_notifikasi', $value);
		return jsonSucces();
	}

	function submitSub(){
		$post=$this->input->post();
		cekajax();
		$KodeKomen=$post['bkb'];
		$value=array(
			'KodeSubKomen' => date('y') . uniqid(),
			'KodeKomen' => $KodeKomen,
			'KodeArtikel' => $post['rKodeArtikel'],
			'Nama' =>$post['rNama'],
			'Email' =>$post['rEmail'],
			'Website' =>'',
			'IsiKomen' => "@".$post['ReplyTo']." ".$post['rIsiKomen'],
			'Tanggal' => date('Y-m-d H:i:s'),
			'StatusKomen' => 1,
		);
		$this->db->insert('t_sub_comment', $value);
		$value=array(
			'HasSubKomen' => 1,
		);
		$this->db->where('KodeKomen', $KodeKomen);
		$this->db->update('t_comment', $value);

		$this->db->select('UrlArtikel,JudulId');
		$this->db->where('KodeArtikel', $post['rKodeArtikel']);
		$qry=$this->db->get('t_artikel', 1)->first_row();
		$value=array(
			'IdNotification' => date('y') . uniqid(),
			'Type' => $post['rType'],
			'Content' => $post['rNama'].' mengomentari '.$qry->JudulId,
			'Idnya' => $qry->UrlArtikel,
			'DateAdded' => date('Y-m-d H:i:s'),
			'IsRead' => 0,
			'IsOpen' => 0,
		);
		$this->db->insert('t_notifikasi', $value);

		return jsonSucces();
	}

	function getGravatar( $email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array() ) {
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		return $url;
	}

	function getNotif(){
		cekajax();
		$this->db->limit(10);
		$this->db->order_by('DateAdded','desc');
		return toJson($this->db->get('t_notifikasi')->result_array());
	}

	function readNotif(){
		cekajax();
		$data=array(
			'IsRead' => 1
		);
		$this->db->where('IsRead', '0');
		$this->db->update('t_notifikasi', $data);
		jsonSucces();
	}
}
