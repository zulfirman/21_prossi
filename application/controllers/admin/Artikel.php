<?php
class Artikel extends CI_Controller{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$this->load->model('t_artikel_model');
		$this->load->model('m_kategori');
	}

	var $_link='./assets/uploads/';
	var $_folder='admin/artikel/';
	var $_tipe=1;
	/*
	 * Listing of t_artikel
	 */
	function index()
	{
		$data['getAll'] = $this->t_artikel_model->get_all_t_artikel($this->_tipe);
		$this->load->view($this->_folder.'index',$data);
	}

	/*
	 * Adding a new t_artikel
	 */
	function tambah()
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			$po=$this->input->post();
			$uniqId=myUniqId();
			$UrlArtikel=cleanJudul($po['JudulId']);
			$UrlArtikel=str_replace(' ','-', strtolower($UrlArtikel));
			$data['val'] = $this->t_artikel_model->get_uniq($this->_tipe, $UrlArtikel);
			if(isset($data['val']['UrlArtikel']))
			{
				show_error('Duplicate Judul Detected');
			}
			$params = array(
				'KodeArtikel' => $uniqId,
				'UrlArtikel' => $UrlArtikel,
				'JudulId' => $po['JudulId'],
				'JudulEn' => $po['JudulEn'],
				'ContentId' => $po['ContentId'],
				'ContentEn' => $po['ContentEn'],
				'MainImage' => uploadCrop($uniqId,'MainImage', array(1440,810)),
				'KodeKategori' => implode(',', $po['KodeKategori']),
				'EnableComment' => $po['EnableComment'],
				'Tipe' => $this->_tipe,
				'DateCreated' => tglSekarang(3),
				'DateUpdated' => null,
				'CreatedBy' => $_SESSION['s_kodeuser'],
				'UpdatedBy' => '',
			);
			$this->t_artikel_model->add_t_artikel($params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect($this->_folder.'tambah');
		}
		else
		{
			$data['listKategori'] = $this->m_kategori->getSelect();
			$this->load->view($this->_folder.'tambah', $data);
		}
	}

	/*
	 * Editing a t_artikel
	 */
	function edit($KodeArtikel)
	{
		if(isset($_POST) && count($_POST) > 0) {
			$po=$this->input->post();
			$UrlArtikel=saniz($po['JudulId']);
			$UrlArtikel=str_replace(' ','-', strtolower($UrlArtikel));
			$data['val'] = $this->t_artikel_model->get_uniq($po['KodeArtikel'], $UrlArtikel);
			if(isset($data['val']['UrlArtikel']))
			{
				show_error('Duplicate Judul Detected');
			}
			$params = array(
				'UrlArtikel' => $UrlArtikel,
				'JudulId' => $po['JudulId'],
				'JudulEn' => $po['JudulEn'],
				'ContentId' => $po['ContentId'],
				'ContentEn' => $po['ContentEn'],
				'MainImage' => editCrop($po,'MainImage','OldImage', array(1440,810)),
				'KodeKategori' => implode(',', $po['KodeKategori']),
				'EnableComment' => $po['EnableComment'],
				'DateUpdated' => tglSekarang(3),
				'UpdatedBy' => $_SESSION['s_kodeuser'],
			);
			$this->t_artikel_model->update_t_artikel($po['KodeArtikel'], $params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect($this->_folder);
		}else {
			// check if the t_artikel exists before trying to edit it
			$data['val'] = $this->t_artikel_model->get_t_artikel($KodeArtikel);
			if (isset($data['val']->KodeArtikel)) {
				$data['listKategori'] = $this->m_kategori->getSelect();
				$this->load->view($this->_folder.'edit', $data);
			} else {
				show_error('The t_artikel you are trying to edit does not exist.');
			}
		}
	}


	function comment($KodeArtikel)
	{
		$this->db->where('KodeArtikel', $KodeArtikel);
		$this->db->select('KodeKomen,Nama,IsiKomen,Tanggal,Email');
		$this->db->order_by('Tanggal', 'desc');
		$this->db->where('StatusKomen', 1);
		$data['t_komen']=$this->db->get('t_comment', $KodeArtikel)->result_array();

		$this->db->where('KodeArtikel', $KodeArtikel);
		$this->db->select('KodeSubKomen as KodeKomen,Nama,IsiKomen,Tanggal,Email');
		$this->db->order_by('Tanggal', 'desc');
		$this->db->where('StatusKomen', 1);
		$data['t_sub_comment']=$this->db->get('t_sub_comment', $KodeArtikel)->result_array();
		$this->load->view($this->_folder.'comment', $data);
	}

}
