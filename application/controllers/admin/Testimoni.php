<?php
class Testimoni extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('t_gambar_model');
		//cekAkses(array('0013'));
	}

	var $_tipe=4;
	var $_link='./assets/uploads/';
	var $_folder='admin/testimoni/';
	/*
	 * Listing of t_gambar
	 */
	function index()
	{
		$data['getAll'] = $this->t_gambar_model->get_all_t_gambar($this->_tipe);
		$this->load->view($this->_folder.'index',$data);
	}

	function tambah()
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			$po=$this->input->post();
			$uniqId=myUniqId();
			$params['KodeGambar']=$uniqId;
			$params['MainImage']=uploadGambar($uniqId);
			$params['Tipe']=$this->_tipe;
			$params['DateCreated'] = tglSekarang(3);
			$params['CreatedBy'] = $_SESSION['s_kodeuser'];
			$this->t_gambar_model->add_t_gambar($po+$params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect($this->_folder);
		}
		else
		{
			$this->load->view($this->_folder.'tambah');
		}
	}


	function edit($KodeGambar)
	{
		if(isset($_POST) && count($_POST) > 0) {
			$po=$this->input->post();
			$params['MainImage']=editGambar($po);
			unset($po["OldImage"]);
			$params['DateUpdated'] = tglSekarang(3);
			$params['UpdatedBy'] = $_SESSION['s_kodeuser'];
			$this->t_gambar_model->update_t_gambar($po['KodeGambar'], $po+$params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect($this->_folder);
		}else {
			// check if the t_gambar exists before trying to edit it
			$data['val'] = $this->t_gambar_model->get_t_gambar($KodeGambar);
			if (isset($data['val']->KodeGambar)) {
				$this->load->view($this->_folder . 'edit', $data);
			} else
				show_error('Data Not Found.');
		}
	}

}
