<?php
class Promo extends CI_Controller{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$this->load->model('t_artikel_model');
		$this->load->model('m_kategori');
	}

	var $_link='./assets/uploads/';
	var $_folder='admin/promo/';
	var $_tipe=2;
	/*
	 * Listing of t_artikel
	 */
	function index()
	{
		$data['getAll'] = $this->t_artikel_model->get_all_t_artikel($this->_tipe);
		$this->load->view($this->_folder.'index',$data);
	}

	/*
	 * Adding a new t_artikel
	 */
	function tambah()
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			$po=$this->input->post();
			$uniqId=myUniqId();
			$UrlArtikel=saniz($po['JudulId']);
			$UrlArtikel=str_replace(' ','-', strtolower($UrlArtikel));
			$data['val'] = $this->t_artikel_model->get_uniq($this->_tipe, $UrlArtikel);
			if(isset($data['val']['UrlArtikel']))
			{
				show_error('Duplicate Judul Detected');
			}
			$params = array(
				'KodeArtikel' => $uniqId,
				'UrlArtikel' => $UrlArtikel,
				'JudulId' => $po['JudulId'],
				'JudulEn' => $po['JudulEn'],
				'ContentId' => $po['ContentId'],
				'ContentEn' => $po['ContentEn'],
				'MainImage' => uploadCrop($uniqId,'MainImage', array(500,500)),
				'KodeKategori' => 'promo',
				'EnableComment' => $po['EnableComment'],
				'Tipe' => $this->_tipe,
				'Lainnya1' => $po['Lainnya1'],
				'DateCreated' => tglSekarang(3),
				'DateUpdated' => null,
				'CreatedBy' => $_SESSION['s_kodeuser'],
				'UpdatedBy' => '',
			);
			if($po['Lainnya1']==="1"){
				$this->db->where('Tipe', 2);
				$this->db->update('t_artikel', array('Lainnya1' => 0));
			}
			$this->t_artikel_model->add_t_artikel($params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect($this->_folder.'tambah');
		}
		else
		{
			$data['listKategori'] = $this->m_kategori->getSelect();
			$this->load->view($this->_folder.'tambah', $data);
		}
	}

	/*
	 * Editing a t_artikel
	 */
	function edit($KodeArtikel)
	{
		if(isset($_POST) && count($_POST) > 0) {
			$po=$this->input->post();
			$UrlArtikel=saniz($po['JudulId']);
			$UrlArtikel=str_replace(' ','-', strtolower($UrlArtikel));
			$data['val'] = $this->t_artikel_model->get_uniq($po['KodeArtikel'], $UrlArtikel);
			if(isset($data['val']['UrlArtikel']))
			{
				show_error('Duplicate Judul Detected');
			}
			$params = array(
				'UrlArtikel' => $UrlArtikel,
				'JudulId' => $po['JudulId'],
				'JudulEn' => $po['JudulEn'],
				'ContentId' => $po['ContentId'],
				'ContentEn' => $po['ContentEn'],
				'Lainnya1' => $po['Lainnya1'],
				'EnableComment' => $po['EnableComment'],
				'MainImage' => editCrop($po,'MainImage','OldImage', array(500,500)),
				'DateUpdated' => tglSekarang(3),
				'UpdatedBy' => $_SESSION['s_kodeuser'],
			);
			if($po['Lainnya1']==="1"){
				$this->db->where('Tipe', 2);
				$this->db->update('t_artikel', array('Lainnya1' => 0));
			}
			$this->t_artikel_model->update_t_artikel($po['KodeArtikel'], $params);
			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect($this->_folder);
		}else {
			// check if the t_artikel exists before trying to edit it
			$data['val'] = $this->t_artikel_model->get_t_artikel($KodeArtikel);
			if (isset($data['val']->KodeArtikel)) {
				$data['listKategori'] = $this->m_kategori->getSelect();
				$this->load->view($this->_folder.'edit', $data);
			} else {
				show_error('The t_artikel you are trying to edit does not exist.');
			}
		}
	}

}
