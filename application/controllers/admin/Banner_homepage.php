<?php
class Banner_homepage extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('t_gambar_model');
		//cekAkses(array('0010'));
	}

	var $_tipe=1;
	var $_link='./assets/uploads/';
	var $_folder='admin/banner-homepage/';
	/*
	 * Listing of t_gambar
	 */
	function index()
	{
		$data['getAll'] = $this->t_gambar_model->get_all_t_gambar($this->_tipe);
		$this->load->view($this->_folder.'index',$data);
	}

	/*
	 * Adding a new t_gambar
	 */
	function add()
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			$po=$this->input->post();
			$uniqId=myUniqId();
			$params = array(
				'KodeGambar' => $uniqId,
				'Lainnya1' => $po['Lainnya1'],
				'Tipe' => $this->_tipe,
				//'Language' => $po['Language'],
				'DateAdded' => tglSekarang(3),
				'DateUpdated' => null,
				'CreatedBy' => $_SESSION['s_kodeuser'],
				'UpdatedBy' => '',
			);
			$this->t_gambar_model->add_t_gambar($params);
			redirect($this->_folder);
		}
		else
		{
			$this->load->view($this->_folder.'add');
		}
	}

	/*
	 * Editing a t_gambar
	 */
	function edit($KodeGambar)
	{
		if(isset($_POST) && count($_POST) > 0) {
			$po=$this->input->post();
			/*$url_components = parse_url($po['Lainnya1']);
			parse_str($url_components['query'], $parameter);
			if(isset($parameter['v'])) {
				//['Lainnya1'] = "https://youtube.com/embed/$url?autoplay=1&controls=0&showinfo=0&autohide=1&mute=1&playlist=$url&loop=1&modestbranding=1";
				$po['Lainnya1'] = $parameter['v'];
			}else{
				if(isset(explode('/',$po['Lainnya1'])[3])) {
					$po['Lainnya1']=explode('/',$po['Lainnya1'])[3];
				}else{
					unset($po['Lainnya1']);
				}
			}*/
			$params['DateUpdated'] = tglSekarang(3);
			$params['UpdatedBy'] = $_SESSION['s_kodeuser'];
			$this->t_gambar_model->update_t_gambar($po['KodeGambar'], $po+$params);
			redirect($this->_folder);
		}else {
			// check if the t_gambar exists before trying to edit it
			$data['val'] = $this->t_gambar_model->get_t_gambar($KodeGambar);
			if (isset($data['val']->KodeGambar)) {
				$this->load->view($this->_folder . 'edit', $data);
			} else
				show_error('Data Not Found.');
		}
	}

}
