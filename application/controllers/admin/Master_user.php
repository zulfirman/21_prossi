<?php

class Master_user extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$this->load->model('m_user');//load model
		$this->load->model('m_kategori_user');
		//cekAkses(array('0002'));
	}

	private $blade='admin/master-user/';//folder
	private $bladeCabang='kasir/master-user/';
	var $_link='./assets/uploads/';

	function index(){
		$namaView='index';
		$data['getAll'] = $this->m_user->getAll();
		return $this->load->view("$this->blade$namaView", $data);
	}

	function tambah()
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			$po=$this->input->post();
			$KodeUser=strtok($po['Username'], '@');
			$data['val'] = $this->m_user->getById($KodeUser);
			if($data['val'])
			{
				show_error('Data Duplikat Terdeteksi');
			}
			$params = array(
				'KodeUser' => $KodeUser,
				'MainImage' => uploadGambar(myUniqId()),
				'Username' => $po['Username'],
				'NamaUser' => $po['NamaUser'],
				'Password' => md5(sha1($po['Password'])),
				'KodeKategoriUser' => $po['KodeKategoriUser'],

				'DateCreated' => tglSekarang(3),
				'DateUpdated' => null,
				'CreatedBy' => $_SESSION['s_kodeuser'],
				'UpdatedBy' => '',
			);
			$this->m_user->add($params+$po);
			deleteDbCaches();

			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			redirect(current_url());
		}
		else{
			$namaView='tambah';
			$data['listKategoriUser'] = $this->m_kategori_user->getSelect();
			return $this->load->view("$this->blade$namaView", $data);
		}
	}

	function edit($KodeUser=0)
	{
		if(isset($_POST) && count($_POST) > 0) {
			$po=$this->input->post();
			if($po['Password']!==$po['oldPassword']){//jika pass baru maka update pass
				$po['Password'] =md5(sha1($po['Password']));
			}
			if(isset($po['gantiPassword'])) {
				$redirectGantiPass=1;
				unset($po['gantiPassword']);
			}
			else{
				$redirectGantiPass=0;
			}

			$oldImage = $po['oldImage'];
			if (!empty($_FILES["MainImage"]["name"])) {
				if (file_exists("$this->_link$oldImage")) {
					unlink("$this->_link$oldImage");
				}
				$MainImage = uploadGambar(myUniqId());
			} else {
				$MainImage = $oldImage;
			}
			unset($po['oldImage']);
			$po['MainImage'] = $MainImage;
			unset($po['oldPassword']);
			$params['DateUpdated'] =  tglSekarang(3);
			$params['UpdatedBy'] =  $_SESSION['s_kodeuser'];
			$this->m_user->update($po['KodeUser'], $po+$params);
			deleteDbCaches();

			$this->session->set_flashdata('fnotif','Sukses Menyimpan Data');
			if($_SESSION['s_tipeuser']==="1") {
				if($redirectGantiPass===0) {
					redirect($this->blade);
				}
				else{
					redirect(site_url('user/ganti-password'));
				}
			}
			else if($_SESSION['s_tipeuser']==="2") {
				if($redirectGantiPass===0) {
					redirect($this->bladeCabang);
				}
				else{
					redirect(site_url('user/ganti-password'));
				}
			}
			else if($_SESSION['s_tipeuser']==="3") {
				if($redirectGantiPass===0) {
					redirect($this->bladeCabang);
				}
				else{
					redirect(site_url('user/ganti-password'));
				}
			}
		}
		else {
			$data['val'] = $this->m_user->getById($KodeUser);
			if (isset($data['val']->KodeUser)) {
				$namaView = 'edit';
				$data['listKategoriUser'] = $this->m_kategori_user->getSelect();
				return $this->load->view("$this->blade$namaView", $data);
			} else{
				show_error('Data Tidak Dapat Ditemukan.');
			}
		}
	}

	function listTrustee($TipeMenu=0){
		if($TipeMenu===0){
			return toJson(array());
		}
		$this->load->model('m_navigasi');
		return toJson($this->m_navigasi->getTrustee($TipeMenu));
	}

	function getData(){
		$this->db->select('a.KodeUser as A_I,a.Username as User,a.MainImage , a.NamaUser as Nama, 
		 '.cTUp('a').', c.NamaKategoriUser');
		$this->db->where('a.IsActive', 1);
		$this->db->join('m_kategori_user c', 'a.KodeKategoriUser = c.KodeKategoriUser','left');
		return toJson($this->db->get('m_user a')->result());
	}
}
