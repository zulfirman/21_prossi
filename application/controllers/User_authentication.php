<?php
class User_authentication extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	function getCsr(){
		cekajax();
		$data = array(
			'csName' =>$this->security->get_csrf_token_name(),
			'csValue' => $this->security->get_csrf_hash()
		);
		return toJson($data);
	}

	function sendEmail(){
		$po=$this->input->post();
		$input=array('name','subject','email','message');
		foreach ($input as $val){
			if(!isset($po[$val])){
				return jsonError();
			}
			$data[$val]=$po[$val];
		}
		$pe=getPengaturan();
		$body= $this->load->view('email/email-view',$data,TRUE);
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://x-uni.net/api/email/send',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('sendTo' => $pe->Email,'subject' => $po['subject'],'message' => $body),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$result=json_decode($response);
		if($result->statusCode??500===200){
			$this->session->set_flashdata('fnotif','Message Sent.');
			return redirect(site_url('kontak?p='.uniqid()));
		}
		return jsonError(json_encode($response));
	}

	function sendEmailV1(){
		$po=$this->input->post();
		$input=array('name','subject','email','message');
		foreach ($input as $val){
			if(!isset($po[$val])){
				return jsonError();
			}
			$data[$val]=$po[$val];
		}
		$pe=getPengaturan();
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.sumurmonyo.com',
			'smtp_port' => 465,
			'smtp_crypto'   => 'ssl',
			'smtp_user' => 'no.reply@sumurmonyo.com',
			'smtp_pass' => 'passnyaprossi',
			'smtp_timeout' => '4',
			'mailtype' => 'html',
			'charset' => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($po['email'], $po['name']);
		$this->email->to($pe->Email);
		$this->email->subject(saniz($po['subject']));
		$body= $this->load->view('email/email-view',$data,TRUE);
		$this->email->message($body);
		$send=$this->email->send();
		if($send){
			$this->session->set_flashdata('fnotif','Message Sent.');
			return redirect(site_url('kontak?p='.uniqid()));
		}
		return jsonError($this->email->_debug_msg);
	}

	function subscribe(){

	}
	// Unset session and logout
	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
?>
