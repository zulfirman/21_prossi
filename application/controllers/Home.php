<?php

class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
	}

	function index(){
		$data['init']='';
		$data['titlePage']='Home';
		$view='front/home';
		$vie= $this->cache->model('view_model', 'view', array($view, $data));
		echo $vie;
	}
}
