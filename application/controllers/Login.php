<?php

class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$this->load->helper('cookie');
	}

	function index(){
		if (get_cookie('cusername') !== null && get_cookie('cpassword') !== null) {//cookie tidak kosong
			$_POST['Username'] = get_cookie('cusername');
			$_POST['Password'] = get_cookie('cpassword');
			if(isset($_GET['KodeMeja'])){
				$_POST['KodeMeja'] = 	$_GET['KodeMeja'];
			}
			return $this->proses(1);
		}
		if (isset($_SESSION['s_tipeuser'])) {
			if($_SESSION['s_tipeuser']==="1") {
				redirect(site_url('admin/dashboard'));
			}
		}
		return $this->load->view("login");
	}

	function proses($isCookie=0)
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			$po=$this->input->post();
			$this->db->select("KodeUser as s_kodeuser, Username as s_username, NamaUser as s_namauser,
			KodeKategoriUser as s_kodekategoriuser,MainImage as s_mainimage");
			if(isset($po['relog'])) {
				$this->db->where("KodeUser", $_SESSION['s_kodeuser']);
			}
			else{
				$this->db->where("Username", $po['Username']);
				if($isCookie===1){
					$this->db->where("Password", $po['Password']);
				}
				else{
					$this->db->where("Password", md5(sha1($po['Password'])));
				}
			}
			$data = $this->db->get("m_user",1)->row_array();
			if($data) {
				$this->db->select("TipeUser as s_tipeuser, Trustee as s_trustee, NamaKategoriUser as s_namakategori");//kategori get trustee
				$this->db->where("KodeKategoriUser", $data['s_kodekategoriuser']);
				$dataKategoriUser = $this->db->get("m_kategori_user",1)->row_array();
				$this->session->set_userdata($dataKategoriUser);
				$this->session->set_userdata('s_trustee', explode(',', $dataKategoriUser['s_trustee']));

				$this->session->set_userdata($data);// trustee master user
				//$_SESSION['KCFINDER']= array();
				$_SESSION['KCFINDER']['disabled']= false;

				if(isset($po['rememberMe'])){//set cookie
					$this->input->set_cookie('cusername', $po['Username'], time() + (86400));
					$this->input->set_cookie('cpassword', md5(sha1($po['Password'])), time() + (86400));
				}

				if(isset($po['relog'])) {
					return jsonSucces();
				}
				else{
					if(isset($po['redirect'])){
						redirect($po['redirect']);
					}
					if ($dataKategoriUser['s_tipeuser'] === "1") {
						redirect(site_url('admin/dashboard'));
					}
				}
			}
			else{
				$this->session->set_flashdata('fnotif','Username atau Password anda salah');
				$this->session->set_flashdata('input',$po);
				redirect('login');
			}
		}
	}

	public function logout() {
		session_destroy();
		delete_cookie('cusername');
		delete_cookie('cpassword');
		redirect(base_url('login'));
	}

	function get(){
		enableCors();
		return toJson(array('name'=>'asas'));
	}
}
