<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$this->load->model('view_model');
	}

	function index(){
		$blade='front/search';
		$data['init']='';
		$view=$blade;
		$vie= $this->cache->model('view_model', 'view', array($view, $data, $_GET));
		echo $vie;
	}
}
