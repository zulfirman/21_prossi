<?php

class Search extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		cekajax();
	}

	function index(){
		enableCors();
		$po=$this->input->post();
		$this->db->select('KodeArtikel,JudulId,Tipe,ContentId,MainImage,Lainnya1');
		$this->db->like('LOWER( JudulId )', strtolower($po['searchTerm']));
		$this->db->order_by('DateCreated', 'desc');
		$qry=$this->db->get('t_artikel', 10)->result();
		foreach ($qry as $key => $val) {
			$tipe='';
			if($val->Tipe==='1'){
				$tipe='Artikel';
			}
			if($val->Tipe==='2'){
				$tipe='Promo';
			}
			if($val->Tipe==='3'){
				$tipe='Pelayanan';
			}
			$qry[$key] = array(
				"id" => json_encode($qry[$key]),
				"text" => $val->JudulId.' | '.$tipe,
			);
		}
		return toJson($qry);
	}

	function get(){

	}
}
