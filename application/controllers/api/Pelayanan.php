<?php

class Pelayanan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
	}

	function kategori($rowno){
		$po=$this->input->post();

		$KodeKategori=explode(',', $po['KodeSubKategori']);
		$this->load->model('t_artikel_model');
		$this->load->library('pagination');

		$pageof= $rowno;
		$rowperpage = 12;// Row per page
		// Row position
		if($rowno != 0){$rowno = ($rowno-1) * $rowperpage;}
		// All records count
		$allcount = $this->getrecordCount($KodeKategori);
		// Get records
		$users_record = $this->getData($rowno,$rowperpage,$KodeKategori);
		// Pagination Configuration
		$config['base_url'] = site_url($this->uri->segment(1).'/');
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $allcount;
		$config['per_page'] = $rowperpage;
		$config['num_links']=1;
		// Initialize
		$ofpage=round($allcount/$rowperpage);
		if($pageof===0){$pageof=1;}
		$config['next_link'] = '>';
		$config['prev_link'] = '<';
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		//$config['full_tag_open'] = "<ul class='pagination'><li class='centerText'><span>Page $pageof of $ofpage:</span></li>";
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"> <a id="pageActive" class="page-link active-pagination" style="background-color: #CDAC65; border-color: #CDAC65" href="'.current_url().'">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['attributes'] = array('class' => 'page-link');
		$this->pagination->initialize($config);
		// Initialize $data Array
		$data['pagination'] = $this->pagination->create_links();
		$data['result'] = $users_record;
		$data['row'] = $rowno;
		$data['allcount']=$allcount;
		return toJson($data);
	}

	// Fetch records
	public function getData($rowno,$rowperpage,$Tipe) {
		$this->db->select('a.UrlArtikel,a.KodeKategori,a.JudulId,b.NamaKategoriId,a.Lainnya1');
		$this->db->join('m_kategori b', 'a.KodeKategori = b.KodeKategori','left');
		$this->db->join('m_kategori c', 'b.KodeKategoriParent = c.KodeKategori','left');
		$this->db->where_in('a.KodeKategori', $Tipe);
		$this->db->limit($rowperpage, $rowno);
		$this->db->order_by('b.NamaKategoriId');
		$this->db->from('t_artikel a');
		$query = $this->db->get();
		return $query->result();
	}

	// Select total records
	public function getrecordCount($Tipe) {
		$this->db->select('count(KodeKategori) as allcount');
		$this->db->where_in('KodeKategori', $Tipe);
		$this->db->from('t_artikel a');
		$query = $this->db->get()->first_row();
		return $query->allcount;
	}
}
