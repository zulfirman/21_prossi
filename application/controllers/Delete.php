<?php

class Delete extends CI_Controller
{
	var $_link='./assets/uploads/';

	function index(){
		if (@!$_SERVER['HTTP_REFERER']) {
			exit;
			$url = site_url();
			echo "<script>location.href=$url</script>";
		}
		$po=$this->input->post();
		$table=$po['namaTabel'];
		$namaId=$po['namaId'];
		$id=$po['id'];
		$gambar=$po['gambar'];
		$isactive=$po['isactive'];
		if($isactive) {
			$params['IsActive'] = 0;
			$params['DateUpdated'] = tglSekarang(3);
			$params['UpdatedBy'] = $_SESSION['s_kodeuser'];
			$this->db->where($namaId, $id);
			$this->db->update($table, $params);
		}else{
			if($table==='t_comment'){//delete sub komen
				$this->db->where('KodeKomen', $id);
				$this->db->delete('t_sub_comment');
			}
			$this->db->delete($table, array($namaId => $id));
		}
		if($gambar!=='') {
			if(!in_array($gambar,['default.jpg','icon_4.svg','loading.jpg'])){
				if (file_exists("$this->_link$gambar")) {
					unlink("$this->_link$gambar");
				}
			}
		}
		return jsonSucces();
	}

	function reset(){
		if(!isset($_POST['KodeCabang'])){
			return jsonError();
		}
		$this->db->select('Password');
		$this->db->where('KodeUser', $_SESSION['s_kodeuser']);
		$getPass=$this->db->get('m_user')->first_row();
		if($getPass->Password===md5(sha1($_POST['Password']))) {
			foreach ($this->input->post('KodeCabang') as $KodeCabang) {
				foreach (array('t_batal_pesan', 't_detail_penjualan', 't_komposisi_keluar', 't_komposisi_masuk', 't_penjualan'
						 , 't_s_detail_penjualan','m_customer','m_detail_modifier','m_stok_menu') as $val) {
					if($val==='m_detail_modifier'){
						$this->db->where('IsActive', 0);
						$this->db->where('KodeCabang', $KodeCabang);
						$this->db->delete($val);
					}
					else if($val==='m_stok_menu'){
						$this->db->where('KodeCabang', $KodeCabang);
						$this->db->update($val, array('Stok' => 0));
					}
					else{
						$this->db->where('KodeCabang', $KodeCabang);
						$this->db->delete($val);
					}
				}
			}
			return jsonSucces();
		}
		else{
			return toJson(array('statusCode'=>0));
		}
	}
}
