<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends CI_Controller {

	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
		$this->load->model('t_comment');
	}

	function index(){
		cekajax();
		$post=$this->input->post();
		$KodeArtikel= $post['KodeArtikel'];
		$mainjson=array();
		// -------komen
		$getcomments=$this->t_comment->comments($KodeArtikel);
		$totalsubcomments=0;
		foreach ($getcomments as $r){
			$r['Avatar']=$this->getGravatar($r['Email'],'80','retro');
			unset($r['Email']);
			if($r['HasSubKomen']==true){
				//jika ada komen query subkomen
				$this->db->select('KodeSubKomen,KodeKomen,Nama,Website,IsiKomen,Tanggal,Email');
				$this->db->where('StatusKomen', 1);
				$this->db->order_by('Tanggal', 'asc');
				$this->db->where('KodeKomen',$r['KodeKomen']);
				$sub = $this->db->get('t_sub_comment')->result_array();
				$i=-1;
				foreach ($sub as $rSub){
					$i++;
					$sub[$i]['Avatar']=$this->getGravatar($sub[$i]['Email'],'80','retro');
					unset($sub[$i]['Email']);
				}
				//buat new array key berbentuk array
				$mainjson[]=$r+array('SubKomen' => $sub);
				$totalsubcomments+=count($sub);
			}
			else{
				$mainjson[]=$r;
			}
		}
		$data['comments']=$mainjson;

		$this->db->select('count(KodeKomen) as total, KodeKomen');
		$this->db->where('KodeArtikel', $KodeArtikel);
		$totalcomments = $this->db->get('t_comment')->first_row();
		$data['last']=$totalcomments->total+$totalsubcomments;
		$datasend['data']=$data;
		// -------komen
		return toJson($data);
	}

	function submit(){
		$post = $this->input->post();
		cekajax();
		$KodeArtikel = $post['KodeArtikel'];
		$value = array(
			'KodeKomen' => date('y') . uniqid(),
			'KodeArtikel' => $KodeArtikel,
			'Nama' => $post['Nama'],
			'Email' => $post['Email'],
			'Website' =>'',
			'IsiKomen' => $post['IsiKomen'],
			'Tanggal' => date('Y-m-d H:i:s'),
			'HasSubKomen' => 0,
			'StatusKomen' => 1,
		);
		$this->db->insert('t_comment', $value);

		$this->db->select('UrlArtikel,JudulId');
		$this->db->where('KodeArtikel', $KodeArtikel);
		$qry=$this->db->get('t_artikel', 1)->first_row();
		$value=array(
			'IdNotification' => date('y') . uniqid(),
			'Type' => $post['Type'],
			'Content' => $post['Nama'].' mengomentari '.$qry->JudulId,
			'Idnya' => $qry->UrlArtikel,
			'DateAdded' => date('Y-m-d H:i:s'),
			'IsRead' => 0,
			'IsOpen' => 0,
		);
		$this->db->insert('t_notifikasi', $value);
		return jsonSucces();
	}

	function submitSub(){
		$post=$this->input->post();
		cekajax();
		$KodeKomen=$post['bkb'];
		$value=array(
			'KodeSubKomen' => date('y') . uniqid(),
			'KodeKomen' => $KodeKomen,
			'KodeArtikel' => $post['rKodeArtikel'],
			'Nama' =>$post['rNama'],
			'Email' =>$post['rEmail'],
			'Website' =>'',
			'IsiKomen' => "@".$post['ReplyTo']." ".$post['rIsiKomen'],
			'Tanggal' => date('Y-m-d H:i:s'),
			'StatusKomen' => 1,
		);
		$this->db->insert('t_sub_comment', $value);
		$value=array(
			'HasSubKomen' => 1,
		);
		$this->db->where('KodeKomen', $KodeKomen);
		$this->db->update('t_comment', $value);

		$this->db->select('UrlArtikel,JudulId');
		$this->db->where('KodeArtikel', $post['rKodeArtikel']);
		$qry=$this->db->get('t_artikel', 1)->first_row();
		$value=array(
			'IdNotification' => date('y') . uniqid(),
			'Type' => $post['rType'],
			'Content' => $post['rNama'].' mengomentari '.$qry->JudulId,
			'Idnya' => $qry->UrlArtikel,
			'DateAdded' => date('Y-m-d H:i:s'),
			'IsRead' => 0,
			'IsOpen' => 0,
		);
		$this->db->insert('t_notifikasi', $value);

		return jsonSucces();
	}

	function getGravatar( $email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array() ) {
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		return $url;
	}

	function getNotif(){
		cekajax();
		$this->db->limit(10);
		$this->db->order_by('DateAdded','desc');
		return toJson($this->db->get('t_notifikasi')->result_array());
	}

	function readNotif(){
		cekajax();
		$data=array(
			'IsRead' => 1
		);
		$this->db->where('IsRead', '0');
		$this->db->update('t_notifikasi', $data);
		jsonSucces();
	}
}
