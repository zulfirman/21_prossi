<?php

class Pelayanan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
	}

	function index(){
		$data['init']='';
		$view='front/pelayanan';
		$data['titlePage']='Service';
		$vie= $this->cache->model('view_model', 'view', array($view, $data));
		echo $vie;
	}

	function main(){
		$urlParam=$this->uri->segment(2);
		$data['init'] = $urlParam;
		$view='front/pelayanan-detail';
		if(!isset($_GET['id'])){//jika tidak ada sub categ berarti kategori
			$this->db->select('NamaKategoriId,KodeKategori,DeskripsiId');
			$this->db->where('NamaKategori', $urlParam);
			$data['pelayananKategori']= $this->db->get('m_kategori')->first_row();
			if($data['pelayananKategori']===null){
				return $this->load->view('errors/li404');
			}
			$data['titlePage']=$data['pelayananKategori']->NamaKategoriId;
			$view='front/pelayanan';
		}else{//detail pelayanan
			$this->db->where('UrlArtikel', $urlParam);
			$this->db->where('KodeKategori', $_GET['id']);
			$this->db->from('t_artikel');
			$query = $this->db->get();
			$data['isiArtikel']= $query->first_row();
			$data['titlePage']=$data['isiArtikel']->JudulId;
		}
		$vie= $this->cache->model('view_model', 'view', array($view, $data));
		echo $vie;
	}
}
