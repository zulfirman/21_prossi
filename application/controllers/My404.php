<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My404 extends CI_Controller {

	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
	}

	public function index()
	{
		if(isset($_SESSION['s_kodeuser'])){
			return $this->load->view('errors/li404');
		}
		else{
			$data['init']='';
			$view='errors/li404';
			$vie= $this->cache->model('view_model', 'view', array($view, $data));
			echo $vie;
		}
	}

}
