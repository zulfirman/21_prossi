<?php

class Artikel extends CI_Controller
{
	function __construct()
	{
		parent::__construct();$this->ci_minifier->enable_obfuscator();
	}

	function home(){
		$data['init']='';
		if($this->uri->segment(2)===''){
			$rowno=0;
		}else{
			$rowno=$this->uri->segment(2);
		}
		$vie= $this->cache->model('view_model', 'list_artikel', array($rowno));
		echo $vie;
	}
	function artikel_detail($id){
		$data['url']=$id;
		$this->db->select('a.KodeArtikel,a.JudulId,a.JudulEn,a.DateCreated,a.KodeKategori,
		a.ContentId,a.ContentEn, b.NamaUser,a.MainImage as ArtikelImage, a.EnableComment, 
		b.MainImage as ProfileImage, b.Pekerjaan, b.AboutMeId, b.AboutMeEn, a.Tipe');
		$this->db->join('m_user b', 'a.CreatedBy = b.KodeUser');
		$this->db->where('a.UrlArtikel', $id);
		$this->db->order_by('a.DateCreated', 'desc');
		$data['isi'] = $this->db->get('t_artikel a', 1)->first_row();
		if (!$data['isi']) {
			return $this->load->view('errors/li404');
		}
		$data['mainTitle']=$data['isi']->JudulId;

		if($data['isi']->Tipe==="1"){
			$data['judulHalaman']="Artikel";
		}else if($data['isi']->Tipe==="2"){
			$data['judulHalaman']="Promo";
		}else if($data['isi']->Tipe==="3"){
			$data['judulHalaman']="Pelayanan";
		}
		$data['init']='';
		$view='front/artikel-detail';
		$vie= $this->cache->model('view_model', 'view', array($view, $data));
		echo $vie;
	}
}
