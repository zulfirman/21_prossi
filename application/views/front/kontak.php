<?php include APPPATH . 'views/front/_partials/header.php'; ?>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/contact.min.css">
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/contact_responsive.min.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>
<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$bHeader->bKontak?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title"><?=$tHeader->tKontak?></div>
						<div class="home_text select-box choose-position">
							<select class="form-control" name="allSearch" id="allSearch" required>
								<option value="">-</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Contact -->

<div class="contact">
	<div class="container">
		<div class="row">

			<!-- Contact Form -->
			<div class="col-md-12 mb-4">
				<?php include APPPATH . 'views/front/_part/kontak-form.php'; ?>
			</div>
		</div>
		<div class="row google_map_row"  id="divMap">
			<div class="col">

				<!-- Contact Map -->

				<div class="contact_map">

					<!-- Google Map -->

					<div class="map">
						<div id="google_map" class="google_map">
							<div class="map_container">
								<div id="mapContent"></div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
</div>

<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
		$.ajax({
			url: baseUrl+'user-authentication/getCsr',
			type: "get",
			processData: false,
			contentType: false,
			success: function (data) {
				document.getElementsByName('blazn')[0].value=data.csValue;
			}
		});
	});
</script>
