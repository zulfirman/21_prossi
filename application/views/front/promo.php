<?php include APPPATH . 'views/front/_partials/header.php'; ?>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about.min.css">
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about_responsive.min.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>
<style>
	.why{
		padding-top: 70px;
		padding-bottom: 81px;
	}
	.margin-card-body{
		margin-bottom: -15%;
	}
</style>
<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$bHeader->bPromo?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title mb-4"><?=$tHeader->tPromo?></div>
						<div class="home_text select-box choose-position">
							<select class="form-control" name="allSearch" id="allSearch" required>
								<option value="">-</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="why">
	<!-- <div class="background_image" style="background-image:url(images/why.jpg)"></div> -->
	<div class="container">
		<div class="row row-eq-height">
			<!-- Why Choose Us Content -->
			<div class="col-lg-12 order-lg-2 order-1">
				<div class="why_content">
					<div class="col text-center">
						<div class="section_title_container">
							<!--<div class="section_title mb-4"><h3>Promo</h3></div>-->
						</div>
					</div>
					<div class="row services_row">
						<div class="container">
							<div class="row">
								<div class="owl-carousel owl-theme promo_slider">
									<?php
									$this->db->where('Tipe', 2);
									$this->db->order_by('DateCreated', 'desc');
									$promoMain = $this->db->get('t_artikel', 15)->result();
									foreach ($promoMain as $val){?>
										<div class="col-md-12 owl-item">
											<div hidden class="extra_container d-flex flex-row align-items-start justify-content-end">
												<div class="extra_content bemas">
													<a style="cursor: pointer" onclick="lihatPromo('<?=$val->JudulId?>','<?=$val->KodeArtikel?>','<?=$linkUpload.$val->MainImage?>')">
														<img src="<?=$linkUpload.$val->MainImage?>" alt="Card Image">
													</a>
												</div>
												<div hidden id="content<?=$val->KodeArtikel?>">
													<?=$val->ContentId?>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="scrollTo" class="mb-4"><br><br></div>
			</div>
		</div>
	</div>
	<div class="container mb-4">
		<div class="row">
			<div class="col-md-12"><br></div>
			<div class="col-md-12">
				<div class="col-md-12 mb-4">
					<h3 id="judul" style="text-align: center"></h3>
				</div>
				<div class="col-md-12 text-center">
					<img id="imagenya" src="" alt="" class="img-responsive">
				</div>
				<div class="col-md-12" id="content">

				</div>
			</div>
			<div class="col-md-12"><br></div>
		</div>
	</div>
</div>

<?php /*include APPPATH . 'views/front/_part/pelayanan.php'; */?>

<!-- Testimonials -->

<?php /*include APPPATH . 'views/front/_part/testimoni.php'; */?>

<!-- Call to action -->

<?php include APPPATH . 'views/front/_part/janji.php'; ?>

<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
<script>
	var pageno=0;
	var kodeKategori=
	$(document).ready(function () {
		initTestSlider();
		lihatPromo('<?=$promoMain[0]->JudulId?>','<?=$promoMain[0]->KodeArtikel?>','<?=$linkUpload.$promoMain[0]->MainImage?>', 0);
	});
	function lihatPromo(Judul,KodeArtikel, Image,Scroll=1){
		$('#judul').html(Judul);
		$('#imagenya').prop('src', Image);
		$('#content').html('<br><br>'+$('#content'+KodeArtikel).html());
		if(Scroll)
		$('html, body').animate({
			scrollTop: $('#scrollTo').offset().top
		}, 500);
	}
</script>
