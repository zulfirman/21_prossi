<?php include APPPATH . 'views/front/_partials/header.php'; ?>
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/blog.min.css">
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/blog_responsive.min.css">
	
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/artikel.css">
<?php
$TipeArtikel=$isi->Tipe;
$tipenya='';
if(strtolower($this->uri->segment(1))!=='pelayanan') {
	$tipenya= ucwords($this->uri->segment(1));
}else{
	$tipenya= ucwords(str_replace('-',' ',$this->uri->segment(2)??''));
}
include APPPATH . 'views/front/_part/bgcss.php'; ?>
<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$pe->BackgroundHeader?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title"><?=$judulHalaman?></div>
						<div class="home_text" hidden>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<meta property="og:image" content="<?=$linkUpload.$isi->ArtikelImage?>" />
<!-- ##### Breadcrumb Area End ##### -->

<!-- ##### Blog Content Area Start ##### -->

<section class="blog">
	<div class="container">
		<div class="row justify-content-center">
			<!-- Blog Posts Area -->
			<div class="col-12 col-md-8">
				<div class="blog-posts-area">

					<!-- Post Details Area -->
					<div class="single-post-details-area">
						<div class="post-content">
							<h4 class="post-title"><?= $lang === "Id" ? $isi->JudulId : $isi->JudulEn?></h4>
							<div class="post-meta mb-30">
								<a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i><?=$isi->DateCreated?></a>
								<a href="#"><i class="fa fa-user" aria-hidden="true"></i><?=$isi->NamaUser?></a>
							</div>
							<div class="post-thumbnail mb-30" style="text-align: center">
								<img src="<?=$linkUpload.$isi->ArtikelImage?>" alt="">
							</div>
							<?= $lang === "Id" ? $isi->ContentId : $isi->ContentEn?>
						</div>
					</div>

					<!-- Post Tags & Share -->
					<div class="post-tags-share d-flex justify-content-between align-items-center">
						<!-- Tags -->
						<ol class="popular-tags d-flex align-items-center flex-wrap">
							<li><span>Tag:</span></li>
							<?php
							$listCategori=explode(',',$isi->KodeKategori);
							sort($listCategori);
							foreach ($listCategori as $listCategoriv) {
								$this->db->select('NamaKategoriId, NamaKategoriEn');
								$this->db->where('KodeKategori', $listCategoriv);
								$qry = $this->db->get('m_kategori', 1)->first_row();
								?>
								<li><a href="<?=$si.'search?category='?><?=$lang === "Id" ? $qry->NamaKategoriId : $qry->NamaKategoriEn?>"><?=$lang === "Id" ? $qry->NamaKategoriId : $qry->NamaKategoriEn?></a></li>
							<?php } ?>
						</ol>
						<!-- Share -->
						<div class="post-share">
							<a href="https://www.facebook.com/sharer/sharer.php?u=<?=$cu?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<a href="https://www.twitter.com/share?url=<?=$cu?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
							<a href="#" hidden><i class="fa fa-google-plus" aria-hidden="true"></i></a>
							<a href="#" hidden><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</div>
					</div>

					<!-- Comment Area Start -->
					<?php
					if($isi->EnableComment==="1"){
						?>
						<div class="comment_area clearfix">
							<h4 class="headline" id="pTotalComments">0 Comments</h4>

							<ol>
								<!-- Single Comment Area -->
								<li class="single_comment_area" id="pComments">

								</li>
								<li style="text-align: center">
									<button type="button" id="btnLoadMore" class="btn button_1 trans_200" onclick="offSet=limitKomen;limitKomen=limitKomen+10;loadComments();">Load More</button>
								</li>
							</ol>
						</div>

						<!-- Leave A Comment -->
						<div class="leave-comment-area clearfix">
							<div class="comment-form">
								<h4 class="headline">Leave A Comment</h4>

								<div class="contact-form-area">
									<!-- Comment Form -->
									<form action="#" id="formComments">
										<input type="text" id="KodeArtikel" name="KodeArtikel" style="display: none" readonly hidden value="<?=$isi->KodeArtikel?>">
										<input type="text" id="Type" name="Type" style="display: none" readonly hidden value="<?=$TipeArtikel?>">
										<div class="row">
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label for="Nama"></label>
													<input type="text" class="form-control" id="Nama" name="Nama" placeholder="Name">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label for="Email"></label>
													<input type="email" class="form-control" id="Email" name="Email" placeholder="Email">
												</div>
											</div>
											<div class="col-12">
												<div class="form-group">
													<label for="Email"></label>
													<textarea class="form-control" name="IsiKomen" id="IsiKomen" cols="30" rows="10" placeholder="Comment"></textarea>
												</div>
											</div>
											<div class="col-12">
												<button type="submit" class="btn btn-danger">Post Comment</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>

					<?php } ?>
				</div>
			</div>

			<!-- Blog Sidebar Area -->
			<div class="col-12 col-sm-9 col-md-4">
				<div class="post-sidebar-area">

					<!-- ##### Single Widget Area ##### -->
					<div class="single-widget-area">
						<form action="<?=$si.'search'?>" method="get" class="search-form">
							<input type="search" name="keywords" id="widgetsearch" placeholder="Search..." autocomplete="off">
							<button type="submit"><i class="icon_search"></i></button>
							<input name="type" value="<?=strtolower($tipenya)?>" hidden>
							<input name="category" value="all" hidden disabled>
						</form>
					</div>

					<!-- ##### Single Widget Area ##### -->
					<div class="single-widget-area">
						<!-- Author Widget -->
						<div class="author-widget">
							<div class="author-thumb-name d-flex align-items-center">
								<div class="author-thumb">
									<img src="<?=$linkUpload.$isi->ProfileImage?>" alt="">
								</div>
								<div class="author-name">
									<h5><?=$isi->NamaUser?></h5>
									<p><?=$isi->Pekerjaan?></p>
								</div>
							</div>
							<p><?=$lang === "Id" ? $isi->AboutMeId : $isi->AboutMeEn?></p>
							<div class="social-info">
								<!--<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>-->
							</div>
						</div>
					</div>

					<?php include '_part/latest-article.php'?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include APPPATH . 'views/front/_part/janji.php'; ?>
<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
<script>
	/*	var newTitle="";
		if (document.title !== newTitle) {
			document.title = newTitle;
		}*/
	$('meta[name="description"]').attr("content", "<?=substr((strip_tags($isi->ContentId)),0,400) ?>");
	$(document).ready(function () {
		loadComments();
		$('#formComments').submit(function (e) {
			e.preventDefault();
			var Nama = $("#Nama");
			var Email = $("#Email");
			var IsiKomen = $("#IsiKomen");
			if(Nama.val()===""){Nama.focus();}
			else if(Email.val()===""){Email.focus();}
			else if(IsiKomen.val()===""){IsiKomen.focus();}
			else {
				$.ajax({
					url: baseUrl+'comment/submit',
					type: "post",
					data: new FormData(this),
					processData: false,
					contentType: false,
					cache: true,
					async: true,
					success: function (data) {
						$("#pComments").empty();
						loadComments();
						$("#Nama").val('');
						$("#Email").val('');
						$("#IsiKomen").val('');
					}
				});
			}
		});
	});
	var limitKomen=10;
	var offSet=0;
	var totalComments=0;
	function loadComments() {
		$.ajax({
			url: baseUrl + 'comment', type: 'POST',
			data: {KodeArtikel: '<?=$isi->KodeArtikel?>',limitKomen:limitKomen,offSet:offSet},
			success: function (val) {
				//Pace.restart();
				$.each(val.comments, function (index, val) {
					totalComments++;
					var mainKomen=val.KodeKomen;
					var rowData=`<div class="comment-wrapper d-flex">
					<div class="comment-author">
					<img src="${val.Avatar}" alt="">
					</div>
					<div class="comment-content">
					<div class="d-flex align-items-center justify-content-between">
					<h5>${val.Nama}</h5>
					<span class="comment-date" style="color: black">${val.Tanggal}</span>
					</div>
					<p>${val.IsiKomen}</p>
					<a class="active" style="cursor: pointer" onclick="showReply('re${mainKomen}','${val.KodeKomen}','${val.Nama}');">Reply</a>
					</div>
					</div>
					<div id="re${mainKomen}"></div><div id="${mainKomen}">`;
					$("#pComments").append(rowData);

					if(val.HasSubKomen==="1"){
						$("#"+mainKomen).empty();
						$.each(val.SubKomen, function (index, val) {
							totalComments++;
							var subkomen=`<ol class="children">
							<li class="single_comment_area">
							<div class="comment-wrapper d-flex">
							<div class="comment-author">
							<img src="${val.Avatar}" alt="">
							</div>
							<div class="comment-content">
							<div class="d-flex align-items-center justify-content-between">
							<h5>${val.Nama}</h5>
							<span class="comment-date">${val.Tanggal}</span>
							</div>
							<p>${val.IsiKomen}</p>
							<a class="active" style="cursor: pointer" onclick="showReply('re${val.KodeSubKomen}','${val.KodeKomen}','${val.Nama}');">Reply</a>
							</div>
							</div>
							</li>
							</ol><div id="re${val.KodeSubKomen}"></div>`;
							$("#"+mainKomen).append(subkomen);
						});
					}
				});
				$('#pTotalComments').html(val.last+" Comments");
				if(totalComments>=val.last){
					$('#btnLoadMore').css('display','none');
				}
			}
		});
	}
	function showReply(e,bkb,to){
		$('.comment-reply').remove();
		var rowdata=`<div class="leave-comment-area clearfix comment-reply" style="border-top: none">
		<form action="#" id="formReply">
		<div class="comment-form">
		<input type="text" id="bkb" name="bkb" style="display: none" readonly hidden>
		<input type="text" id="rKodeArtikel" name="rKodeArtikel" style="display: none" readonly hidden>
		<input type="text" id="rType" name="rType" style="display: none" readonly hidden>
		<h4 class="headline">Reply</h4>
		<div class="contact-form-area">
		<form action="#" method="post">
		<div class="row">
		<div class="col-12 col-md-6">
		<div class="form-group">
		<input type="text" class="form-control" id="rNama" name="rNama" placeholder="Name">
		</div>
		</div>
		<div class="col-12 col-md-6">
		<div class="form-group">
		<input type="email" class="form-control" id="rEmail" name="rEmail" placeholder="Email">
		</div>
		</div>
		<div class="col-12">
		<div class="form-group">
		<input type="text" placeholder="Reply To" id="ReplyTo" name="ReplyTo" style="display: none" readonly>
		<textarea class="form-control" id="rIsiKomen" name="rIsiKomen" cols="30" rows="10" placeholder="Comment"></textarea>
		</div>
		</div>
		<div class="col-6">
		<button type="submit" class="btn btn-light" onclick="$('.comment-reply').remove();">Cancel</button>
		</div>
		<div class="col-6" style="text-align: left">
		<button type="submit" class="btn btn-danger">Post Reply</button>
		</div>
		</div>
		</form>
		</div>
		</div>
		</form>
		<br><br></div>`;
		$('#'+e).append(rowdata);
		$('#bkb').val(bkb);//id comment
		$('#rKodeArtikel').val('<?=$isi->KodeArtikel?>');
		$('#rType').val('<?=$TipeArtikel?>');
		$('#ReplyTo').val(to);
		$('#formReply').submit(function (e) {
			e.preventDefault();
			var Nama = $("#rNama");
			var Email = $("#rEmail");
			var IsiKomen = $("#rIsiKomen");
			if(Nama.val()===""){Nama.focus();}
			else if(Email.val()===""){Email.focus();}
			else if(IsiKomen.val()===""){IsiKomen.focus();}
			else {
				$.ajax({
					url: baseUrl+'comment/submitSub',
					type: "post",
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: function (data) {
						$("#pComments").empty();
						loadComments();
					}
				});
			}
		});
	}
</script>
