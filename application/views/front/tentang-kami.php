<?php include APPPATH . 'views/front/_partials/header.php'; ?>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about.min.css">
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about_responsive.min.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>
<style>
	.team_text {
		text-align: justify;
		word-wrap: break-word;
	}
	.intro {
		padding-top: 0;
	}
	.team {
		padding-bottom: 0;
	}
	@media only screen and (max-width: 765px) {
		.team_image img {
			width: 75%;
		}
	}
	.modalimage{
		margin-left: auto;
		margin-right: auto;
		width: 80%;
	}
</style>
<?php
$this->db->where('Tipe', 3);
$this->db->order_by('Lainnya1');
$team = $this->db->get('t_gambar')->result();
?>
<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$bHeader->bTentang?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title"><?=$tHeader->tTentang?></div>
						<div class="home_text select-box choose-position">
							<select class="form-control" name="allSearch" id="allSearch" required>
								<option value="">-</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Team -->
<div class="team">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<div class="section_subtitle" hidden>This is Dr Pro</div>
					<div class="section_title"><h2><br>Meet Our Professionals</h2></div>
				</div>
			</div>
		</div>
		<div class="row team_row">

			<!-- Team Item -->
			<?php foreach ($team as $val){?>
				<div class="col-lg-3 col-6 mb-4">
					<div class="card border-0 transform-on-hover h-100" style="background-color: transparent">
						<a class="lightbox" style="cursor:pointer;" onclick="tampilModal('<?=$val->KodeGambar?>','<?=$linkUpload.$val->MainImage?>','<?=$val->Lainnya1?>','<?=$val->Lainnya2?>')">
							<img src="<?=$linkUpload.'loading.jpg'?>" class="lazy card-img-top" data-src="<?=$linkUpload.$val->MainImage?>" alt="Card Image">
						</a>
						<div class="card-body">
							<a style="cursor: pointer" onclick="tampilModal('<?=$val->KodeGambar?>','<?=$linkUpload.$val->MainImage?>','<?=$val->Lainnya1?>','<?=$val->Lainnya2?>')">
								<h5><?=$val->Lainnya1?></h5>
								<h6 class="pemas"><?=$val->Lainnya2?></h6>
								<div id="jadwal<?=$val->KodeGambar?>" hidden><?=$val->Lainnya3?></div>
							</a>
						</div>
					</div>
				</div>

				<!--<div hidden class="col-md-4 team_col h-100">
					<div class="team_item text-center d-flex flex-column aling-items-center justify-content-end">
						<div class="team_image"><img src="https://localhost/21_prossi/assets/uploads/2114674902ad2e7.jpg" alt=""></div>
						<div class="team_content text-center">
							<div class="team_name"><a href="#"><?/*=$val->Lainnya1*/?></a></div>
							<div class="team_title"><?/*=$val->Lainnya2*/?></div>
							<div class="team_text">
								<p><?/*=$val->Lainnya3*/?></p>
							</div>
						</div>
					</div>
				</div>-->
			<?php } ?>
		</div>
	</div>
</div>

<?php include APPPATH . 'views/front/_part/tentang-part.php'; ?>

<!-- Testimonials -->

<?php include APPPATH . 'views/front/_part/testimoni.php'; ?>

<!-- Call to action -->

<?php include APPPATH . 'views/front/_part/janji.php'; ?>

<div class="modal fade bs-example-modal-lg show " id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd">
	<div class="modal-dialog modal-lg box-shadow" role="document">
		<div class="modal-content bpinkmuda">
			<div class="modal-header">
				<h3 class="modal-title"></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body bpinkmuda">
				<div class="row">
					<div class="col-md-6 mb-4">
						<div class="card border-0 transform-on-hover h-100" style="background-color: transparent">
							<img id="gambarModal" src="" alt="Card Image" class="card-img-top modalimage">
							<div class="card-body">
								<h5 id="nama">Doaa</h5>
								<h6 class="pemas" id="jabatan">asas</h6>
							</div>
						</div>
					</div>
					<div class="col-md-6" id="jadwal">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>


<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
		initTestSlider();
	});
	function tampilModal(KodeGambar,gambar,nama,jabatan,jadwal){
		$('#modalAdd').modal('show');
		$('#nama').html(nama);
		$('#gambarModal').attr('src', gambar);
		$('#jabatan').html(jabatan);
		$('#jadwal').html('<br><br>'+$('#jadwal'+KodeGambar).html());
	}
</script>
