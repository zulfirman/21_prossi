<?php include APPPATH . 'views/front/_partials/header.php'; ?>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about.min.css">
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about_responsive.min.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>
<style>
	.service_title {
		font-size: 20px;
		font-weight: 600;
		color: #404040;
		line-height: 1.2;
		margin-top: 17px;
	}
	.why{
		padding-top: 70px;
		padding-bottom: 81px;
	}
	.margin-card-body{
		margin-bottom: -15%;
	}
	/*gallery filter */

	.gallery-title:after {
		content: "";
		position: absolute;
		width: 7.5%;
		left: 46.5%;
		height: 45px;
		border-bottom: 1px solid #5e5e5e;
	}

	.port-image
	{
		width: 100%;
	}

	.gallery_product
	{
		margin-bottom: 30px;
	}

	/*end gallery filter */
</style>
<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$bHeader->bPelayanan?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title" style="font-size: 50px"><?=$pelayananKategori->NamaKategoriId?></div>
						<div class="home_text" style="color: black"><?=$pelayananKategori->DeskripsiId?></div>
						<div class="home_text select-box choose-position">
							<br>
							<select class="form-control" name="allSearch" id="allSearch" required style="width: 10%">
								<option value="">-</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="why">
	<!-- <div class="background_image" style="background-image:url(images/why.jpg)"></div> -->
	<div class="container">
		<div class="row row-eq-height">
			<!-- Why Choose Us Content -->
			<div class="col-lg-12 order-lg-2 order-1">
				<div class="why_content">
					<div class="col text-center">
						<div class="section_title_container">
							<div class="section_title"><h3>Layanan Kami</h3></div>
						</div>
					</div>
					<div class="row services_row">
						<div class="container">
							<div class="row gallery-block cards-gallery">
								<!--<div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
									<h1 class=""></h1>
								</div>-->

								<div class="text-center col-md-12 mb-5" id="listPelayanan">
									<?php
									$this->db->select('KodeKategori, NamaKategoriId');
									$this->db->where('KodeKategoriParent', $pelayananKategori->KodeKategori);
									$this->db->where('IsActive', 1);
									$qryService = $this->db->get('m_kategori')->result();
									$kKategoriArray=array();
									foreach ($qryService as $val) {
										$kKategoriArray[] = $val->KodeKategori;
									} ?>
									<button class="btn btngold filter-button mr-2 allfilter" onclick="filternya('<?=implode(",", $kKategoriArray)?>', 'all')" data-filter="all">All</button>
									<?php foreach ($qryService as $val) { ?>
										<button class="btn btngold filter-button mr-2" onclick="filternya('<?=$val->KodeKategori?>')" data-filter="<?=$val->KodeKategori?>"><?=$val->NamaKategoriId?></button>
									<?php } ?>
								</div>


								<!--<div class="gallery_product col-lg-3 col-6 mb-4 filter hdpe">
									<img src="http://fakeimg.pl/365x365/" class="img-responsive">
								</div>-->

							</div>
							<div class="row page_nav_row">
								<div class="col">
									<div class="page_nav">
										<ul class="d-flex flex-row align-items-center justify-content-cente img-responsive" id="pagination" style="font-size: 18px">

										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php /*include APPPATH . 'views/front/_part/pelayanan.php'; */?>

<!-- Testimonials -->

<?php include APPPATH . 'views/front/_part/testimoni.php'; ?>

<!-- Call to action -->

<?php include APPPATH . 'views/front/_part/janji.php'; ?>

<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
<script>
	var pageno=0;
	var KodeSubKategoriAwal='';
	$(document).ready(function () {
		initTestSlider();
		/*var filteButton=$(".filter-button");
		filteButton.click(function(){
			var value = $(this).attr('data-filter');
			if(value === "all")
			{
				$('.filter').show('1000');
			}
			else
			{
				$(".filter").not('.'+value).hide('3000');
				$('.filter').filter('.'+value).show('3000');
			}
		});
		if (filteButton.removeClass("active")) {
			$(this).removeClass("active");
		}
		$(this).addClass("active");*/
		$('.allfilter').click();
		$('#pagination').on('click', 'a', function (e) {
			e.preventDefault();
			pageno = $(this).attr('data-ci-pagination-page');
			filternya();
			$('html, body').animate({
				scrollTop: $('#listPelayanan').offset().top
			}, 500);
		});
	});
	function filternya(KodeSubKategori='', namakateg=''){
		if(KodeSubKategori===''){//jika pagination di click
			KodeSubKategori=KodeSubKategoriAwal;
		}else{
			pageno = 0;
		}
		$('#listPelayanan').nextAll().hide('3000');
		$.ajax({
			url: baseUrl + 'api/pelayanan/kategori/'+pageno, type: 'POST',
			data: {KodeSubKategori: KodeSubKategori},
			success: function (data) {
				KodeSubKategoriAwal=KodeSubKategori;
				$('#pagination').html(data.pagination);
				$('#listPelayanan').nextAll().remove();
				var html='';
				$.each(data.result, function (index, val) {
					var linknya=baseUrl+'pelayanan/'+val.UrlArtikel+"?id="+val.KodeKategori;
					var judulService='';
					if(namakateg==='all'){
						judulService=val.JudulId+' - '+val.NamaKategoriId;
					}else{
						judulService=val.JudulId;
					}
					html+=`<div class="gallery_product col-lg-3 col-6 mb-4 filter" style="padding-right: 9px;padding-left: 9px;">
									<div class="card border-0 transform-on-hover h-100">
										<a class="lightbox extra_content" href="${linknya}">
											<img src="<?=$linkUpload.'loading.jpg'?>" class="lazy card-img-top" data-src="${urlImage+val.Lainnya1}" alt="Card Image">
										</a>
										<div class="card-body margin-card-body">
											<h4 class="pemas"> ${judulService}</h4>
											<br>
											<a href="${linknya}" class="btn btnemas ml-auto mr-auto">Read More</a>
										</div>
									</div>
								</div>`;
				});
				$("#listPelayanan").after(html);
				$('#listPelayanan').nextAll().show('3000');
				lazyImageDefault();
			}
		});
	}
</script>
