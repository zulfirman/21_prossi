<?php include APPPATH . 'views/front/_partials/header.php'; ?>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/main_styles.min.css">
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/responsive.min.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>
<style>
	.bg-banner {
		height: auto;
		position: relative;
		display: block;
		filter: opacity(40%);
	}
	.icon_container {
		width: 90px;
		height: 90px;
	}
	.icon_container .icon{
		width: 60px;
		height: 60px;
	}

	.page-banner .bg-banner {
		width: 100%;
		height: auto;
		position: relative;
		display: block;
	}
	.video-container {
		display: grid;
		justify-items: center;
		align-items: center;
		position: relative;
		width: 100%;
		height: 100%;
		overflow: hidden;
	}
	.video-container video {
		position: absolute;
		z-index: 1;
		top: 50%;
		left:50%;
		min-width: 100%;
		min-height: 100%;
		transform: translate(-50%, -50%);
	}
	.col-xs-5-cols,
	.col-sm-5-cols,
	.col-md-5-cols,
	.col-lg-5-cols {
		position: relative;
		width: 100%;
		min-height: 1px;
		padding-right: 15px;
		padding-left: 15px;
	}

	.col-xs-5-cols {
		-ms-flex: 0 0 20%;
		flex: 0 0 20%;
		max-width: 20%;
	}
	@media (min-width: 768px) {
		.col-sm-5-cols {
			-ms-flex: 0 0 20%;
			flex: 0 0 20%;
			max-width: 20%;
		}
	}
	@media (min-width: 992px) {
		.col-md-5-cols {
			-ms-flex: 0 0 20%;
			flex: 0 0 20%;
			max-width: 20%;
		}
	}
	@media (min-width: 1200px) {
		.col-lg-5-cols {
			-ms-flex: 0 0 20%;
			flex: 0 0 20%;
			max-width: 20%;
		}
	}
	.services_card{
		box-shadow: 0 1rem 3rem #fae3db!important;
		border-radius: 15px;
	}
</style>
<div class="home" style="margin-top: 10px; background: white">
	<!-- Home Slider -->
	<div class="home_slider_container">
		<div class="owl-carousel owl-theme home_slider">
			<!-- Slide -->
			<div class="owl-item">
				<div style="position: fixed; z-index: -99; width: 100%; height: 100%">
					<?php
					$this->db->where('KodeGambar', 1);
					$valPage = $this->db->get('t_gambar', 1)->first_row();
					if ($GLOBALS['env']['playYoutube']) {
						?>
						<div class="video-container">
							<video autoplay muted loop style="filter: opacity(40%);">
								<source src="<?=$valPage->Lainnya1?>" type="video/mp4">
							</video>
						</div>
					<?php } ?>
				</div>
				<!--<div hidden class="background_image" style="background-image:url(<? /*=$si*/ ?>assets/gadxcd/images/home_slider.jpg)"></div>-->
				<div class="home_container">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="home_content">
									<?= $valPage->ContentId ?>
									<div class="home_buttons d-flex flex-row align-items-center justify-content-start">
										<div class="button button_1 trans_200" hidden><a href="#">read more</a></div>
										<div class="button button_2 trans_200" hidden><a href="#">make an appointment</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<!-- Home Slider Dots -->

		<div class="home_slider_dots">
			<ul id="home_slider_custom_dots"
				class="home_slider_custom_dots d-flex flex-row align-items-center justify-content-start">
				<li class="home_slider_custom_dot trans_200 active"></li>
			</ul>
		</div>

	</div>
</div>

<?php include APPPATH . 'views/front/_part/pelayanan.php'; ?>
<!-- Services -->

<div class="services" style="background: white!important;">
	<div class="container">
		<!--<div class="row">
			<div class="col text-center">
				<div class="section_title_container">
					<div class="section_title"><h3>Mengapa Pilih <?/*=$pe->NamaWebsite*/?></h3></div>
				</div>
			</div>
		</div>-->
		<div class="row services_row" style="margin-left: -7%;margin-right: -7%;">
			<?php
			$this->db->select('ContentId,MainImage,Lainnya1,"" as col');
			$this->db->where('Tipe', 5);
			$this->db->order_by('cast(Lainnya3 as unsigned)', 'asc');
			$listWhy = $this->db->get('t_gambar')->result();
			$colCountWhy=0;
			$colWhy='col-lg-12';
			$divide4=count($listWhy)/4;
			$checked=0;
			foreach ($listWhy as $key => $val) {
				$colCountWhy++;
				if($colCountWhy===2){
					$colWhy='col-lg-6';
					$listWhy[$key-1]->col=$colWhy;
					$listWhy[$key]->col=$colWhy;
				}
				if($colCountWhy===3){
					$colWhy='col-lg-4';
					$listWhy[$key-1]->col=$colWhy;
					$listWhy[$key-2]->col=$colWhy;
					$listWhy[$key]->col=$colWhy;
				}
				if($colCountWhy===4&&$key>4){
					$colWhy='col-lg-3';
					$listWhy[$key-1]->col=$colWhy;
					$listWhy[$key-2]->col=$colWhy;
					$listWhy[$key-3]->col=$colWhy;
					$listWhy[$key]->col=$colWhy;
				}
				if($colCountWhy===5){
					$colWhy='col-lg-5-cols';
					$listWhy[$key-1]->col=$colWhy;
					$listWhy[$key-2]->col=$colWhy;
					$listWhy[$key-3]->col=$colWhy;
					$listWhy[$key-4]->col=$colWhy;
					$listWhy[$key]->col=$colWhy;
					$colCountWhy=0;
				}
			}
			foreach ($listWhy as $key => $val){?>
			<!-- Service -->
				<div class="gallery_product <?=$val->col?> mb-4 filter">
					<div class="card border-0 transform-on-hover h-100 services_card">
						<div class="card-body margin-card-body row">
							<div class="col-md-12">
								<h4 class="pemas text-center"><?=$val->ContentId?></h4>
							</div>
							<div class="col-md-12">
								<div class="icon_container d-flex flex-column align-items-center justify-content-center ml-auto mr-auto bpinkmuda">
									<div class="icon"><img src="<?=$linkUpload.$val->MainImage?>" alt="https://www.flaticon.com/authors/prosymbols"></div>
								</div>
							</div>
							<br>
							<div class="col-md-12">
								<div class="service_text">
									<p class="text-center"><?=$val->Lainnya1?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>

			<div hidden>
			<!--Service -->
			<div class="col-xl-4 col-md-6 service_col">
				<div class="service text-center">
					<div class="service">
						<div class="icon_container d-flex flex-column align-items-center justify-content-center ml-auto mr-auto">
							<div class="icon"><img src="<?=$si?>assets/gadxcd/images/icon_5.svg" alt="https://www.flaticon.com/authors/prosymbols"></div>
						</div>
						<div class="service_title">Breast Augmentation</div>
						<div class="service_text">
							<p>Odio ultrices ut. Etiam ac erat ut enim maximus accumsan vel ac nisl. Duis feugiat bibendum orci, non elementum urna.</p>
						</div>
					</div>
				</div>
			</div>

			<!-- Service -->
			<div class="col-xl-4 col-md-6 service_col">
				<div class="service text-center">
					<div class="service">
						<div class="icon_container d-flex flex-column align-items-center justify-content-center ml-auto mr-auto">
							<div class="icon"><img src="<?=$si?>assets/gadxcd/images/icon_6.svg" alt="https://www.flaticon.com/authors/prosymbols"></div>
						</div>
						<div class="service_title">Breast Augmentation</div>
						<div class="service_text">
							<p>Odio ultrices ut. Etiam ac erat ut enim maximus accumsan vel ac nisl. Duis feugiat bibendum orci, non elementum urna.</p>
						</div>
					</div>
				</div>
			</div>

			<!-- Service -->
			<div class="col-xl-4 col-md-6 service_col">
				<div class="service text-center">
					<div class="service">
						<div class="icon_container d-flex flex-column align-items-center justify-content-center ml-auto mr-auto">
							<div class="icon"><img src="<?=$si?>assets/gadxcd/images/icon_7.svg" alt="https://www.flaticon.com/authors/prosymbols"></div>
						</div>
						<div class="service_title">Breast Augmentation</div>
						<div class="service_text">
							<p>Odio ultrices ut. Etiam ac erat ut enim maximus accumsan vel ac nisl. Duis feugiat bibendum orci, non elementum urna.</p>
						</div>
					</div>
				</div>
			</div>

			<!-- Service -->
			<div class="col-xl-4 col-md-6 service_col">
				<div class="service text-center">
					<div class="service">
						<div class="icon_container d-flex flex-column align-items-center justify-content-center ml-auto mr-auto">
							<div class="icon"><img src="<?=$si?>assets/gadxcd/images/icon_8.svg" alt="https://www.flaticon.com/authors/prosymbols"></div>
						</div>
						<div class="service_title">Breast Augmentation</div>
						<div class="service_text">
							<p>Odio ultrices ut. Etiam ac erat ut enim maximus accumsan vel ac nisl. Duis feugiat bibendum orci, non elementum urna.</p>
						</div>
					</div>
				</div>
			</div>

			<!-- Service -->
			<div class="col-xl-4 col-md-6 service_col">
				<div class="service text-center">
					<div class="service">
						<div class="icon_container d-flex flex-column align-items-center justify-content-center ml-auto mr-auto">
							<div class="icon"><img src="<?=$si?>assets/gadxcd/images/icon_3.svg" alt="https://www.flaticon.com/authors/prosymbols"></div>
						</div>
						<div class="service_title">Breast Augmentation</div>
						<div class="service_text">
							<p>Odio ultrices ut. Etiam ac erat ut enim maximus accumsan vel ac nisl. Duis feugiat bibendum orci, non elementum urna.</p>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>


<?php include APPPATH . 'views/front/_part/tentang-part.php'; ?>

<!-- Why Choose Us -->

<div class="why" hidden>
	<!-- <div class="background_image" style="background-image:url(images/why.jpg)"></div> -->
	<div class="container">
		<div class="row row-eq-height">

			<!-- Why Choose Us Image -->
			<div class="col-lg-6 order-lg-1 order-2">
				<div class="why_image_container">
					<div class="why_image"><img src="<?=$si?>assets/gadxcd/images/why_1.jpg" alt=""></div>
				</div>
			</div>

			<!-- Why Choose Us Content -->
			<div class="col-lg-6 order-lg-2 order-1">
				<div class="why_content">
					<div class="section_title_container">
						<div class="section_subtitle">This is Dr Pro</div>
						<div class="section_title"><h2>Why choose us?</h2></div>
					</div>
					<div class="why_text">
						<p>Odio ultrices ut. Etiam ac erat ut enim maximus accumsan vel ac nisl. Duis feugiat bibendum
							orci, non elementum urna vestibulum in. Nulla facilisi. Nulla egestas vel lacus sed
							interdum. Sed mollis, orci elementum eleifend tempor, nunc libero porttitor tellus, vel
							pharetra metus dolor.</p>
					</div>
					<div class="why_list">
						<ul>

							<!-- Why List Item -->
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div class="icon_container d-flex flex-column align-items-center justify-content-center">
									<div class="icon"><img src="<?=$si?>assets/gadxcd/images/icon_1.svg"
														   alt="https://www.flaticon.com/authors/prosymbols"></div>
								</div>
								<div class="why_list_content">
									<div class="why_list_title">Only Top Products</div>
									<div class="why_list_text">Etiam ac erat ut enim maximus accumsan vel ac nisl</div>
								</div>
							</li>

							<!-- Why List Item -->
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div class="icon_container d-flex flex-column align-items-center justify-content-center">
									<div class="icon"><img src="<?=$si?>assets/gadxcd/images/icon_2.svg"
														   alt="https://www.flaticon.com/authors/prosymbols"></div>
								</div>
								<div class="why_list_content">
									<div class="why_list_title">The best Doctors</div>
									<div class="why_list_text">Ac erat ut enim maximus accumsan vel ac</div>
								</div>
							</li>

							<!-- Why List Item -->
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div class="icon_container d-flex flex-column align-items-center justify-content-center">
									<div class="icon"><img src="<?=$si?>assets/gadxcd/images/icon_3.svg"
														   alt="https://www.flaticon.com/authors/prosymbols"></div>
								</div>
								<div class="why_list_content">
									<div class="why_list_title">Great Feedback</div>
									<div class="why_list_text">Etiam ac erat ut enim maximus accumsan vel</div>
								</div>
							</li>

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Call to action -->

<?php include APPPATH . 'views/front/_part/janji.php'; ?>

<!-- Extra -->

<div class="extra">
	<div class="parallax_background parallax-window bpinkmuda" data-parallax="scroll"
		 data-image-src="" data-speed="0.8"></div>
	<div class="container">
		<div class="row">
			<div class="owl-carousel owl-theme promo_slider">
				<?php
				$this->db->where('Tipe', 2);
				$promoMain = $this->db->get('t_artikel', 6)->result();
				foreach ($promoMain as $val){
					?>
					<div class="col-md-12 owl-item">
						<div hidden class="extra_container d-flex flex-row align-items-start justify-content-end">
							<div class="extra_content bemas">
								<a style="cursor: pointer" onclick="mdynamic('<?=$val->KodeArtikel?>','<?=$val->JudulId?>','<?=$linkUpload.$val->MainImage?>')">
									<img src="<?=$linkUpload.'loading.jpg'?>" class="lazy" data-src="<?=$linkUpload.$val->MainImage?>" alt="Card Image">
								</a>
							</div>
							<div hidden id="content<?=$val->KodeArtikel?>">
								<?=$val->ContentId?>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<!-- Testimonials -->

<?php include APPPATH . 'views/front/_part/testimoni.php'; ?>

<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
		initTestSlider();
		/*setTimeout(function () {
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/iframe_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		}, 1000);*/
	});


	// 3. This function creates an <iframe> (and YouTube player)
	//    after the API code downloads.
	var player;
	function onYouTubeIframeAPIReady() {
		player = new YT.Player('player', {
			height: '100%',
			width: '100%',
			videoId: '<?=$valPage->Lainnya1?>',
			playerVars: {
				vq: 'medium',
				mute: 1,
				autoplay: 1,        // Auto-play the video on load
				controls: 0,        // Show pause/play buttons in player
				showinfo: 1,        // Hide the video title
				modestbranding: 1,  // Hide the Youtube Logo
				playlist: '<?=$valPage->Lainnya1?>',
				loop: 1,            // Run the video in a loop
				fs: 0,              // Hide the full screen button
				cc_load_policy: 0, // Hide closed captions
				iv_load_policy: 3,  // Hide the Video Annotations
				autohide: 1,         // Hide video controls when playing
				playsinline: 1, //forbid fullscreen on ios
			},
			events: {
				'onReady': onPlayerReady,
				/*'onStateChange': onPlayerStateChange*/
			}
		});
		/*144p: &vq=tiny
		240p: &vq=small
		360p: &vq=medium
		480p: &vq=large
		720p: &vq=hd720*/
	}

	// 4. The API will call this function when the video player is ready.
	function onPlayerReady(event) {
		event.target.playVideo();
	}
	// 5. The API calls this function when the player's state changes.
	//    The function indicates that when playing a video (state=1),
	//    the player should play for six seconds and then stop.
	var done = false;
	function onPlayerStateChange(event) {
		if (event.data === YT.PlayerState.PLAYING && !done) {
			setTimeout(stopVideo, 6000);
			done = true;
		}
	}
	function stopVideo() {
		player.stopVideo();
	}
</script>
