<style>
	.test_image {
		width: 100px;
		height: 100px;
		border-radius: 50%;
		overflow: hidden;
	}
</style>
<div class="testimonials">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<div class="section_title"><h2>Testimoni Pasien <?=$pe->NamaWebsite?></h2></div>
				</div>
			</div>
		</div>
		<div class="row testimonials_row bpinkmuda">
			<div class="col">
				<!--<div class="quote d-flex flex-column align-items-center justify-content-center ml-auto mr-auto"><img src="<?/*=$si*/?>assets/gadxcd/images/quote.png" alt=""></div>-->

				<!-- Testimonials Slider -->
				<div class="test_slider_container">
					<div class="owl-carousel owl-theme test_slider">
						<!-- Slide -->
						<?php
						$this->db->where('Tipe', 4);
						$this->db->order_by('Lainnya1');
						$testimoni = $this->db->get('t_gambar')->result();
						foreach ($testimoni as $val){
						?>
						<div class="owl-item">
							<div class="test_item text-center" style="background-image:url(<?=$si?>assets/gadxcd/image-default/double-quote.png);background-repeat:no-repeat;background-position: 74% 1%;">
								<div class="test_text">
									<h3 style="font-weight: 500;">Testimoni Klien
									</h3><br>
									<center>
										<div class="test_image" style="border: 4px solid #CDAC65"><img src="<?=$linkUpload.'loading.jpg'?>" class="lazy" data-src="<?=$linkUpload.$val->MainImage?>" alt=""></div><br>
									</center>
									<p style="color: black"><?=$val->Lainnya3?></p>
								</div>
								<div class="test_info d-flex flex-row align-items-center justify-content-center">
									<div class="test_text">-<?=$val->Lainnya1?>-</div>
									<!--<div class="test_text"><?/*=$val->Lainnya1*/?>, <span><?/*=$val->Lainnya2*/?></span></div>-->
								</div>
							</div>
						</div>
						<?php } ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
