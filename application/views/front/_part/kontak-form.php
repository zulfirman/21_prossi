<style>
	.contact_input {
		width: 100%;
	}
	.contact_input::-webkit-input-placeholder, .textareaclass::-webkit-input-placeholder
	{
		font-size: 14px !important;
		font-weight: bolder!important;
		color: black !important;
	}
</style>
<div class="contact_form_container">
	<div class="contact_form_title">Hubungi Kami</div>
	<form action="<?=$si.'user_authentication/sendEmail'?>" method="post" class="contact_form" id="contact_form">
		<?=inCsrf()?>
		<?php
		if(isset($_SESSION['fnotif'])){notifSukses($_SESSION['fnotif'],'success');}
		?>
		<div class="d-flex flex-row align-items-start justify-content-between flex-wrap row">
			<div class="row">
				<div class="col-md-4 mb-4">
					<input type="text" class="contact_input" name="name" placeholder="Name" required="required" style="background: #f5bbad">
				</div>
				<div class="col-md-4 mb-4">
					<input type="email" class="contact_input" name="email" placeholder="Email" required="required" style="background: #fce7e2">
				</div>
				<div class="col-md-4 mb-4">
					<input type="text" class="contact_input" name="subject" placeholder="Subject Email" required="required" style="background: #e3ce99">
				</div>
				<div class="col-md-12 mb-4">
					<textarea class="form-control textareaclass" name="message" id="message" cols="30" rows="10" placeholder="Message" required=""></textarea>
				</div>
			</div>
			<!--<select class="contact_select contact_input" required>
				<option disabled="" selected="">Speciality</option>
				<option>Speciality 1</option>
				<option>Speciality 2</option>
				<option>Speciality 3</option>
				<option>Speciality 4</option>
				<option>Speciality 5</option>
			</select>
			<select class="contact_select contact_input"required="required">
				<option disabled="" selected="">Doctor</option>
				<option>Doctor 1</option>
				<option>Doctor 2</option>
				<option>Doctor 3</option>
				<option>Doctor 4</option>
				<option>Doctor 5</option>
			</select>-->
			<!--<input type="text" id="datepicker" class="contact_input datepicker" placeholder="Date" required="required">-->
		</div>
		<br>
		<button class="button w-100 trans_200 btngold">Submit</button>
	</form>
</div>
