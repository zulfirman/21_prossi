<style>
	/*background color*/
	body{
		background-image:url(<?=$si?>assets/gadxcd/image-default/bg@1X.jpeg);
		background-size:     cover;                      /* <------ */
		background-repeat:   no-repeat;
		background-position: center center;
		font-family: 'Poppins', sans-serif;
	}
	.why, .blog, .services, .testimonials, .team, .contact{
		/*background: rgb(254,238,232)!important;
		background: linear-gradient(0deg, rgba(254,238,232,1) 6%, rgba(254,207,190,1) 96%)!important;*/
		/*background: rgb(254,238,232)!important;
		background: radial-gradient(circle, rgba(254,238,232,1) 6%, rgba(245,209,196,1) 44%, rgba(255,226,216,1) 100%)!important;
		padding-top: 20px;*/
		background: transparent!important;
		padding-top: 20px;
	}
	/*end background color*/
	.select2-container--default .select2-selection--single .select2-selection__arrow b {
		background-image: url(<?=$si?>assets/gadxcd/images/icon-search.png);
		background-color: transparent;
		background-size: contain;
		border: none !important;
		height: 20px !important;
		width: 20px !important;
		margin: auto !important;
		top: auto !important;
		left: auto !important;
	}
</style>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/css.min.css?v=0.1">
