<?php
if($lang==="US"){
	$recentPost="Recent Post";
	$sideKategori="Category";
}
else{
	$recentPost="Post Terbaru";
	$sideKategori="Kategori";
}
?>
<div class="single-widget-area">
	<!-- Title -->
	<div class="widget-title">
		<h4><?=$recentPost?></h4>
	</div>

	<!-- Single Latest Posts -->
	<?php
	$this->db->select('a.UrlArtikel, a.JudulId, a.JudulEn, a.MainImage, a.DateCreated');
	$this->db->join('m_user b', 'a.CreatedBy = b.KodeUser');
	$this->db->where('a.Tipe', $TipeArtikel);
	$this->db->order_by('a.DateCreated', 'desc');
	$qryLatest=$this->db->get('t_artikel a', 4)->result();
	foreach ($qryLatest as $qryLatestv) {
		?>
		<div class="single-latest-post d-flex align-items-center">
			<div class="post-thumb">
				<img src="<?=$linkUpload.$qryLatestv->MainImage?>" alt="">
			</div>
			<div class="post-content">
				<a href="<?=$si."artikel/$qryLatestv->UrlArtikel"?>" class="post-title" >
					<h6><?=$lang === "Id" ? $qryLatestv->JudulId : $qryLatestv->JudulEn?></h6>
				</a>
				<a href="#" class="post-date"><?=$qryLatestv->DateCreated?></a>
			</div>
		</div>
	<?php } ?>
</div>

<!-- ##### Single Widget Area ##### -->
<div class="single-widget-area">
	<!-- Title -->
	<div class="widget-title">
		<h4><?=$sideKategori?></h4>
	</div>
	<!-- Tags -->
	<ol class="popular-tags d-flex flex-wrap">
		<?php
		$this->db->select('b.NamaKategoriId, b.NamaKategoriEn');
		$this->db->join('m_kategori b', 'a.KodeKategori = b.KodeKategori');
		$this->db->where('a.Tipe', $TipeArtikel);
		$this->db->group_by('b.NamaKategoriId');
		$qryKategori=$this->db->get('t_artikel a')->result();
		foreach ($qryKategori as $val) {
			?>
			<li><a href="<?=$si.'search?category='?><?=$lang === "Id" ? $val->NamaKategoriId : $val->NamaKategoriEn?>"><?=$lang === "Id" ? $val->NamaKategoriId : $val->NamaKategoriEn?></a></li>
		<?php } ?>
	</ol>
</div>
