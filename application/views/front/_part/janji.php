<div class="cta" style="background: #f3b9a2">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="cta_container d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
					<div class="cta_content">
						<div class="cta_title">Reservasi Sekarang</div>
						<div class="cta_text" hidden>Etiam ac erat ut enim maximus accumsan vel ac nisl</div>
					</div>
					<a href="#" class="cta_phone ml-lg-auto" data-toggle="modal" data-target="#modalKontak" style="background: white">
						<div style="color: #f3b9a2">Chat Via Whatsapp</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
