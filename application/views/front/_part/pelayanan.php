
<div class="why mb-5">
	<!-- <div class="background_image" style="background-image:url(images/why.jpg)"></div> -->
	<div class="container">
		<div class="row row-eq-height">
			<!-- Why Choose Us Content -->
			<div class="col-lg-12 order-lg-2 order-1">
				<div class="row services_row">
					<div class="gallery-block cards-gallery" style="padding-bottom: 10px; padding-top: 10px">
						<div class="row"  style="margin-left: -9%; margin-right: -9%">
							<?php
							$backgroundService='';
							foreach ($qryService as $val) {
								if($backgroundService===''||$backgroundService==='bemas'){
									$backgroundService='bpinktua';
								}else{
									$backgroundService='bemas';
								}
								?>
								<div class="col-md-6 mb-4 ">
									<a href="<?=$si."pelayanan/$val->NamaKategori"?>">
										<center class="h-100">
											<div class="card border-0 transform-on-hover h-100 card-service <?=$backgroundService?>">
												<div class="row card-body">
													<div class="col-sm-6">
														<div class="service_title"><?=$val->NamaKategoriId?></div>
														<div class="service_text">
															<p><?=$val->DeskripsiId?></p>
														</div>
													</div>
													<div class="col-sm-6">
														<img src="<?=$linkUpload.'loading.jpg'?>" class="lazy" data-src="<?=$linkUpload.$val->MainImage?>" alt="<?=$val->NamaKategoriId?>" style="width: 100%">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 text-center">
														<a href="<?=$si."pelayanan/$val->NamaKategori"?>" class="btn btnpinkwhite ml-auto mr-auto">See More</a>
													</div>
													<div class="col-md-6">
													</div>
												</div>
											</div>
										</center>
									</a>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<!--<div class="why_content">
					<div class="col text-center">
						<div class="section_title_container">
							<div class="section_title"><h3>Layanan Kami</h3></div>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div>
</div>
