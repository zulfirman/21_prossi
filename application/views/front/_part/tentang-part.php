<?php
$this->db->where('KodeGambar', 2);
$valTentang = $this->db->get('t_gambar', 1)->first_row();
?>
<!-- Intro -->
<style>
	.about_slider {
		-moz-transform: rotate(1.5deg);
		-webkit-transform: rotate(1.5deg);
		-o-transform: rotate(1.5deg);
		-ms-transform: rotate(1.5deg);
		transform: rotate(1.5deg);
	}
	.border_about_img{
		border: 20px solid white;
		background: white;
	}
</style>
<div class="intro why">
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<div class="intro_content" style="text-align: center">
					<!--<img src="<?/*=$linkUpload.$valTentang->MainImage*/?>" class="img-90">-->
				</div>
			</div>
			<!-- Intro Content -->
			<div class="col-md-12">
				<div class="intro_content">

					<!-- Milestones -->
					<div class="milestones" hidden>
						<div class="row milestones_row">

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="5000" data-sign-before="+">0</div>
									<div class="milestone_text">Satisfied Patients</div>
								</div>
							</div>

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="352">0</div>
									<div class="milestone_text">Face Liftings</div>
								</div>
							</div>

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="718">0</div>
									<div class="milestone_text">Injectibles</div>
								</div>
							</div>

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="5">0</div>
									<div class="milestone_text">Awards Won</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>

			<div class="col-md-6 bpinkmuda border_content">
				<?=$valTentang->ContentId?>
			</div>
			<div class="col-md-6 bemas">
				<div class="test_slider_container">
					<div class="owl-carousel owl-theme about_slider">
						<!-- Slide -->
						<?php
						foreach (json_decode($valTentang->Lainnya1) as $val){
						?>
						<div class="owl-item" style="border: 14px solid #F8E0D7">
							<div class="test_item text-center">
								<div class="test_text border_about_img">
									<img src="<?=$linkUpload.'loading.jpg'?>" class="lazy" data-src="<?=$val->CabangImage?>" alt="">
									<br>
									<h4><?=$val->NamaCabang?></h4>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
				<br>
			</div>

		</div>
	</div>
</div>
