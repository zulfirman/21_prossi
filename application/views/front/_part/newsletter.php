<!-- Newsletter -->

<div class="newsletter" hidden>
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$si?>assets/gadxcd/images/newsletter.jpg" data-speed="0.8"></div>
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<div class="newsletter_title">Subscribe to our newsletter</div>
			</div>
		</div>
		<div class="row newsletter_row">
			<div class="col-lg-8 offset-lg-2">
				<div class="newsletter_form_container">
					<form action="<?=$si?>user-authentication/subscribe" id="newsleter_form" class="newsletter_form">
						<input type="email" class="newsletter_input" placeholder="Your E-mail" required="required">
						<button class="newsletter_button">subscribe</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
