<?php include APPPATH . 'views/front/_partials/header.php'; ?>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about.css">
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about_responsive.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>
<style>
	.team_text {
		text-align: justify;
		word-wrap: break-word;
	}
	.intro {
		padding-top: 0;
	}
	.team {
		padding-bottom: 0;
	}
	@media only screen and (max-width: 765px) {
		.team_image img {
			width: 75%;
		}
	}
	.modalimage{
		margin-left: auto;
		margin-right: auto;
		width: 80%;
	}
	.centerContent{
		display: flex;align-items: center;
	}
	.rowPromo {
		display: flex;
	}
	.promo-image{
		height: 300px;width: 300px;  margin-left: auto; margin-right: auto;display: block;
		background: white;
	}
	.padding-promo{
		padding-top: 15px;
		padding-left: 15px;
		padding-right: 15px;
		padding-bottom: 15px;
	}
	.oftext{
		margin: 0;
		padding: 12px;
		text-align: center;
	}
	.ofimg{
		margin: 0;
		padding: 0;
	}

	.dparent {
		border: 1px solid black;
		display: flex;
		position: relative;
	}

	.dchild {
		border: 1px solid black;
		width: 80%;
		height: 80%;
		margin: auto;
	}
</style>
<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$isiArtikel->MainImage?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title"><?=ucwords($this->uri->segment(1))?></div>
						<div class="home_text" hidden>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Team -->
<style>
	.align-items-center {
		-ms-flex-align: center!important;
		align-items: center!important;
	}
	.d-flex {
		display: -ms-flexbox!important;
		display: flex!important;
	}
</style>
<div class="team">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<div class="section_subtitle" hidden>This is Dr Pro</div>
					<div class="section_title"><h2><?=$isiArtikel->JudulId?></h2></div>
				</div>
			</div>
		</div>
		<div class="row team_row">
			<div class="container">
				<div class="col-md-12 text-center"  id="pelayananD" hidden>
					<table style="width: 100%">
						<tr>
							<td style="height: 400px;width: 400px"><img style="border: 15px solid white" alt="" class="img-fluid" src="<?=$linkUpload.$isiArtikel->Lainnya1?>"/></td>
							<td class="bemas extra_content"><?=$isiArtikel->Lainnya2?></td>
						</tr>
						<tr>
							<td class="bpinkmuda extra_content"><?=$isiArtikel->Lainnya4?></td>
							<td style="height: 400px;width: 400px;"><img style="border: 15px solid white" alt="" class="img-fluid" src="<?=$linkUpload.$isiArtikel->Lainnya3?>"/></td>
						</tr>
					</table>
				</div>

				<div class="col-md-6 mb-4">
					<img style="border: 15px solid white" alt="" class="img-fluid" src="<?=$linkUpload.$isiArtikel->Lainnya1?>"/>
				</div>
				<div class="col-md-6 mb-4">
					<?=$isiArtikel->Lainnya2?>
				</div>
				<div class="col-md-6 mb-4">
					<img style="border: 15px solid white" alt="" class="img-fluid" src="<?=$linkUpload.$isiArtikel->Lainnya3?>"/>
				</div>
				<div class="col-md-6 mb-4">

				</div>


				<!--<section class="section mt-5">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-lg-5 ml-auto d-flex align-items-center mt-4 mt-md-0 bemas">
								<div>
									<img alt="Web Studio" class="img-fluid" src="<?/*=$linkUpload.$isiArtikel->Lainnya1*/?>" />
								</div>
							</div>
							<div class="col-md-6 col-lg-5 ml-auto d-flex align-items-center mt-4 mt-md-0 bemas">
								<div>
									<p class="margin-top-s"><?/*=$isiArtikel->Lainnya2*/?></p>
								</div>
							</div>
							<div class="col-md-6 col-lg-5 ml-auto d-flex align-items-center mt-4 mt-md-0 bemas">
								<div>
									<p class="margin-top-s"><?/*=$isiArtikel->Lainnya2*/?></p>
								</div>
							</div>
							<div class="col-md-6 col-lg-5 ml-auto d-flex align-items-center mt-4 mt-md-0 bemas">
								<div>
									<img alt="Web Studio" class="img-fluid" src="<?/*=$linkUpload.$isiArtikel->Lainnya1*/?>" />
								</div>
							</div>
						</div>
					</div>
				</section>-->
				<!--<div class="row rowPromo dparent">
					<div class="col-md-6 mb-4 ofimg dchild">
						<a href="portfolio-item.html">
							<img class="img-responsive img-hover promo-image padding-promo" src="<?/*=$linkUpload.$isiArtikel->Lainnya1*/?>" alt="">
						</a>
					</div>
					<div class="col-md-6 mb-4 oftext bemas dchild">
						<?/*=$isiArtikel->Lainnya2*/?><
					</div>
				</div>-->
				<!--<div class="row rowPromo">
					<div class="col-md-6 mb-4 oftext bpinkmuda" style="min-height: 400px">
						<?/*=$isiArtikel->Lainnya4*/?>
					</div>
					<div class="col-md-6 mb-4 ofimg padding-promo">
						<a href="portfolio-item.html">
							<img class="img-responsive img-hover promo-image padding-promo" src="<?/*=$linkUpload.$isiArtikel->Lainnya3*/?>" alt="">
						</a>
					</div>
				</div>-->
		</div>
	</div>
		<br><br>
	</div>
</div>

<!-- Testimonials -->

<?php include APPPATH . 'views/front/_part/testimoni.php'; ?>

<!-- Call to action -->

<?php include APPPATH . 'views/front/_part/janji.php'; ?>

<div class="modal fade bs-example-modal-lg show " id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd">
	<div class="modal-dialog modal-lg box-shadow" role="document">
		<div class="modal-content bpinkmuda">
			<div class="modal-header">
				<h3 class="modal-title"></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body bpinkmuda">
				<div class="row">
					<div class="col-md-6 mb-4">
						<div class="card border-0 transform-on-hover h-100" style="background-color: transparent">
							<img id="gambarModal" src="" alt="Card Image" class="card-img-top modalimage">
							<div class="card-body">
								<h5 id="nama">Doaa</h5>
								<h6 class="pemas" id="jabatan">asas</h6>
							</div>
						</div>
					</div>
					<div class="col-md-6" id="jadwal">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>


<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
		initTestSlider();
	});
	function tampilModal(KodeGambar,gambar,nama,jabatan,jadwal){
		$('#modalAdd').modal('show');
		$('#nama').html(nama);
		$('#gambarModal').attr('src', gambar);
		$('#jabatan').html(jabatan);
		$('#jadwal').html('<br><br>'+$('#jadwal'+KodeGambar).html());
	}
</script>
