<?php include APPPATH . 'views/front/_partials/header.php'; ?>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about.min.css">
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about_responsive.min.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>
<style>
	.service_title {
		font-size: 20px;
		font-weight: 600;
		color: #404040;
		line-height: 1.2;
		margin-top: 17px;
	}
	.why{
		padding-top: 70px;
		padding-bottom: 81px;
	}
	.margin-card-body{
		margin-bottom: -15%;
	}
	/*gallery filter */

	.gallery-title:after {
		content: "";
		position: absolute;
		width: 7.5%;
		left: 46.5%;
		height: 45px;
		border-bottom: 1px solid #5e5e5e;
	}

	.port-image
	{
		width: 100%;
	}

	.gallery_product
	{
		margin-bottom: 30px;
	}

	/*end gallery filter */
</style>
<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$bHeader->bPelayanan?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<h2><?=$isiArtikel->JudulId?></h2>
						<h5 style="font-weight: bold">
							<br>
							<?=$isiArtikel->ContentId?>
						</h5>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="why">
	<!-- <div class="background_image" style="background-image:url(images/why.jpg)"></div> -->
	<div class="container">
		<div class="row row-eq-height">
			<!-- Why Choose Us Content -->
			<div class="col-lg-12 order-lg-2 order-1">
				<div class="why_content">
					<div class="col text-center">
						<div class="section_title_container">
							<div class="section_title"></div>
						</div>
					</div>
					<div class="row services_row" id="pelayananD" hidden>
						<div class="container">
							<div class="col-md-12 text-center">
								<table style="width: 100%">
									<tr>
										<td style="height: 400px;width: 400px"><img style="border: 15px solid white" alt="" src="<?=$linkUpload.'loading.jpg'?>" class="lazy img-fluid" data-src="<?=$linkUpload.$isiArtikel->Lainnya1?>"/></td>
										<td class="bemas extra_content"><?=$isiArtikel->Lainnya2?></td>
									</tr>
									<tr>
										<td class="bpinkmuda extra_content"><?=$isiArtikel->Lainnya4?></td>
										<td style="height: 400px;width: 400px;"><img style="border: 15px solid white" alt="" src="<?=$linkUpload.'loading.jpg'?>" class="lazy img-fluid" data-src="<?=$linkUpload.$isiArtikel->Lainnya3?>"/></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="row services_row" id="pelayananM" hidden>
						<div class="col-md-6 mb-4 text-center">
							<img style="border: 15px solid white" alt="" src="<?=$linkUpload.'loading.jpg'?>" class="lazy img-fluid" data-src="<?=$linkUpload.$isiArtikel->Lainnya1?>"/>
						</div>
						<div class="col-md-6 mb-4 bemas">
							<?=$isiArtikel->Lainnya2?>
						</div>
						<div class="col-md-6 mb-4 bpinkmuda">
							<?=$isiArtikel->Lainnya4?>
						</div>
						<div class="col-md-6 mb-4 text-center">
							<img style="border: 15px solid white" alt="" src="<?=$linkUpload.'loading.jpg'?>" class="lazy img-fluid" data-src="<?=$linkUpload.$isiArtikel->Lainnya3?>"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php /*include APPPATH . 'views/front/_part/pelayanan.php'; */?>

<!-- Testimonials -->

<?php include APPPATH . 'views/front/_part/testimoni.php'; ?>

<!-- Call to action -->

<?php include APPPATH . 'views/front/_part/janji.php'; ?>

<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
		initTestSlider();
		cekLebar();
	});
	window.onresize = function() {
		cekLebar();
	};
	function cekLebar(){
		if(window.innerWidth<961){
			$('#pelayananD').prop('hidden', 1);
			$('#pelayananM').prop('hidden', 0);
		}
		else{
			$('#pelayananD').prop('hidden', 0);
			$('#pelayananM').prop('hidden', 1);
		}
	}
</script>
