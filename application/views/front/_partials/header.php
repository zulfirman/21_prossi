<?php
$pe=getPengaturan();
//$NomorTelepon="+62 ".wordwrap(str_replace('62','', '') , 3 , '-' , true );
//$NomorWa=$pe->NomorTelepon;
$si=site_url();
$sa=site_url().'admin/';
$cu=current_url();
$linkUpload=$si.'assets/uploads/';
$bHeader=json_decode($pe->BackgroundHeader);
$tHeader=json_decode($pe->BackgroundFooter);
if(!isset($_SESSION['lang'])) {
	$_SESSION['lang'] = "Id";
}
$lang=$_SESSION['lang'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?=$pe->NamaWebsite?> | <?=$titlePage??''?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="<?=$pe->NamaWebsite?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#CDAC65">
	<meta name="msapplication-navbutton-color" content="#CDAC65">
	<meta name="apple-mobile-web-app-status-bar-style" content="#CDAC65">
	<!--<link rel="stylesheet" type="text/css" href="<?/*=$si*/?>assets/gadxcd/styles/bootstrap-4.1.2/bootstrap.min.css">-->
	<link rel="stylesheet" href="<?=$si?>assets/gadxcd/styles/bs-4.4.1/bootstrap.min.css">
	<script src="<?=$si?>assets/gadxcd/styles/bs-4.4.1/f6b6a59c5d-fontawesome-main.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/plugins/OwlCarousel2-2.2.1/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/plugins/OwlCarousel2-2.2.1/owl.theme.default.min.css">
	<!--<link rel="stylesheet" type="text/css" href="<?/*=$si*/?>assets/gadxcd/plugins/OwlCarousel2-2.2.1/animate.css">-->
	<link rel="stylesheet" href="<?=$si?>assets/gadxcd/styles/bs-4.4.1/animate-3.7.0.min.css">
	<link href="<?=$si?>assets/gadxcd/plugins/jquery-datepicker/jquery-ui.min.css" rel="stylesheet" type="text/css">
	<!--<link rel="stylesheet" type="text/css" href="<?/*=$si*/?>assets/gadxcd/plugins/bootnavbar/bootnavbar.css">-->
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/gallery.min.css">
	<link rel="stylesheet" media="screen, print" href="<?=$si?>assets/xdfxcd/css/formplugins/select2/select2.bundle.min.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
	<!-- Global site tag (gtag.js) - Google Ads: 10811839985 -->
	<!--<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10811839985"></script>
	<script> window.dataLayer = window.dataLayer || [];
		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'AW-10811839985'); </script>-->

	<!-- Global site tag (gtag.js) - Google Analytics Mine-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-DFE6FGRL6D"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'G-DFE6FGRL6D');
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
					new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TG5HK69');</script>
	<!-- End Google Tag Manager -->
	<style>
		.preloader{background-color:#f2f4f5;width:100%;height:100%;position:fixed;top:0;left:0;right:0;bottom:0;z-index:999999;overflow:hidden}.preloader .preloader-circle{width:80px;height:80px;position:relative;border-style:solid;border-width:2px;border-top-color:#f39c12;border-bottom-color:transparent;border-left-color:transparent;border-right-color:transparent;z-index:10;border-radius:50%;box-shadow:0 1px 5px 0 rgba(0,0,0,.15);background-color:#fff;-webkit-animation:zoom 2s infinite ease;animation:zoom 2s infinite ease}.preloader .preloader-img{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);z-index:200}.preloader .preloader-img img{max-width:45px}
	</style>
	<link rel="icon" type="image/png" href="<?=$linkUpload.$pe->Icon?>">
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TG5HK69" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="preloader d-flex align-items-center justify-content-center">
	<div class="preloader-circle"></div>
	<div class="preloader-img">
		<img src="<?=$linkUpload.$pe->Icon?>" alt="">
	</div>
</div>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_400 scrolled">
		<div class="header_content d-flex flex-row align-items-center jusity-content-start trans_400">
			<!-- Logo -->
			<div class="logo">
				<a href="<?=$si?>">
					<div hidden><span style="margin-left: 0"><?=$pe->NamaWebsite?></span></div>
					<div class="col-md-6"><img class="mylogo" src="<?=$linkUpload.$pe->Logo?>" alt="<?=$pe->NamaWebsite?>" aria-roledescription="logo" id="logo" style="margin-bottom: 5px"></div>
				</a>
			</div>

			<!-- Main Navigation -->
			<nav class="main_nav">
				<?php
				$navMobile=0;
				include APPPATH . 'views/front/_partials/nav.php';
				?>
			</nav>
			<div class="header_extra d-flex flex-row align-items-center justify-content-end ml-auto">

				<!-- Work Hourse -->
				<div class="work_hours" hidden>Mo - Sat: 8:00am - 9:00pm</div>

				<!-- Header Phone -->
				<a href="#" data-toggle="modal" data-target="#modalKontak">
				<div class="header_phone">Kontak Kami</div>
				</a>

				<!-- Appointment Button -->
				<div class="button button_1 header_button" hidden><a  href="#">Make an Appointment</a></div>

				<!-- Header Social -->
				<div class="social header_social">
					<ul class="d-flex flex-row align-items-center justify-content-start">
						<?php include APPPATH . 'views/front/_part/social-media.php'; ?>
					</ul>
				</div>

				<!-- Hamburger -->
				<div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
			</div>
		</div>
	</header>

	<!-- Menu -->

	<div class="menu_overlay trans_400"></div>
	<div class="menu trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<?php
		$navMobile=1;
		include APPPATH . 'views/front/_partials/nav.php';
		?>
		<div class="menu_extra" hidden>
			<div class="menu_link">Mo - Sat: 8:00am - 9:00pm</div>
			<div class="menu_link">+34 586 778 8892</div>
			<div class="menu_link"><a href="#">Make an appointment</a></div>
		</div>
		<div class="social menu_social">
			<ul class="d-flex flex-row align-items-center justify-content-start">
				<?php include APPPATH . 'views/front/_part/social-media.php'; ?>
			</ul>
		</div>
	</div>

	<!-- Home -->
