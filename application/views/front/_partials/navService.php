<?php
$listNavigasi=listNavigasi(1);
if(!$navMobile){//jika nav pc
?>
<ul class="d-flex flex-row align-items-center justify-content-start" id="navpc">
	<?php
	$i=0;
	foreach ($listNavigasi as $nav){
	$i++;
	if($nav->Url!==""){
		$liActive1 = '';
		if ($cu == $si.$nav->Url) {
			$liActive1 = 'active';
		}
	}
	if($i===2){
	?>
		<li class="nav-item dropdown">
			<!--data-toggle="dropdown"-->
			<a class="dropdown-toggle" href="<?=$si?>pelayanan" id="navbarDropdown1" role="button"
			   aria-haspopup="true" aria-expanded="false">
				Pelayanan
			</a>
			<ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
				<?php foreach ($qryService as $val) {?>
					<li class="nav-item dropdown">
						<a class="dropdown-item dropdown-toggle" href="<?=$si."pelayanan/$val->NamaKategori"?>" id="nav<?=$val->KodeKategori?>" role="button" data-toggle="dropdown"
						   aria-haspopup="true" aria-expanded="false">
							<?=$val->NamaKategoriId?>
						</a>
						<ul class="dropdown-menu" aria-labelledby="nav<?=$val->KodeKategori?>">
							<?php
							$this->db->select('JudulId,UrlArtikel');
							$this->db->where('KodeKategori', $val->KodeKategori);
							$qrySubService = $this->db->get('t_artikel')->result();
							foreach ($qrySubService as $valSub){
							?>
							<li><a class="dropdown-item" href="<?=$si.'artikel/'.$valSub->UrlArtikel?>"><?=$valSub->JudulId?></a></li>
							<?php } ?>
							<!--<li class="nav-item dropdown">
								<a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown"
								   aria-haspopup="true" aria-expanded="false">
									Left Dropdown
								</a>
								<ul class="dropdown-menu left" aria-labelledby="navbarDropdown2">
									<li><a class="dropdown-item" href="#">Action</a></li>
									<li><a class="dropdown-item" href="#">Another action</a></li>
								</ul>
							</li>-->
						</ul>
					</li>
				<?php } ?>
			</ul>
		</li>
	<?php } ?>
	<li class="<?=$liActive1?>"><a href="<?=$si.$nav->Url?>"><?=$nav->NamaNavigasi?></a></li>
	<?php } ?>
</ul>
<?php }else{ ?>
	<nav class="menu_nav" id="navmobile">
		<ul>
		<?php
		$i=0;
		foreach ($listNavigasi as $nav) {$i++;?>
			<li <?=$liActive1?>><a href="<?=$si.$nav->Url?>"><?=$nav->NamaNavigasi?></a></li>
			<?php if($i===1){?>
				<li class="nav-item dropdown" style="background-color: #CDAC65">
					<a class="dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Pelayanan
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown2" style="background-color: #CDAC65">
						<?php foreach ($qryService as $val) {?>
							<a class="dropdown-item" href="<?=$si."pelayanan/$val->NamaKategori"?>"><?=$val->NamaKategoriId?></a>
						<?php } ?>
					</div>
				</li>
			<?php } ?>
		<?php } ?>

		</ul>
	</nav>
<?php } ?>
