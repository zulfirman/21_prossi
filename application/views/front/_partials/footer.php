<!-- Footer -->
<style>
	.opening_hours td, th {
		color: white;
	}
	.opening_hours{
		padding-bottom: 0;
		border: solid 0;
	}
	.locations_list {
		max-width: 100%;
		margin-top: 42px;
	}
	::-webkit-scrollbar {
		width: 0px;
		background: transparent; /* make scrollbar transparent */
	}
</style>
<footer class="footer" style="background: #e3ce99 ">
	<div class="footer_content">
		<div class="container" style="max-width: 1200px">
			<div class="row">
				<!-- Footer Locations -->
				<div class="col-sm-2 mb-4">
					<div class="col-md-12">
						<div class="logo" >
							<a href="#">
								<img class="mylogo logo_footer" src="<?=$linkUpload.$pe->Logo?>" alt="<?=$pe->NamaWebsite?>" aria-roledescription="logo" id="logo" style="background: white">
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-7 mb-4">
					<div class="col-md-12">
						<div class="footer_title mb-4 font-weight-bold" style="color: black">Prossi Center</div>
						<?=$pe->Alamat ?>
					</div>
				</div>

				<!-- Footer Opening Hours -->
				<div class="col-md-3 mb-4">
					<div class="col-md-12">
						<div class="footer_title mb-4 font-weight-bold" style="color: black">Operasional Hours</div>
						<?= $pe->JamBuka ?>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="footer_bar">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="footer_bar_content  d-flex flex-md-row flex-column align-items-md-center justify-content-start">
						<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;<script>document.write(new Date().getFullYear());</script> <?=$pe->NamaWebsite?></a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</div>
						<nav class="footer_nav ml-md-auto">
							<ul class="d-flex flex-row align-items-center justify-content-start">
								<?php
								foreach ($listNavigasi as $nav){
								?>
								<li><a href="<?=$si.$nav->Url?>"><?=$nav->NamaNavigasi?></a></li>
								<?php } ?>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
</div>

<div class="modal modal-produk modal-promosi fade bs-example-modal-lg show" id="modalPromo" tabindex="-1" role="dialog" aria-labelledby="modalPromo" style="padding-right: 17px;">
	<div class="modal-dialog modal-lg box-shadow" role="document">
		<div class="modal-content bpinkmuda">
			<div class="modal-header">
				<h3 class="modal-title">Penawaran menarik !</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body bpinkmuda">
				<?php
				$this->db->select('judulId, UrlArtikel, ContentId, MainImage');
				$this->db->where('Tipe', 2);
				$this->db->where('Lainnya1', 1);
				$getPopUpPromo=$this->db->get('t_artikel', 1)->first_row();
				if($getPopUpPromo!==null){
				?>
				<div class="row">
					<div class="col-md-12 mb-4 text-center">
						<h2><?=$getPopUpPromo->judulId?></h2>
					</div>
					<div class="col-md-12 mb-4 text-center">
						<img src="<?=$linkUpload.$getPopUpPromo->MainImage?>" alt="" class="img-responsive">
					</div>
					<div class="col-md-12 mb-4">
						<?=$getPopUpPromo->ContentId?>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-produk modal-promosi fade bs-example-modal-lg show" id="modalKontak" tabindex="-1" role="dialog" aria-labelledby="modalKontak" style="padding-right: 17px;">
	<div class="modal-dialog modal-lg box-shadow" role="document">
		<div class="modal-content bpinkmuda">
			<div class="modal-header">
				<h3 class="modal-title">Kontak Kami</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body bpinkmuda">
				<div class="row px-2">
					<div class="col-md-12">
						<?=$pe->NomorTelepon?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg show " id="modalDynamic" tabindex="-1" role="dialog" aria-labelledby="modalDynamic">
	<div class="modal-dialog modal-lg box-shadow" role="document">
		<div class="modal-content bpinkmuda">
			<div class="modal-header">
				<h3 class="modal-title"></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body bpinkmuda">
				<div class="row" id="contentModal">
					<div class="col-md-12 mb-4 text-center">
						<h2 id="dJudul"></h2>
					</div>
					<div class="col-md-12 mb-4 text-center">
						<img id="dGambar" src="" alt="" class="img-responsive">
					</div>
					<div class="col-md-12 mb-4" id="dContent">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>


<script src="<?=$si?>assets/gadxcd/js/jquery.min.js"></script>
<script src="<?=$si?>assets/gadxcd/styles/bootstrap-4.1.2/popper.js"></script>
<script src="<?=$si?>assets/gadxcd/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="<?=$si?>assets/gadxcd/plugins/greensock/TweenMax.min.js"></script>
<script src="<?=$si?>assets/gadxcd/plugins/scrollmagic/ScrollMagic.min.js"></script>
<!--
<script src="<?/*=$si*/?>assets/gadxcd/plugins/greensock/TimelineMax.min.js"></script>
<script src="<?/*=$si*/?>assets/gadxcd/plugins/greensock/animation.gsap.min.js"></script>
<script src="<?/*=$si*/?>assets/gadxcd/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="<?/*=$si*/?>assets/gadxcd/plugins/easing/easing.js"></script>
<script src="<?/*=$si*/?>assets/gadxcd/plugins/bootnavbar/bootnavbar.js"></script>-->
<script src="<?=$si?>assets/gadxcd/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="<?=$si?>assets/gadxcd/plugins/parallax-js-master/parallax.min.js"></script>
<script src="<?=$si?>assets/gadxcd/plugins/jquery-datepicker/jquery-ui.js"></script>
<script src="<?=$si?>assets/gadxcd/js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="<?=$si?>assets/xdfxcd/js/formplugins/select2/select2.bundle.min.js"></script>

<script>
	var baseUrl='<?=$si?>';
	var curUrl='<?=$cu?>';
	var browserWindow = $(window);
	var urlImage = '<?=$linkUpload?>';
	// :: 1.0 Preloader Active Code
	browserWindow.on('load', function () {
		$('.preloader').fadeOut('slow', function () {
			$(this).remove();
		});
	});
	setTimeout(function () {
		$('.preloader').remove()
	}, 4000);
	$(window).resize(function () {
		/*if($(window).width()>969){//pc
			$('#navpc').bootnavbar();
			$('#logo').css('height','80px');
			$('#logo').css('width','80px');
			$('#logo').css('margin-bottom','6px');
		}else{
			$('#logo').css('height','45px');
			$('#logo').css('width','45px');
			$('#logo').css('width','8px');
		}*/
	});
	function mdynamic(id,judul,gambar,content=''){
		$('#modalDynamic').modal('show');
		$('#dJudul').html(judul);
		$('#dGambar').prop('src', gambar);
		if(content===''){
			$('#dContent').html('<br><br>'+$('#content'+id).html());
		}else{
			$('#dContent').html('<br><br>'+content);
		}
	}
	var isShowedMap=0;
	$(document).ready(function () {
		getCsr();
		//$('#navpc').bootnavbar();
		if ($("#divMap").length !== 0) {
			showMap();
		}
		<?php if(!empty($getPopUpPromo)){?>
		if ($.cookie('janganTampilPromo') === undefined) {
			setTimeout(function () {
				$('#modalPromo').modal('show');
			}, 5000);
		}
		$("#modalPromo").on('hide.bs.modal', function () {
			dontShowAgain();
		});
		<?php } ?>
		allSearch();
		$('#allSearch').on('change', function() {
			var isi=JSON.parse($('#allSearch').val());
			var image=isi.MainImage;
			if(isi.Tipe==='3'){
				image=isi.Lainnya1;
			}
			mdynamic(isi.KodeArtikel,isi.JudulId,urlImage+image,isi.ContentId);
		});
		lazyImageDefault();
	});
	$.fn.isOnScreen = function(){
		var win = $(window);
		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();
		var bounds = this.offset();
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();
		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
	};
	function showMap() {
		$(window).scroll(function () {
			if ($('#divMap').isOnScreen()) {
				if (isShowedMap === 0) {
					$('#mapContent').html(`<?=$pe->Map?>`);
					isShowedMap = 1;
				}
			}
		});
	}
	function initTestSlider() {
		var testSlider = $('.test_slider');
		if(testSlider.length)
		{
			testSlider.owlCarousel({
				items: 1,
				autoplay: true,
				loop: true,
				dots: true,
				smartSpeed: 1200,
				nav: true,
				navText : ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
			});
		}
		var aboutSlider = $('.about_slider');
		if(aboutSlider.length)
		{
			aboutSlider.owlCarousel({
				items: 1,
				autoplay: true,
				loop: true,
				dots: true,
				smartSpeed: 1200,
				nav: true,
				navText : ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
			});
		}
		var promoSlider = $('.promo_slider');
		if(promoSlider.length)
		{
			promoSlider.owlCarousel({
				items: 3,
				autoplay: true,
				loop: false,
				rewind: true,
				dots: true,
				smartSpeed: 1200,
				nav: true,
				//navText : ['<i class="fas fa-chevron-circle-left"></i>','<i class="fas fa-chevron-circle-right"></i>'],
				navText : ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
				//stageOuterClass: 'visible',
				responsive : {
					320 : { items : 1  }, // from zero to 480 screen width 4 items
					480 : { items : 1  }, // from zero to 480 screen width 4 items
					768 : { items : 2  }, // from 480 screen widthto 768 6 items
					1024 : { items : 3   // from 768 screen width to 1024 8 items
					}
				},
			});
		}
	}
	function getCsr() {
		var csName='<?=$this->security->get_csrf_token_name()?>';//csfrn
		var csValue='<?=$this->security->get_csrf_hash()?>';//csrfv
		$.ajaxPrefilter(function (options, originalOptions, jqXHR) {//setting ajax
			if (originalOptions.data instanceof FormData) {
				originalOptions.data.append(csName, csValue);
			}
			else if (options.type.toLowerCase() === 'post') {
				options.data += '&'+csName+'='+csValue;
				if (options.data.charAt(0) === '&') {
					options.data = options.data.substr(1);
				}
			}
		});
		$.ajaxSetup({
			ajaxStart: function(){},
			beforeSend: function (result, status, xhr) {// sebelum send ajax
				let bsubmit=$(".bsubmit");
				bsubmit.prop('disabled', true);
				if(bsubmit.text()==="Simpan") {
					bsubmit.html('Menyimpan...');
				}
			},
			complete: function (result, status, xhr) {// complete ajax
				let bsubmit=$(".bsubmit");
				bsubmit.prop('disabled', false);
				if(bsubmit.text()==="Menyimpan...") {
					bsubmit.html('Simpan');
				}
			},
			error: function (result, status, xhr) { //global error
				$('.modal').modal('hide');
				let bsubmit=$(".bsubmit");
				bsubmit.prop('disabled', false);
				if(bsubmit.text()==="Menyimpan...") {
					bsubmit.html('Simpan');
				}
				if(xhr!=='abort') {

				}
			}
		});
	}
	function dontShowAgain(){
		$.cookie("janganTampilPromo", 1, { expires : 1 });
		//$.removeCookie("janganTampilPromo");
	}
	$('#kode').keypress(function( e ) {
		limitText(this, 20);
		if(e.which === 32){
			return false;
		}
	});
	function limitText(field, maxChar){
		var ref = $(field),
				val = ref.val();
		if ( val.length >= maxChar ){
			ref.val(function() {
				console.log(val.substr(0, maxChar));
				return val.substr(0, maxChar);
			});
		}
	}
	function allSearch(){
		$('#allSearch').select2({
			allowClear: true,
			minimumInputLength : 3, delay: 300,placeholder: "Search Everything",
			ajax: {
				url: '<?=$si?>api/search',
				type: "post",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term // search term
					};
				},
				processResults: function (response) {
					var user = response;
					return {
						results: response
					};
				},
				cache: true
			},
			language: {
				inputTooShort: function() {
					return 'Harap masukan 3 karakter atau lebih.';
				}
			}
		});
	}
	function lazyImageDefault(){
		var lazyloadImages;
		if ("IntersectionObserver" in window) {
			lazyloadImages = document.querySelectorAll(".lazy");
			var imageObserver = new IntersectionObserver(function(entries, observer) {
				entries.forEach(function(entry) {
					if (entry.isIntersecting) {
						var image = entry.target;
						image.src = image.dataset.src;
						image.classList.remove("lazy");
						imageObserver.unobserve(image);
					}
				});
			});

			lazyloadImages.forEach(function(image) {
				imageObserver.observe(image);
			});
		} else {
			var lazyloadThrottleTimeout;
			lazyloadImages = document.querySelectorAll(".lazy");

			function lazyload () {
				if(lazyloadThrottleTimeout) {
					clearTimeout(lazyloadThrottleTimeout);
				}

				lazyloadThrottleTimeout = setTimeout(function() {
					var scrollTop = window.pageYOffset;
					lazyloadImages.forEach(function(img) {
						if(img.offsetTop < (window.innerHeight + scrollTop)) {
							img.src = img.dataset.src;
							img.classList.remove('lazy');
						}
					});
					if(lazyloadImages.length === 0) {
						document.removeEventListener("scroll", lazyload);
						window.removeEventListener("resize", lazyload);
						window.removeEventListener("orientationChange", lazyload);
					}
				}, 20);
			}

			document.addEventListener("scroll", lazyload);
			window.addEventListener("resize", lazyload);
			window.addEventListener("orientationChange", lazyload);
		}
	}
</script>

</body>
</html>
