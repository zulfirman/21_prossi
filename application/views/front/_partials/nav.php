<?php
$listNavigasi=listNavigasi(1);
$this->db->where('IsService', 1);
$this->db->where('IsActive', 1);
$qryService = $this->db->get('m_kategori', 4)->result();
if(!$navMobile){//jika nav pc
?>
<ul class="d-flex flex-row align-items-center justify-content-start" id="navpc">
	<?php
	$i=0;
	foreach ($listNavigasi as $nav){
	$i++;
	if($nav->Url!==""){
		$liActive1 = '';
		if ($cu === $si.$nav->Url) {
			$liActive1 = 'active';
		}
	}
	if($i===3){
	?>
		<li class="dropdown">
			<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Services
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<?php foreach ($qryService as $qryServicev){?>
					<a class="dropdown-item" href="<?=$si."pelayanan/".$qryServicev->NamaKategori?>"><?=$qryServicev->NamaKategoriId?></a>
				<?php } ?>
			</div>
		</li>
	<?php }else{ ?>
	<li class="<?=$liActive1?>"><a href="<?=$si.$nav->Url?>"><?=$nav->NamaNavigasi?></a></li>
	<?php } }?>
</ul>
<?php }else{ ?>
	<nav class="menu_nav" id="navmobile">
		<ul>
		<?php
		$i=0;
		foreach ($listNavigasi as $nav) {$i++;
			if($i===3){?>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Services
					</a>
					<div class="dropdown-menu bemas" aria-labelledby="navbarDropdown">
						<?php foreach ($qryService as $qryServicev){?>
						<a class="dropdown-item" href="<?=$si."pelayanan/".$qryServicev->NamaKategori?>"><?=$qryServicev->NamaKategoriId?></a>
					<?php } ?>
					</div>
				</li>
			<?php }else { ?>
			<li class="nav-item <?=$liActive1?>"><a class="nav-link" href="<?=$si.$nav->Url?>"><?=$nav->NamaNavigasi?></a></li>
		<?php } }?>
		</ul>
	</nav>
<?php } ?>
