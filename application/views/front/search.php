<?php include APPPATH . 'views/front/_partials/header.php'; ?>
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/blog.css">
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/blog_responsive.css">

<?php include APPPATH . 'views/front/_part/css.php'; ?>

<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$pe->BackgroundHeader?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title">Hasil Pencarian</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<!-- Blog -->

	<div class="blog">
		<div class="container">
			<?php
			if(isset($_GET['type'])){
			?>
			<form action="<?=$si.'search'?>" method="get" class="mb-4">
				<div class="form-group">
					<label for="exampleInputEmail1">Pencarian <?=$_GET['type']?></label>
					<input type="text" class="form-control" id="txtcari" name="txtcari" value="<?=$_GET['keywords']?>">
					<input name="type" value="<?=$_GET['type']?>" hidden>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Cari</button>
			</form>
			<?php } ?>
			<div class="row">
				<?php
				if(isset($_GET['category'])){// search by kategori
					if (!isset($_GET['category'])) {
						redirect(site_url('artikel'));
					}
					$category=$_GET['category'];
					$this->db->where('c.NamaKategoriId', $category);
					$this->db->or_where('c.NamaKategoriEn', $category);
					$this->db->join('m_kategori c', 'find_in_set(c.KodeKategori, a.KodeKategori)');
				}
				else {
					if (!isset($_GET['keywords']) || !isset($_GET['type'])) {// search by nama dan type
						redirect(site_url('artikel'));
					}
					$Judul=$_GET['keywords'];
					$TipeArtikel=$_GET['type'];
					if($TipeArtikel==="artikel"){
						$this->db->where('a.Tipe', 1);
					}
					else if($TipeArtikel==="promo"){
						$this->db->where('a.Tipe', 2);
					}
					else {
						$this->db->where('a.Tipe', 3);
					}
					$this->db->like('a.JudulId', $Judul);
					$this->db->or_like('a.JudulEn', $Judul);
					if($TipeArtikel==="artikel"){
						$this->db->where('a.Tipe', 1);
					}
					else if($TipeArtikel==="promo"){
						$this->db->where('a.Tipe', 2);
					}
					else {
						$this->db->where('a.Tipe', 3);
					}
				}
				$this->db->select('a.KodeArtikel as myId, a.ContentId,a.ContentEn, a.UrlArtikel, a.JudulId, a.JudulEn, a.MainImage, a.DateCreated, b.NamaUser, b.Pekerjaan');
				$this->db->join('m_user b', 'a.CreatedBy = b.KodeUser','left');
				$this->db->order_by('a.DateCreated','desc');
				$qry=$this->db->get('t_artikel a', 300)->result();
				$i=0;
				$aturKolom='4';
				$maxContent=150;
				foreach ($qry as $val) {
					$i++;
					$tahunO = date("Y", strtotime($val->DateCreated));
					$bulanO = date("M", strtotime($val->DateCreated));
					$tanggalO = date("d", strtotime($val->DateCreated));
					$Content=$val->JudulId;
					if (strlen($Content) > 55) {
						// truncate string
						$stringCut = substr($Content, 0, 55);
						$endPoint = strrpos($stringCut, ' ');
						//if the string doesn't contain any space then it will cut without word basis.
						$Content = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
						$Content .= "...";
					}
				?>
				<div class="col-md-<?=$aturKolom?> mb-5">
					<!-- Blog Post -->
					<div class="blog_post">
						<div class="blog_post_image"><img src="<?=$linkUpload.$val->MainImage?>" alt=""></div>
						<div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
							<div class="date_day"><?=$tanggalO?></div>
							<div class="date_month"><?=$bulanO?></div>
							<div class="date_year"><?=$tahunO?></div>
						</div>
						<div class="blog_post_title"><a href="<?=$si.'artikel/'.$val->UrlArtikel?>"><?=$Content?></a></div>
						<div class="blog_post_info">
							<ul class="d-flex flex-row align-items-center justify-content-center">
								<li>by <a href="#"><?=$val->NamaUser?></a></li>
								<li>- <a href="#"><?=$val->Pekerjaan?></a></li>
								<li><a href="#" hidden>2 Comments</a></li>
							</ul>
						</div>
						<div class="blog_post_text text-center">
							<p><?=substr(strip_tags($val->ContentId),0, $maxContent)?>...</p>
						</div>
						<div class="blog_post_button text-center"><div class="button button_1 ml-auto mr-auto"><a href="<?=$si.'artikel/'.$val->UrlArtikel?>">read more</a></div></div>
					</div>

				</div>
				<?php } ?>
			</div>
			<div class="row page_nav_row">
				<div class="col">
					<div class="page_nav">
						<ul class="d-flex flex-row align-items-center justify-content-center">
							<!--<li class="active"><a href="#">01.</a></li>
							<li><a href="#">02.</a></li>
							<li><a href="#">03.</a></li>-->
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include APPPATH . 'views/front/_part/janji.php'; ?>
<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
