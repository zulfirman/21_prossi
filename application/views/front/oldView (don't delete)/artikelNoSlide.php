<?php include APPPATH . 'views/front/_partials/header.php'; ?>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about.css">
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/about_responsive.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>
<style>
	.why{
		padding-top: 70px;
		padding-bottom: 81px;
	}
	.margin-card-body{
		margin-bottom: -15%;
	}

	.c {
		padding: 15px;
		margin-top: 30px;
		/*box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.1);*/
	}

	.bottom-sec {
		width: 90%;
		object-fit: cover;
		border-radius: 3px;
		margin-left: 12px;
		margin-top: -50px;
		box-shadow: 0 3px 20px 11px rgba(0, 0, 0, 0.09);
	}

	.c .top-sec {
		margin-top: -30px;
		margin-bottom: 15px;
	}
</style>
<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$bHeader->bArtikel?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title"><?=$tHeader->tArtikel?></div>
						<div class="home_text" hidden>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="why">
	<!-- <div class="background_image" style="background-image:url(images/why.jpg)"></div> -->
	<div class="container">
		<div class="row row-eq-height">
			<!-- Why Choose Us Content -->
			<div class="col-lg-12 order-lg-2 order-1">
				<div class="why_content">
					<div class="col text-center">
						<div class="section_title_container">
							<div class="section_title mb-4"><h3>Artikel</h3></div>
						</div>
					</div>
					<div class="row services_row">
						<div class="container">
							<div class="row">
								<div class="row">
									<?php
									foreach ($listArtikel['result'] as $val){?>
										<div class="col-md-4 mb-4 filter">
											<div class="c">
												<div class="top-sec">
													<a href="<?=$si.'artikel/'.$val->UrlArtikel?>">
														<img src="<?=$linkUpload.$val->MainImage?>">
													</a>
												</div>
												<div class="bottom-sec bemas">
													<div class="col-md-12">
														<a href="<?=$si.'artikel/'.$val->UrlArtikel?>">
															<h4><?=$val->JudulId?></h4>
														</a>
													</div>
												</div>
												<div class="col-md-12">
													<p><?=substr(strip_tags($val->ContentId),0, 150)?>...</p>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="row page_nav_row">
								<div class="col">
									<div class="page_nav">
										<ul class="d-flex flex-row align-items-center justify-content-center" id="pagination" style="font-size: 18px">
											<?= $listArtikel['pagination'];?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Call to action -->
<?php include APPPATH . 'views/front/_part/janji.php'; ?>

<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
	});
</script>
