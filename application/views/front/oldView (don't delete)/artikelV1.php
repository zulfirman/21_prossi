<?php include APPPATH . 'views/front/_partials/header.php'; ?>
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/blog.css">
	<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/blog_responsive.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>

<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$pe->BackgroundHeader?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title"><?php
							$tipenya='';
							if(strtolower($this->uri->segment(1))!=='pelayanan') {
								$tipenya= ucwords($this->uri->segment(1));
								echo $tipenya;
							}else{
								$this->db->select('DeskripsiId,DeskripsiEn');
								$this->db->where('NamaKategori', $this->uri->segment(2));
								$desk=$this->db->get('m_kategori', 1)->first_row();
								$tipenya= ucwords(str_replace('-',' ',$this->uri->segment(2)??''));
								echo $tipenya;
								echo "<div class='home_text' style='font-size: 15px'>$desk->DeskripsiId.</div>";
							}
							?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


	<!-- Blog -->

	<div class="blog">
		<div class="container">
			<form action="<?=$si.'search'?>" method="get" class="mb-4">
				<div class="form-group">
					<label for="exampleInputEmail1">Pencarian <?=$tipenya?></label>
					<input type="text" class="form-control" id="keywords" name="keywords">
					<input name="type" value="<?=strtolower($tipenya)?>" hidden>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Cari</button>
			</form>

			<div class="gallery-block cards-gallery" style="padding-bottom: 10px; padding-top: 10px">
				<div class="row">
					<!--<div class="col-md-6 col-lg-4 mb-3">
						<div class="card border-0 transform-on-hover h-100">
							<a class="lightbox" href="https://localhost/21_prossi/assets/uploads/21571998ffff210.jpg">
								<img src="https://localhost/21_prossi/assets/uploads/21571998ffff210.jpg" alt="Card Image" class="card-img-top">
							</a>
							<div class="card-body">
								<h6 hidden><a href="#">Lorem Ipsum</a></h6>
								<p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
							</div>
						</div>
					</div>-->
					<?php
					$aturKolom='4';
					$maxContent=150;
					foreach ($listArtikel['result'] as $val){
						$tahunO = date("Y", strtotime($val->DateCreated));
						$bulanO = date("M", strtotime($val->DateCreated));
						$tanggalO = date("d", strtotime($val->DateCreated));
						$Content=$val->JudulId;
						if (strlen($Content) > 55) {
							// truncate string
							$stringCut = substr($Content, 0, 55);
							$endPoint = strrpos($stringCut, ' ');
							//if the string doesn't contain any space then it will cut without word basis.
							$Content = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
							$Content .= "...";
						}
						?>
						<div class="col-md-<?=$aturKolom?> mb-5">
							<!-- Blog Post -->
							<div class="blog_post">
								<div class="blog_post_image"><img src="<?=$linkUpload.$val->MainImage?>" alt=""></div>
								<div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
									<div class="date_day"><?=$tanggalO?></div>
									<div class="date_month"><?=$bulanO?></div>
									<div class="date_year"><?=$tahunO?></div>
								</div>
								<div class="blog_post_title"><a href="<?=$si.'artikel/'.$val->UrlArtikel?>"><?=$Content?></a></div>
								<div class="blog_post_info">
									<ul class="d-flex flex-row align-items-center justify-content-center">
										<li>by <a href="#"><?=$val->NamaUser?></a></li>
										<li> - <a href="#"><?=$val->Pekerjaan?></a></li>
										<li><a href="#" hidden>2 Comments</a></li>
									</ul>
								</div>
								<div class="blog_post_text text-center">
									<p><?=substr(strip_tags($val->ContentId),0, $maxContent)?>...</p>
								</div>
								<div class="blog_post_button text-center"><div class="button button_1 ml-auto mr-auto"><a href="<?=$si.'artikel/'.$val->UrlArtikel?>">read more</a></div></div>
							</div>

						</div>
					<?php } ?>
				</div>
			</div>
			<div class="row page_nav_row">
				<div class="col">
					<div class="page_nav">
						<ul class="d-flex flex-row align-items-center justify-content-center">
							<?= $listArtikel['pagination'];?>
							<!--<li class="active"><a href="#">01.</a></li>
							<li><a href="#">02.</a></li>
							<li><a href="#">03.</a></li>-->
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include APPPATH . 'views/front/_part/janji.php'; ?>
<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
