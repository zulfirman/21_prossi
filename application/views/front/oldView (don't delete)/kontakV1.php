<?php include APPPATH . 'views/front/_partials/header.php'; ?>
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/contact.css">
<link rel="stylesheet" type="text/css" href="<?=$si?>assets/gadxcd/styles/contact_responsive.css">
<?php include APPPATH . 'views/front/_part/css.php'; ?>
<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$linkUpload.$bHeader->bKontak?>" data-speed="0.8"></div>
	<!--<div class="home_overlay"><img src="<?/*=$si*/?>assets/gadxcd/images/home_overlay.png" alt=""></div>-->
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title"><?=$tHeader->tKontak?></div>
						<div class="home_text" hidden>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Contact -->

<div class="contact">
	<div class="container">
		<div class="row">

			<!-- Contact Form -->
			<div class="col-lg-6">
				<?php include APPPATH . 'views/front/_part/kontak-form.php'; ?>
			</div>

			<!-- Contact Content -->
			<div class="col-lg-5 offset-lg-1 contact_col">
				<div class="contact_content">
					<div class="contact_content_title">Hubungi Kami</div>
					<div class="direct_line d-flex flex-row align-items-center justify-content-start">
						<div class="direct_line_title text-center bpinkmuda">KONTAK</div>
					   <a href="#" class="direct_line_num text-center" data-toggle="modal" data-target="#modalKontak">Chat Via Whatsapp</a>
					</div>
					<div class="contact_content_text">
						<?=$pe->Kontak?>
					</div>
					<div class="contact_content_title">Opening Hour</div>
					<div class="contact_content_text">
						<?=$pe->JamBuka?>
					</div>
					<div class="contact_social">
						<ul class="d-flex flex-row align-items-center justify-content-start">
							<!--<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>-->
							<?php include APPPATH . 'views/front/_part/social-media.php'; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row google_map_row">
			<div class="col">

				<!-- Contact Map -->

				<div class="contact_map" id="divMap">

					<!-- Google Map -->

					<div class="map">
						<div id="google_map" class="google_map">
							<div class="map_container">
								<div id="mapContent"></div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
</div>

<?php include APPPATH . 'views/front/_partials/footer.php'; ?>
