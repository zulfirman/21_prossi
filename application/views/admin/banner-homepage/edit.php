<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
					</h2>
				<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content">
						<form class="kt-form" method="post" action="<?=$cu?>" enctype="multipart/form-data">
							<?=inCsrf()?>
							<div class="form-row">
								<input name="KodeGambar" id="KodeGambar" type="text" value="<?=$val->KodeGambar?>" class="form-control" required hidden>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="Lainnya1">Video<span class="text-danger">*</span></label>
									<div class="input-group">
										<input type="text" value="<?=$val->Lainnya1?>" id="Lainnya1" name="Lainnya1" required class="form-control" readonly>
										<div class="input-group-append">
											<button onclick="ckChoose('Lainnya1');" class="btn btn-primary waves-effect waves-themed" type="button" id="button-addon5"><i class="fal fa-search"></i></button>
										</div>
									</div>
								</div>
								<div class="col-md-12 mb-4">
									<label class="form-label" for="ContentId">Konten Id</label>
									<textarea id="ContentId" name="ContentId" rows="4" required class=" form-control"><?=$val->ContentId?></textarea>
								</div>
								<div class="col-md-12 mb-4" hidden>
									<label class="form-label" for="ContentEn">Konten En</label>
									<textarea id="ContentEn" name="ContentEn" rows="4" class=" form-control"><?=$val->ContentEn?></textarea>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-4" style="text-align: center;">
									<a href="../" type="button" class="btn btn-default waves-effect waves-themed">Kembali</a>&nbsp
									<button type="submit" class="btn btn-primary waves-effect waves-themed">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
		CKEDITOR.replace('ContentId',{
		});
		CKEDITOR.replace('ContentEn',{
		});
	});
</script>
