<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?>
					</h2>
				<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content table-responsive">
						<?php
						if(isset($_SESSION['fnotif'])){notifSukses($_SESSION['fnotif'],'success');}
						?>
						<!-- datatable start -->
						<p class="text-align-right" style="text-align: right" hidden>
							<a href="<?=$cu."/tambah"?>" type="button" class="btn btn-primary waves-effect waves-themed">Tambah</a>
						</p>
						<table id="table1" class="table table-bordered table-hover table-striped w-100">
							<thead>
							<tr>
								<th>Video</th>
								<th>Konten Id</th>
								<th hidden>Konten En</th>
								<th>Created</th>
								<th>Updated</th>
								<th>Aksi</th>
							</tr>
							</thead>
							<tbody>
							<?php
							foreach ($getAll as $val){
							?>
							<tr>
								<td><a href="<?=$val->Lainnya1?>" target="_blank">Lihat Video</a> </td>
								<td><?=substr((strip_tags($val->ContentId)),0,200)?></td>
								<td hidden><?=substr((strip_tags($val->ContentEn)),0,200)?></td>
								<td><?=$val->Ct?></td>
								<td><?=$val->Up?></td>
								<td>
									<a href="<?=$cu."/edit/$val->KodeGambar"?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"
									   class="btn btn-info btn-icon rounded-circle waves-effect waves-themed">
										<i class="fal fa-edit"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
							</tbody>
						</table>
						<!-- datatable end -->
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	$('#table1').DataTable({
			responsive: true,
			'pageLength': 50,
			'order': [[0, 'desc']],
		});
</script>
