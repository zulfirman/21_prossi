<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
					</h2>
				<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content">
						<form class="kt-form" method="post" action="<?=$cu?>" enctype="multipart/form-data">
							<?=inCsrf()?>
							<?php
							$listFoto=json_decode($val->Lainnya1);
							//$listFoto=array();
							?>
							<div class="form-row">
								<input name="KodeGambar" id="KodeGambar" type="text" value="<?=$val->KodeGambar?>" class="form-control" required hidden>
								<!--<div class="col-md-6 mb-4">
									<label class="form-label" for="MainImage">Gambar<span class="text-danger">*</span></label>
									<div><img class="mb-4" src="<?/*="$linkUpload".$val->MainImage*/?>" style="height: 200px; width:300px"></div>
									<input name="MainImage" id="MainImage" type="file" value="" class="form-control">
									<input name="OldImage" id="OldImage" type="text" value="<?/*=$val->MainImage*/?>" class="form-control" hidden>
								</div>-->
								<div class="col-md-12 mb-4">
									<label class="form-label" for="ContentId">Konten Id</label>
									<textarea id="ContentId" name="ContentId" rows="4" required class=" form-control"><?=$val->ContentId?></textarea>
								</div>
								<div class="col-md-12 mb-4" hidden>
									<label class="form-label" for="ContentEn">Konten En</label>
									<textarea id="ContentEn" name="ContentEn" rows="4" class=" form-control"><?=$val->ContentEn?></textarea>
								</div>
							</div>
							<div id="rowMultiple">
								<?php foreach ($listFoto as $val){?>
									<div class="row" id="div<?=$val->Id?>">
										<div class="col-md-12 mb-6">
											<label for="CabangImage<?=$val->Id?>">Gambar Cabang (Rekomendasi Pixel 500 x 500)</label>
											<br>
											<img class="mb-4" src="<?=$val->CabangImage?>" style="height: 200px; width:200px">
											<div class="input-group">
												<input type="text" value="<?=$val->CabangImage?>" id="CabangImage<?=$val->Id?>" name="CabangImage[]" required class="form-control" readonly>
												<div class="input-group-append">
													<button onclick="ckChoose('<?="CabangImage$val->Id"?>')" class="btn btn-primary waves-effect waves-themed" type="button" id="button-addon5"><i class="fal fa-search"></i></button>
												</div>
											</div>
										</div>
										<div class="col-md-12 mb-4">
											<label class="form-label" for="NamaCabang<?=$val->Id?>">Nama Cabang<span class="text-danger">*</span></label>
											<input type="text" value="<?=$val->NamaCabang?>" id="NamaCabang<?=$val->Id?>" name="NamaCabang[]" class="form-control">
										</div>
										<div class="col-md-12 mb-4 text-center">
											<button type="button" class="btn btn-danger waves-effect waves-themed" onclick="hapusData('<?=$val->Id?>')">Remove</button>&nbsp
										</div>
									</div>
								<?php } ?>
							</div>
							<div class="row">
								<div class="col-md-12 mb-4 text-center">
									<button type="button" class="btn btn-success waves-effect waves-themed" onclick="addData()">Add More</button>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-4" style="text-align: center;">
									<a href="../" type="button" class="btn btn-default waves-effect waves-themed">Kembali</a>&nbsp
									<button type="submit" class="btn btn-primary waves-effect waves-themed">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
		CKEDITOR.replace('ContentId',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		CKEDITOR.replace('ContentEn',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
	});
	function hapusData(id){
		Swal.fire({
			title: "Yakin Akan Hapus ?",
			type: "warning",
			reverseButtons: true,
			confirmButtonColor: '#428bca',
			showCancelButton: true,
			confirmButtonText: "Ya !",
			cancelButtonText: "Tidak !",
		}).then(function (result) {
			if (result.value) {
				$('#div'+id).empty();
			}
		});
	}
	function addData(){
		let randomId = Math.random().toString(36).substring(7);
		var html=`<div class="row" id="div${randomId}">
								<div class="col-md-6 mb-6">
									<label class="form-label" for="CabangImage${randomId}">Gambar Cabang (Rekomendasi Pixel 500 x 500)<span class="text-danger">*</span></label>
									<div class="input-group">
										<input type="text" value="" id="CabangImage${randomId}" name="CabangImage[]" required class="form-control" readonly>
										<div class="input-group-append">
											<button onclick="ckChoose('CabangImage${randomId}');" class="btn btn-primary waves-effect waves-themed" type="button" id="button-addon5"><i class="fal fa-search"></i></button>
										</div>
									</div>
								</div>
									<div class="col-md-6 mb-4">
										<label class="form-label" for="NamaCabang${randomId}">Nama Cabang<span class="text-danger">*</span></label>
										<input type="text" value="" id="NamaCabang${randomId}" name="NamaCabang[]" class="form-control">
									</div>
									<div class="col-md-12 mb-4 text-center">
										<button type="button" class="btn btn-danger waves-effect waves-themed" onclick="hapusData('${randomId}')">Remove</button>&nbsp
									</div>
								</div>`;
		$('#rowMultiple').append(html);
	}
</script>
