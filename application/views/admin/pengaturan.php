<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)??''))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)??''))?>
					</h2>
				<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content">
						<form class="kt-form" method="post" action="<?=$cu?>" enctype="multipart/form-data">
							<?=inCsrf()?>
							<?php if(isset($_SESSION['fnotif'])){notifSukses($_SESSION['fnotif'],'success');}?>
							<div class="form-row">
								<input hidden type="text" value="<?=cet($val->KodePengaturan)?>" id="KodePengaturan" name="KodePengaturan" class="form-control">
								<?php foreach (array('Logo', 'Icon') as $list) { ?>
									<input hidden type="text" value="<?= cet($val->$list) ?>" class="form-control" id="oldImage<?=$list?>" name="oldImage<?=$list?>">
								<?php } ?>
								<?php
								$backgroundHeader=json_decode($val->BackgroundHeader);
								$backgroundFooter=json_decode($val->BackgroundFooter);
								foreach ($backgroundHeader??[] as $array_key => $array_value) {?>
									<input hidden type="text" value="<?=$backgroundHeader->$array_key?>" class="form-control" id="oldImage<?=$array_key?>" name="oldImage<?=$array_key?>">
								<?php } ?>
								<div class="col-md-12 mb-6" hidden>
									<input type="text" value="<?=cet($val->BackgroundHeader)?>" id="jsonBackground" name="jsonBackground" class="form-control">
								</div>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="Logo">Logo (PNG)<span class="text-danger">*</span></label>
									<div><img src="<?=$linkUpload.cet($val->Logo)?>" style="width: 100px;height: 100px"></div>
									<input type="file" value="" id="Logo" name="Logo" class="form-control">
								</div>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="Icon">Icon<span class="text-danger">*</span></label>
									<div><img src="<?=$linkUpload.cet($val->Icon)?>" style="width: 100px;height: 100px"></div>
									<input type="file" value="" id="Icon" name="Icon" class="form-control">
								</div>
								<div class="col-md-12 mb-6">
									<label class="form-label" for="NamaWebsite">Nama Website</label>
									<input type="text" value="<?=cet($val->NamaWebsite)?>" id="NamaWebsite" name="NamaWebsite" class="form-control">
								</div>
								<div class="col-md-12 mb-6" hidden>
									<label class="form-label" for="TentangId">Tentang Kami Id<span class="text-danger">*</span></label>
									<textarea id="TentangId" name="TentangId" rows="4" required class=" form-control"><?=cet($val->TentangId)?></textarea>
								</div>
								<div class="col-md-12 mb-6" hidden>
									<label class="form-label" for="TentangEn">Tentang Kami En<span class="text-danger">*</span></label>
									<textarea id="TentangEn" name="TentangEn" rows="4" class=" form-control"><?=cet($val->TentangEn)?></textarea>
								</div>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="Kontak">Kontak<span class="text-danger">*</span></label>
									<textarea id="Kontak" name="Kontak" rows="4" required class=" form-control"><?=cet($val->Kontak)?></textarea>
								</div>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="JamBuka">Jam Buka<span class="text-danger">*</span></label>
									<textarea id="JamBuka" name="JamBuka" rows="4" required class=" form-control"><?=cet($val->JamBuka)?></textarea>
								</div>
								<div class="col-md-12 mb-6">
									<label class="form-label" for="NomorTelepon">Nomor Telepon<span class="text-danger">*</span></label>
									<textarea id="NomorTelepon" name="NomorTelepon" rows="4" required class=" form-control"><?=cet($val->NomorTelepon)?></textarea>
								</div>
								<div class="col-md-4 mb-6">
									<label class="form-label" for="Email">Email<span class="text-danger">*</span></label>
									<input type="text" value="<?=cet($val->Email)?>" id="Email" name="Email" required class="form-control">
								</div>
								<div class="col-md-4 mb-6">
									<label class="form-label" for="Facebook">Facebook</label>
									<input type="text" value="<?=cet($val->Facebook)?>" id="Facebook" name="Facebook" class="form-control">
								</div>
								<div class="col-md-4 mb-6">
									<label class="form-label" for="Instagram">Instagram</label>
									<input type="text" value="<?=cet($val->Instagram)?>" id="Instagram" name="Instagram" class="form-control">
								</div>
								<div class="col-md-4 mb-6" hidden>
									<label class="form-label" for="Instagram">Twitter</label>
									<input type="text" value="<?=cet($val->Twitter)?>" id="Twitter" name="Twitter" class="form-control">
								</div>
								<div class="col-md-12 mb-6">
									<label class="form-label" for="Alamat">Alamat<span class="text-danger">*</span></label>
									<textarea id="Alamat" name="Alamat" rows="4" required class=" form-control"><?=cet($val->Alamat)?></textarea>
								</div>
								<div class="col-md-12 mb-6" hidden>
									<label class="form-label" for="Maps">Maps<span class="text-danger">*</span></label>
									<textarea id="Map" name="Map" rows="3" required class=" form-control"><?=cet($val->Map)?></textarea>
								</div>
								<div class="col-md-4 mb-6" hidden>
									<label class="form-label" for="EnableMultiLanguage">Enable Multi Language<span class="text-danger">*</span></label>
									<select id="EnableMultiLanguage" name="EnableMultiLanguage" required class="form-control">
										<option value="1">Ya</option>
										<option value="0">Tidak</option>
									</select>
								</div>
								<div class="col-md-4 mb-6" hidden>
									<label class="form-label" for="WarnaTema">Warna Tema <span class="text-danger">*</span></label>
									<input type="text" value="<?=cet($val->WarnaTema)?>" id="WarnaTema" name="WarnaTema" class="form-control colorpicker-element">
								</div>
								<div class="col-md-4 mb-6">
									<label class="form-label" for="Instagram">Last Updated</label>
									<input type="text" value="<?=cet($val->DateUpdated) ?> | <?=cet($val->UpdatedBy) ?>" disabled class="form-control">
								</div>
								<div class="col-md-4 mb-6"></div>
								<div class="col-md-4 mb-6"></div>
								<?php foreach ($backgroundHeader??[] as $array_key => $array_value) {?>
									<div class="col-md-6 mb-6">
										<label class="form-label" for="<?=$array_key?>">Background <?=substr($array_key,1)?> (Rekomendasi Pixel 1299 x 622 px)<span class="text-danger">*</span></label>
										<img class="mb-4" src="<?=$linkUpload.$backgroundHeader->$array_key?>" style="height: 200px; width:300px">
										<input type="file" value="" id="<?=$array_key?>" name="<?=$array_key?>" class="form-control">
									</div>
									<?php
									$array_key='t'.substr($array_key,1);
									?>
									<div class="col-md-6 mb-6">
										<label class="form-label" for="<?=$array_key?>">Text <?=substr($array_key,1)?><span class="text-danger">*</span></label>
										<textarea id="<?=$array_key?>" name="<?=$array_key?>" rows="2" required class=" form-control"><?=$backgroundFooter->$array_key?></textarea>
									</div>
								<?php } ?>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-4" style="text-align: center;">
									<a href="./" type="button" class="btn btn-default waves-effect waves-themed">Kembali</a>&nbsp
									<button type="submit" class="btn btn-primary waves-effect waves-themed">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script src="<?=$sia?>assets/xdfxcd/js/formplugins/bootstrap-colorpicker/bootstrap-colorpicker.js"></script>
<script>
	$(document).ready(function () {
		$('.colorpicker-element').colorpicker();
		CKEDITOR.replace('Kontak',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		CKEDITOR.replace('JamBuka',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		CKEDITOR.replace('NomorTelepon',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		CKEDITOR.replace('Alamat',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		<?php foreach ($backgroundHeader??[] as $array_key => $array_value) {
		$array_key='t'.substr($array_key,1);
		?>
		CKEDITOR.replace('<?=$array_key?>',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		<?php } ?>
	});
</script>
