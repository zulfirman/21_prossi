<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
					</h2>
				<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content">
						<form class="kt-form" method="post" action="<?=$cu?>" enctype="multipart/form-data" id="formsimpan">
							<?=inCsrf()?>
							<?php if(isset($_SESSION['fnotif'])){notifSukses($_SESSION['fnotif'],'success');}?>
						<div class="form-row">
							<div class="col-md-6 mb-4">
								<label class="form-label" for="NamaKategoriUser">Nama Kategori User<span class="text-danger">*</span></label>
								<input name="NamaKategoriUser" id="NamaKategoriUser" type="text" class="form-control" required>
							</div>
							<div class="col-md-6 mb-4" hidden>
								<label class="form-label" for="TipeUser">Tipe User<span
										class="text-danger">*</span> </label>
								<select class="form-control" name="TipeUser" id="TipeUser" required onchange="refreshTable()">
									<option value="1" selected>Admin</option>
								</select>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-12 mb-4" style="text-align: center;">
								<a href="./" type="button" class="btn btn-default waves-effect waves-themed">Kembali</a>&nbsp
								<button type="submit" class="btn btn-primary waves-effect waves-themed">Simpan</button>
							</div>
							<div class="col-md-12 mb-4">
								<table id="table1" class="table table-bordered table-hover table-striped w-100">
									<thead>
									<tr>
										<th>Nama Menu</th>
										<th>Trustee</th>
										<th>Akses</th>
									</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	var table1='';
	$(document).ready(function () {
		initTable();
		$('#formsimpan').on('submit', function(e){
			var form = this;
			var rows_selected = table1.column(2).checkboxes.selected();
			// Iterate over all selected checkboxes
			$.each(rows_selected, function(index, rowId){
				// Create a hidden element
				$(form).append(
					$('<input>')
						.attr('type', 'hidden')
						.attr('name', 'Trustee[]')
						.val(rowId)
				);
			});
		});
		$('#Trustee').select2({placeholder: "-Pilih"});
		$("#pilihSemua").click(function () {
			if ($("#pilihSemua").is(':checked')) { //select all
				$("#Trustee").find('option').prop("selected", true);
				$("#Trustee").trigger('change');
			} else { //deselect all
				$("#Trustee").find('option').prop("selected", false);
				$("#Trustee").trigger('change');
			}
		});
	});
	function initTable() {
		table1 = $('#table1').DataTable({
			initComplete: function () {
				setTimeout(function () {
					//cekbtnprosses();
				}, 200);
			},
			responsive: true,
			'paginate': false,
			'columnDefs': [
				{
					'targets': 2,
					'checkboxes': {
						'selectRow': true,
					}
				}
			],
			'select': 'multi',
			'ajax': {
				'url': '<?=$sa?>master-user/listTrustee/'+$('#TipeUser').val(),
				'dataSrc' : ''
			},
			'columns': [
				{title: 'Nama Menu', data: 'NamaNavigasi'},
				{title: 'Trustee', data: 'Trustee', "visible": false, "searchable": false},
				{title: 'Trustee', data: 'Trustee'},
			],
		});
	}
	function refreshTable() {
		table1.ajax.url('<?=$sa?>master-user/listTrustee/'+$('#TipeUser').val()).load(function (json ) {},false);
	}
</script>
