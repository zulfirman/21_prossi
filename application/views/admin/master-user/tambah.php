<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
					</h2>
				<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content">
						<form class="kt-form" method="post" action="<?=$cu?>" enctype="multipart/form-data" id="formsimpan">
							<?=inCsrf()?>
							<?php
							if(isset($_SESSION['fnotif'])){notifSukses($_SESSION['fnotif'],'success');}
							?>
						<div class="form-row">
							<!--<div class="col-md-6 mb-4">
								<label class="form-label" for="Username">Username<span class="text-danger">*</span> </label>
								<select class="form-control" name="Username" id="Username" required>
									<option value="">-</option>
									<?php /*foreach ($listNama as $listNamav){*/?>
										<option value="<?/*=$listNamav['Username']*/?>"><?/*=$listNamav['Username']*/?> | <?/*=$listNamav['Tipe']*/?></option>
									<?php /*} */?>
								</select>
							</div>-->
							<div class="col-md-6 mb-4">
								<label class="form-label" for="Username">Email<span class="text-danger">*</span></label>
								<input name="Username" id="Username" type="email" class="form-control" required>
							</div>
							<div class="col-md-6 mb-6">
								<label for="MainImage">Gambar (Rekomendasi Pixel 300 x 300 px)<span class="text-danger">*</span></label>
								<input type="file" value="" id="MainImage" name="MainImage" required class="form-control mb-1">
								<span class="help-block font-weight-bold color-primary-800">Max File Size 2MB.</span>
							</div>
							<div class="col-md-6 mb-4">
								<label class="form-label" for="NamaUser">Nama User <span class="text-danger">*</span></label>
								<input name="NamaUser" id="NamaUser" type="text" class="form-control" required>
							</div>
							<div class="col-md-6 mb-4">
								<label class="form-label" for="Password">Password<span class="text-danger">*</span></label>
								<input name="Password" id="Password" type="password" class="form-control" required>
							</div>
							<div class="col-md-6 mb-4">
								<label class="form-label" for="KodeKategoriUser">Kategori User<span class="text-danger">*</span> </label>
								<select class="form-control" name="KodeKategoriUser" id="KodeKategoriUser" required>
									<option value="">-</option>
									<?php foreach ($listKategoriUser as $listKategoriUserv){?>
										<option value="<?=$listKategoriUserv->KodeKategoriUser?>"><?=$listKategoriUserv->NamaKategoriUser?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-6 mb-6">
								<label class="form-label" for="Pekerjaan">Pekerjaan</label>
								<input class="form-control" id="Pekerjaan" name="Pekerjaan" type="text" value="" required>
							</div>
							<div class="col-md-6 mb-6">
								<label class="form-label" for="AboutMeId">Tentang Saya Id</label>
								<textarea id="AboutMeId" name="AboutMeId" rows="3" required class=" form-control"></textarea>
							</div>
							<div class="col-md-6 mb-6" hidden>
								<label class="form-label" for="AboutMeEn">Tentang Saya En</label>
								<textarea id="AboutMeEn" name="AboutMeEn" rows="3" class=" form-control"></textarea>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-12 mb-4" style="text-align: center;">
								<a href="./" type="button" class="btn btn-default waves-effect waves-themed">Kembali</a>&nbsp
								<button type="submit" class="btn btn-primary waves-effect waves-themed">Simpan</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	var table1='';
	$(document).ready(function () {
		/*$('#Username').select2({
			placeholder: "-Pilih",
			allowClear: true,
			tags : true,
			minimumInputLength : 3
		});*/
		// select all
		/*$('#checkAll').on('click',function(){
			if(this.checked){
				$('.listTrustee').each(function(){
					this.checked = true;
				});
			}else{
				$('.listTrustee').each(function(){
					this.checked = false;
				});
			}
		});
		$('.listTrustee').on('click',function(){
			if($('.listTrustee:checked').length === $('.listTrustee').length){
				$('#checkAll').prop('checked',true);
			}else{
				$('#checkAll').prop('checked',false);
			}
		});*/
		//
		$('#Trustee').select2({placeholder: "-Pilih"});
		$("#pilihSemua").click(function () {
			if ($("#pilihSemua").is(':checked')) { //select all
				$("#Trustee").find('option').prop("selected", true);
				$("#Trustee").trigger('change');
			} else { //deselect all
				$("#Trustee").find('option').prop("selected", false);
				$("#Trustee").trigger('change');
			}
		});
	});
</script>
