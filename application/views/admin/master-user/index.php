<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?>
					</h2>
					<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content table-responsive">
						<?php
						if(isset($_SESSION['fnotif'])){notifSukses($_SESSION['fnotif'],'success');}
						?>
						<!-- datatable start -->
						<p class="text-align-right" style="text-align: right">
							<a href="<?=$cu."/tambah"?>" type="button" class="btn btn-primary waves-effect waves-themed">Tambah</a>
						</p>
						<table id="table1" class="table table-bordered table-hover table-striped w-100">
							<thead>

							</thead>
							<tbody>
							</tbody>
						</table>
						<!-- datatable end -->
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	var table1='';
	$(document).ready(function () {
		initTable();
	});
	function initTable(){
		if(table1!==''){
			table1.ajax.reload(null,false);
		}
		else{
			table1= $('#table1').DataTable({
				initComplete: function() {
					setTimeout(function () {
						table1.on('page.dt', function() {
							$('html, body').animate({
								scrollTop: $(".dataTables_wrapper").offset().top
							}, 'slow');
						});
					},500);
				},
				'pageLength': 25,
				'scrollCollapse': true,
				processing: true,
				orderCellsTop: true,
				responsive: true,
				"ajax": {
					"url": '<?=$sa?>master-user/getData',
					type: "POST",
					"data": function (d) {
						d.KodeKategoriUser = $('#KodeKategoriUser').val();
					},
					'dataSrc': ''
				},
				"columns": [
					{ title: 'Username', data: 'User'},
					{ title: 'Nama User', data: 'Nama'},
					{ title: 'Kategori User', data: 'NamaKategoriUser'},
					{title: 'Gambar',
						render: function (cellData, type, row) {
							return `<img src="<?=$linkUpload?>${row.MainImage}" alt="" style="width: 100px;height: 100px">`;
						}
					},
					{ title: 'Created', data: 'Ct'},
					{ title: 'Updated', data: 'Up'},
					{ title: 'Aksi',
						render: function (cellData, type, row) {
						console.log(row.A_I!=="farizan");
						if(row.A_I==="farizan"||row.A_I==="zul") {
							cellData='';
						}else{
							cellData = `<a href="<?=$cu . "/edit/"?>${row.A_I}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"
											 class="btn btn-info btn-icon rounded-circle waves-effect waves-themed">
										<i class="fal fa-edit"></i>
									</a>
									<button onclick="doDelete('m_user','KodeUser','${row.A_I}', 1,'${row.MainImage}')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
											class="btn btn-danger btn-icon rounded-circle waves-effect waves-themed">
										<i class="fal fa-trash"></i>
									</button>
							`;
						}
							return cellData;
						}
					},
				],
				"order": [[ 0, "asc" ]],
			});
		}
	}
</script>
