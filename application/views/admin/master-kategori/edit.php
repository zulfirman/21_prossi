<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
					</h2>
				<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content">
						<form class="kt-form" method="post" action="<?=$cu?>" enctype="multipart/form-data">
							<?=inCsrf()?>
							<div class="form-row">
								<input name="KodeKategori" id="KodeKategori" type="text" value="<?=$val->KodeKategori?>" class="form-control" required hidden>
								<?php if($this->uri->segment(2)==="kategori-service"){?>
									<div class="col-md-12 mb-6">
										<label class="form-label" for="MainImage">Gambar (Rekomendasi Pixel 300 x 300 px)<span class="text-danger">*</span></label>
										<div><img class="mb-4" src="<?=cet("$linkUpload".$val->MainImage) ?>" style="height: 100px; width:100px"></div>
										<input type="file" value="" id="MainImage" name="MainImage" class="form-control">
										<span class="help-block font-weight-bold color-primary-800">Max File Size 2MB.</span>
										<input hidden type="text" value="<?=$val->MainImage?>" class="form-control" id="OldImage" name="OldImage">
									</div>
								<?php } ?>
								<div class="col-md-6 mb-4">
									<label class="form-label" for="NamaKategoriId">Nama Kategori Id<span class="text-danger">*</span></label>
									<input name="NamaKategoriId" id="NamaKategoriId" type="text" value="<?=$val->NamaKategoriId?>" class="form-control" required>
								</div>
								<div class="col-md-6 mb-4" hidden>
									<label class="form-label" for="NamaKategoriEn">Nama Kategori En<span class="text-danger">*</span></label>
									<input name="NamaKategoriEn" id="NamaKategoriEn" type="text" value="<?=$val->NamaKategoriEn?>" class="form-control">
								</div>
								<?php if($this->uri->segment(2)==="kategori-service"){?>
									<div class="col-md-6 mb-4">
										<label class="form-label" for="DeskripsiId">Deskripsi Id<span class="text-danger">*</span></label>
										<input name="DeskripsiId" id="DeskripsiId" type="text" value="<?=$val->DeskripsiId?>" class="form-control" required>
									</div>
									<div class="col-md-6 mb-4" hidden>
										<label class="form-label" for="DeskripsiEn">Deskripsi En<span class="text-danger">*</span></label>
										<input name="DeskripsiEn" id="DeskripsiEn" type="text" value="<?=$val->DeskripsiEn?>" class="form-control">
									</div>
								<?php } ?>
								<div class="col-md-6 mb-4" hidden>
									<label class="form-label" for="IsService">Kategori Service ?<span class="text-danger">*</span></label>
									<select class="form-control" name="IsService" id="IsService" required>
										<?php if($this->uri->segment(2)==="kategori-service"){?>
											<option value="1">Ya</option>
										<?php }else{ ?>
											<option value="0">Tidak</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-4" style="text-align: center;">
									<a href="../" type="button" class="btn btn-default waves-effect waves-themed">Kembali</a>&nbsp
									<button type="submit" class="btn btn-primary waves-effect waves-themed">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	$('#IsService').val('<?=$val->IsService?>');
</script>
