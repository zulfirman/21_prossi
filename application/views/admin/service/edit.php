<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
					</h2>
					<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content">
						<form class="kt-form" method="post" action="<?=$cu?>" enctype="multipart/form-data">
							<?=inCsrf()?>
							<div class="row">
								<input hidden type="text" value="<?=$val->KodeArtikel?>" id="KodeArtikel" name="KodeArtikel" class="form-control">
								<input hidden type="text" value="<?=$val->MainImage?>" class="form-control" id="OldImage" name="OldImage">
								<div class="col-md-12 mb-6" hidden>
									<label for="MainImage">Gambar Utama (Rekomendasi Pixel 1440 x 810)</label>
									<br>
									<img class="mb-4" src="<?=$linkUpload.$val->MainImage?>" style="height: 238px; width:400px">
									<input type="file" value="" id="MainImage" name="MainImage" class="form-control mb-1">
									<span class="help-block font-weight-bold color-primary-800">Max File Size 2MB.</span>
								</div>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="JudulId">Judul Id<span class="text-danger">*</span></label>
									<input type="text" value="<?=$val->JudulId?>" id="JudulId" name="JudulId" required class="form-control">
								</div>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="ContentId">Konten Header<span class="text-danger">*</span></label>
									<input type="text" value="<?=$val->ContentId?>" id="ContentId" name="ContentId" required class="form-control">
								</div>
								<div class="col-md-6 mb-6" hidden>
									<label class="form-label" for="JudulEn">Judul En<span class="text-danger">*</span></label>
									<input type="text" value="<?=$val->JudulEn?>" id="JudulEn" name="JudulEn" class="form-control">
								</div>
								<div class="col-md-12 mb-6">
									<label for="Lainnya1">Gambar Content Atas (Rekomendasi Pixel 500 x 500)</label>
									<br>
									<img class="mb-4" src="<?=$linkUpload.$val->Lainnya1?>" style="height: 238px; width:238px">
									<input type="file" value="" id="Lainnya1" name="Lainnya1" class="form-control mb-1">
									<span class="help-block font-weight-bold color-primary-800">Max File Size 2MB.</span>
									<input hidden type="text" value="<?=$val->Lainnya1?>" class="form-control" id="OldImage1" name="OldImage1">
								</div>
								<div class="col-md-12 mb-6">
									<label class="form-label" for="Lainnya2">Konten Atas<span class="text-danger">*</span></label>
									<textarea id="Lainnya2" name="Lainnya2" rows="4" required class=" form-control"><?=$val->Lainnya2?></textarea>
								</div>
								<div class="col-md-12 mb-6">
									<label for="Lainnya3">Gambar Content Bawah (Rekomendasi Pixel 500 x 500)</label>
									<br>
									<img class="mb-4" src="<?=$linkUpload.$val->Lainnya3?>" style="height: 238px; width:238px">
									<input type="file" value="" id="Lainnya3" name="Lainnya3" class="form-control mb-1">
									<span class="help-block font-weight-bold color-primary-800">Max File Size 2MB.</span>
									<input hidden type="text" value="<?=$val->Lainnya3?>" class="form-control" id="OldImage3" name="OldImage3">
								</div>
								<div class="col-md-12 mb-6">
									<label class="form-label" for="Lainnya4">Konten Bawah<span class="text-danger">*</span></label>
									<textarea id="Lainnya4" name="Lainnya4" rows="4" required class=" form-control"><?=$val->Lainnya4?></textarea>
								</div>
								<div class="col-md-12 mb-6" hidden>
									<label class="form-label" for="ContentEn">Konten En<span class="text-danger">*</span></label>
									<textarea id="ContentEn" name="ContentEn" rows="4" class=" form-control"><?=$val->ContentEn?></textarea>
								</div>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="KodeKategori">Kategori<span class="text-danger">*</span></label>
									<select id="KodeKategori" name="KodeKategori" required class="form-control  kt-select2">
										<option value="">-</option>
										<?php foreach ($listKategori as $m){?>
											<option value="<?=$m->KodeKategori?>"><?=$m->NamaKategoriParent?> | <?=$m->NamaKategoriId?></option>
										<?php }?>
									</select>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-4" style="text-align: center;">
									<a href="../" type="button" class="btn btn-default waves-effect waves-themed">Kembali</a>&nbsp
									<button type="submit" class="btn btn-primary waves-effect waves-themed">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
		CKEDITOR.replace('Lainnya2',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		CKEDITOR.replace('Lainnya4',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		CKEDITOR.replace('ContentEn',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		$('#KodeKategori').val("<?=$val->KodeKategori?>");
	});
</script>
