<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?>
					</h2>
				</div>
				<div class="panel-container show">
					<div class="panel-content table-responsive">
						<form class="kt-form" method="post" action="#" enctype="multipart/form-data">
							<div class="form-row">
								<div class="col-sm-6">
									<div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
										<div class="">
											<h3 class="display-4 d-block l-h-n m-0 fw-500">
												<p>
													<?php
													$this->db->select('count(KodeArtikel) as Total');
													$this->db->where('Tipe', 1);
													$countTbl=$this->db->get('t_artikel', 1)->first_row();
													echo $countTbl->Total;
													?>
												</p>
												<small class="m-0 l-h-n">Total Artikel</small>
											</h3>
										</div>
										<i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
										<div class="">
											<h3 class="display-4 d-block l-h-n m-0 fw-500">
												<p>
													<?php
													$this->db->select('count(KodeKategori) as Total');
													$this->db->where('IsService', 1);
													$countTbl=$this->db->get('m_kategori', 1)->first_row();
													echo $countTbl->Total;
													?>
												</p>
												<small class="m-0 l-h-n">Total Pelayanan</small>
											</h3>
										</div>
										<i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
										<div class="">
											<h3 class="display-4 d-block l-h-n m-0 fw-500">
												<p>
													<?php
													$this->db->select('count(KodeGambar) as Total');
													$this->db->where('Tipe', 3);
													$countTbl=$this->db->get('t_gambar', 1)->first_row();
													echo $countTbl->Total;
													?>
												</p>
												<small class="m-0 l-h-n">Total Dokter</small>
											</h3>
										</div>
										<i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
										<div class="">
											<h3 class="display-4 d-block l-h-n m-0 fw-500">
												<p>
													<?php
													$this->db->select('count(KodeArtikel) as Total');
													$this->db->where('Tipe', 2);
													$countTbl=$this->db->get('t_artikel', 1)->first_row();
													echo $countTbl->Total;
													?>
												</p>
												<small class="m-0 l-h-n">Total Promo</small>
											</h3>
										</div>
										<i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
									</div>
								</div>
								<div class="col-md-12">
									<center>
									<a href="<?=$si?>assets/user-guide.pdf" target="_blank" type="button" class="btn btn-primary waves-effect waves-themed ddsb">Download User Guide</a>
									</center>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-4" style="text-align: center;">
.
								 </div>
								<div class="col-md-12 mb-4" style="text-align: center;">
.
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	var tgl1='';
	var tgl2='';
	$(document).ready(function () {
		var start = moment();
		var end = moment();
		$('#Tanggal').daterangepicker({
			drops: "auto",
			locale: {
				format: 'DD-MM-YYYY'
			},
			startDate: start,
			endDate: end,
			ranges: {
				'Hari Ini': [moment(), moment()],
				'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
				'30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
				'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
				'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, setTgl);
		function setTgl(start, end) {
			console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
			tgl1=start.format('YYYY-MM-DD');
			tgl2=end.format('YYYY-MM-DD');
		}
		setTgl(start, end);
		$('#KodeCabang').select2({
			placeholder: "-Pilih",
			allowClear: true
		});
		$("#checkAll").click(function(){
			if($("#checkAll").is(':checked') ){ //select all
				$("#KodeCabang").find('option').prop("selected",true);
				$("#KodeCabang").trigger('change');
			} else { //deselect all
				$("#KodeCabang").find('option').prop("selected",false);
				$("#KodeCabang").trigger('change');
			}
		});
		$('#checkAll').click();
	});
	function isiWidget() {
		if($('#KodeCabang').val().length>0) {
			$.ajax({
				url: '<?=$sa?>/dashboard/get-data',
				type: "post",
				data: {
					tgl1: tgl1,
					tgl2: tgl2,
					KodeCabang: $('#KodeCabang').val()
				},
				success: function (data) {
					$('#totalTransaksi').html('Rp. '+tsep(data.TotalTransaksi));
					$('#totalJumbel').html(tsep(data.TotalJumbel));
					$('#totalPenjualan').html(tsep(data.TotalPenjualan));
					$('#totalCustomer').html(tsep(data.TotalCustomer));
				},
			});
		}
		else{
			$('#totalTransaksi').html(0);
			$('#totalJumbel').html(0);
		}
	}
</script>
