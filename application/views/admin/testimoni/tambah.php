<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
					</h2>
					<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content">
						<form class="kt-form" method="post" action="<?=$cu?>" enctype="multipart/form-data">
							<?=inCsrf()?>
							<?php
							if(isset($_SESSION['fnotif'])){notifSukses($_SESSION['fnotif'],'success');}
							?>
							<div class="form-row">
								<div class="col-md-6 mb-4">
									<label class="form-label" for="MainImage">Gambar (Rekomendasi Pixel 200 x 200)</label>
									<input type="file" value="" id="MainImage" name="MainImage" required class="form-control mb-1">
									<span class="help-block font-weight-bold color-primary-800">Max File Size 2MB.</span>
								</div>
								<div class="col-md-6 mb-4">
									<label class="form-label" for="Lainnya1">Nama Klien</label>
									<input name="Lainnya1" id="Lainnya1" type="text" value="" class="form-control" required>
								</div>
								<div class="col-md-6 mb-4">
									<label class="form-label" for="Lainnya2">Klien</label>
									<input name="Lainnya2" id="Lainnya2" type="text" value="" class="form-control" required>
								</div>
								<div class="col-md-6 mb-4">
									<label class="form-label" for="Lainnya3">Testimoni</label>
									<textarea id="TentangId" name="Lainnya3" rows="3" required class=" form-control"></textarea>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-4" style="text-align: center;">
									<a href="./" type="button" class="btn btn-default waves-effect waves-themed">Kembali</a>&nbsp
									<button type="submit" class="btn btn-primary waves-effect waves-themed">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
</script>
