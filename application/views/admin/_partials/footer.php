<!-- this overlay is activated only when mobile menu is triggered -->
<div class="page-content-overlay" data-action="toggle" data-class="mobile-nav-on">
</div> <!-- END Page Content -->
<!-- BEGIN Page Footer -->
<footer class="page-footer" role="contentinfo">
	<div class="d-flex align-items-center flex-1 text-muted">
		<span class="hidden-md-down fw-700"><?=date('Y')?> © <?=$pe->NamaWebsite?></span>
	</div>
	<div hidden>
		<ul class="list-table m-0">
			<li><a href="intel_introduction.html" class="text-secondary fw-700">About</a></li>
			<li class="pl-3"><a href="info_app_licensing.html" class="text-secondary fw-700">License</a></li>
			<li class="pl-3"><a href="info_app_docs.html" class="text-secondary fw-700">Documentation</a></li>
			<li class="pl-3 fs-xl"><a href="https://wrapbootstrap.com/user/MyOrange" class="text-secondary" target="_blank"><i class="fal fa-question-circle" aria-hidden="true"></i></a></li>
		</ul>
	</div>
</footer>
<!-- END Page Footer -->
<!-- BEGIN Color profile -->
<!-- this area is hidden and will not be seen on screens or screen readers -->
<!-- we use this only for CSS color refernce for JS stuff -->
<p id="js-color-profile" class="d-none">
	<span class="color-primary-50"></span>
	<span class="color-primary-100"></span>
	<span class="color-primary-200"></span>
	<span class="color-primary-300"></span>
	<span class="color-primary-400"></span>
	<span class="color-primary-500"></span>
	<span class="color-primary-600"></span>
	<span class="color-primary-700"></span>
	<span class="color-primary-800"></span>
	<span class="color-primary-900"></span>
	<span class="color-info-50"></span>
	<span class="color-info-100"></span>
	<span class="color-info-200"></span>
	<span class="color-info-300"></span>
	<span class="color-info-400"></span>
	<span class="color-info-500"></span>
	<span class="color-info-600"></span>
	<span class="color-info-700"></span>
	<span class="color-info-800"></span>
	<span class="color-info-900"></span>
	<span class="color-danger-50"></span>
	<span class="color-danger-100"></span>
	<span class="color-danger-200"></span>
	<span class="color-danger-300"></span>
	<span class="color-danger-400"></span>
	<span class="color-danger-500"></span>
	<span class="color-danger-600"></span>
	<span class="color-danger-700"></span>
	<span class="color-danger-800"></span>
	<span class="color-danger-900"></span>
	<span class="color-warning-50"></span>
	<span class="color-warning-100"></span>
	<span class="color-warning-200"></span>
	<span class="color-warning-300"></span>
	<span class="color-warning-400"></span>
	<span class="color-warning-500"></span>
	<span class="color-warning-600"></span>
	<span class="color-warning-700"></span>
	<span class="color-warning-800"></span>
	<span class="color-warning-900"></span>
	<span class="color-success-50"></span>
	<span class="color-success-100"></span>
	<span class="color-success-200"></span>
	<span class="color-success-300"></span>
	<span class="color-success-400"></span>
	<span class="color-success-500"></span>
	<span class="color-success-600"></span>
	<span class="color-success-700"></span>
	<span class="color-success-800"></span>
	<span class="color-success-900"></span>
	<span class="color-fusion-50"></span>
	<span class="color-fusion-100"></span>
	<span class="color-fusion-200"></span>
	<span class="color-fusion-300"></span>
	<span class="color-fusion-400"></span>
	<span class="color-fusion-500"></span>
	<span class="color-fusion-600"></span>
	<span class="color-fusion-700"></span>
	<span class="color-fusion-800"></span>
	<span class="color-fusion-900"></span>
</p>
<!-- END Color profile -->
</div>
</div>
</div>
<!-- END Page Wrapper -->
<!-- BEGIN Quick Menu -->
<!-- to add more items, please make sure to change the variable '$menu-items: number;' in your _page-components-shortcut.scss -->
<nav class="shortcut-menu d-none d-sm-block">
	<input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
	<label for="menu_open" class="menu-open-button ">
		<span class="app-shortcut-icon d-block"></span>
	</label>
	<a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
		<i class="fal fa-arrow-up"></i>
	</a>
	<a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
		<i class="fal fa-expand"></i>
	</a>
	<a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
		<i class="fal fa-print"></i>
	</a>
</nav>
<!-- END Quick Menu -->
<!-- BEGIN Page Settings -->
<div class="modal fade js-modal-settings modal-backdrop-transparent" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-right modal-md">
		<div class="modal-content">
			<div class="dropdown-header bg-trans-gradient d-flex justify-content-center align-items-center w-100">
				<h4 class="m-0 text-center color-white">
					Layout Settings
					<small class="mb-0 opacity-80">User Interface Settings</small>
				</h4>
				<button type="button" class="close text-white position-absolute pos-top pos-right p-2 m-1 mr-2" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body p-0">
				<div class="settings-panel">
					<div class="mt-4 d-table w-100 px-5">
						<div class="d-table-cell align-middle">
							<h5 class="p-0">
								App Layout
							</h5>
						</div>
					</div>
					<div class="mt-2 d-table w-100 pl-5 pr-3">
						<div class="d-table-cell align-middle">
							<h5 class="p-0">
								Theme colors
							</h5>
						</div>
					</div>
					<div class="expanded theme-colors pl-5 pr-3">
						<!--<ul class="m-0">
							<li>
								<a href="#" id="myapp-0" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/" data-toggle="tooltip" data-placement="top" title="Wisteria (base css)" data-original-title="Wisteria (base css)"></a>
							</li>
							<li>
								<a href="#" id="myapp-1" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-1.css" data-toggle="tooltip" data-placement="top" title="Tapestry" data-original-title="Tapestry"></a>
							</li>
							<li>
								<a href="#" id="myapp-2" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-2.css" data-toggle="tooltip" data-placement="top" title="Atlantis" data-original-title="Atlantis"></a>
							</li>
							<li>
								<a href="#" id="myapp-3" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-3.min.css" data-toggle="tooltip" data-placement="top" title="Indigo" data-original-title="Indigo"></a>
							</li>
							<li>
								<a href="#" id="myapp-4" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-4.css" data-toggle="tooltip" data-placement="top" title="Dodger Blue" data-original-title="Dodger Blue"></a>
							</li>
							<li>
								<a href="#" id="myapp-5" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-5.css" data-toggle="tooltip" data-placement="top" title="Tradewind" data-original-title="Tradewind"></a>
							</li>
							<li>
								<a href="#" id="myapp-6" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-6.css" data-toggle="tooltip" data-placement="top" title="Cranberry" data-original-title="Cranberry"></a>
							</li>
							<li>
								<a href="#" id="myapp-7" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-7.css" data-toggle="tooltip" data-placement="top" title="Oslo Gray" data-original-title="Oslo Gray"></a>
							</li>
							<li>
								<a href="#" id="myapp-8" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-8.css" data-toggle="tooltip" data-placement="top" title="Chetwode Blue" data-original-title="Chetwode Blue"></a>
							</li>
							<li>
								<a href="#" id="myapp-9" data-action="theme-update" data-themesave data-theme="<?/*=$si*/?>assets/xdfxcd/css/cust-theme-9.min.css" data-toggle="tooltip" data-placement="top" title="Apricot" data-original-title="Apricot"></a>
							</li>
							<li>
								<a href="#" id="myapp-10" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-10.css" data-toggle="tooltip" data-placement="top" title="Blue Smoke" data-original-title="Blue Smoke"></a>
							</li>
							<li>
								<a href="#" id="myapp-11" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-11.css" data-toggle="tooltip" data-placement="top" title="Green Smoke" data-original-title="Green Smoke"></a>
							</li>
							<li>
								<a href="#" id="myapp-12" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-12.css" data-toggle="tooltip" data-placement="top" title="Wild Blue Yonder" data-original-title="Wild Blue Yonder"></a>
							</li>
							<li>
								<a href="#" id="myapp-13" data-action="theme-update" data-themesave data-theme="<?/*=$sia*/?>assets/xdfxcd/css/themes/cust-theme-13.css" data-toggle="tooltip" data-placement="top" title="Emerald" data-original-title="Emerald"></a>
							</li>
						</ul>-->
					</div>
					<div><!--other setting-->
					<div class="list" id="fh">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="header-function-fixed"></a>
						<span class="onoffswitch-title">Fixed Header</span>
						<span class="onoffswitch-title-desc">header is in a fixed at all times</span>
					</div>
					<div class="list" id="nff">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="nav-function-fixed"></a>
						<span class="onoffswitch-title">Fixed Navigation</span>
						<span class="onoffswitch-title-desc">left panel is fixed</span>
					</div>
					<div class="list" id="nfm">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="nav-function-minify"></a>
						<span class="onoffswitch-title">Minify Navigation</span>
						<span class="onoffswitch-title-desc">Skew nav to maximize space</span>
					</div>
					<div class="list" id="nfh">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="nav-function-hidden"></a>
						<span class="onoffswitch-title">Hide Navigation</span>
						<span class="onoffswitch-title-desc">roll mouse on edge to reveal</span>
					</div>
					<div class="list" id="nft">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="nav-function-top"></a>
						<span class="onoffswitch-title">Top Navigation</span>
						<span class="onoffswitch-title-desc">Relocate left pane to top</span>
					</div>
					<div class="list" id="mmb">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-main-boxed"></a>
						<span class="onoffswitch-title">Boxed Layout</span>
						<span class="onoffswitch-title-desc">Encapsulates to a container</span>
					</div>
					<div class="expanded">
						<ul class="">
							<li>
								<div class="bg-fusion-50" data-action="toggle" data-class="mod-bg-1"></div>
							</li>
							<li>
								<div class="bg-warning-200" data-action="toggle" data-class="mod-bg-2"></div>
							</li>
							<li>
								<div class="bg-primary-200" data-action="toggle" data-class="mod-bg-3"></div>
							</li>
							<li>
								<div class="bg-success-300" data-action="toggle" data-class="mod-bg-4"></div>
							</li>
						</ul>
						<div class="list" id="mbgf">
							<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-fixed-bg"></a>
							<span class="onoffswitch-title">Fixed Background</span>
						</div>
					</div>
					<div class="mt-4 d-table w-100 px-5">
						<div class="d-table-cell align-middle">
							<h5 class="p-0">
								Mobile Menu
							</h5>
						</div>
					</div>
					<div class="list" id="nmp">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="nav-mobile-push"></a>
						<span class="onoffswitch-title">Push Content</span>
						<span class="onoffswitch-title-desc">Content pushed on menu reveal</span>
					</div>
					<div class="list" id="nmno">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="nav-mobile-no-overlay"></a>
						<span class="onoffswitch-title">No Overlay</span>
						<span class="onoffswitch-title-desc">Removes mesh on menu reveal</span>
					</div>
					<div class="list" id="sldo">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="nav-mobile-slide-out"></a>
						<span class="onoffswitch-title">Off-Canvas <sup>(beta)</sup></span>
						<span class="onoffswitch-title-desc">Content overlaps menu</span>
					</div>
					<div class="mt-4 d-table w-100 px-5">
						<div class="d-table-cell align-middle">
							<h5 class="p-0">
								Accessibility
							</h5>
						</div>
					</div>
					<div class="list" id="mbf">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-bigger-font"></a>
						<span class="onoffswitch-title">Bigger Content Font</span>
						<span class="onoffswitch-title-desc">content fonts are bigger for readability</span>
					</div>
					<div class="list" id="mhc">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-high-contrast"></a>
						<span class="onoffswitch-title">High Contrast Text (WCAG 2 AA)</span>
						<span class="onoffswitch-title-desc">4.5:1 text contrast ratio</span>
					</div>
					<div class="list" id="mcb">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-color-blind"></a>
						<span class="onoffswitch-title">Daltonism <sup>(beta)</sup> </span>
						<span class="onoffswitch-title-desc">color vision deficiency</span>
					</div>
					<div class="list" id="mpc">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-pace-custom"></a>
						<span class="onoffswitch-title">Preloader Inside</span>
						<span class="onoffswitch-title-desc">preloader will be inside content</span>
					</div>
					<div class="mt-4 d-table w-100 px-5">
						<div class="d-table-cell align-middle">
							<h5 class="p-0">
								Global Modifications
							</h5>
						</div>
					</div>
					<div class="list" id="mcbg">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-clean-page-bg"></a>
						<span class="onoffswitch-title">Clean Page Background</span>
						<span class="onoffswitch-title-desc">adds more whitespace</span>
					</div>
					<div class="list" id="mhni">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-hide-nav-icons"></a>
						<span class="onoffswitch-title">Hide Navigation Icons</span>
						<span class="onoffswitch-title-desc">invisible navigation icons</span>
					</div>
					<div class="list" id="dan">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-disable-animation"></a>
						<span class="onoffswitch-title">Disable CSS Animation</span>
						<span class="onoffswitch-title-desc">Disables CSS based animations</span>
					</div>
					<div class="list" id="mhic">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-hide-info-card"></a>
						<span class="onoffswitch-title">Hide Info Card</span>
						<span class="onoffswitch-title-desc">Hides info card from left panel</span>
					</div>
					<div class="list" id="mlph">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-lean-subheader"></a>
						<span class="onoffswitch-title">Lean Subheader</span>
						<span class="onoffswitch-title-desc">distinguished page header</span>
					</div>
					<div class="list" id="mnl">
						<a href="#" onclick="return false;" class="btn btn-switch" data-action="toggle" data-class="mod-nav-link"></a>
						<span class="onoffswitch-title">Hierarchical Navigation</span>
						<span class="onoffswitch-title-desc">Clear breakdown of nav links</span>
					</div>
					<div class="list mt-1">
						<span class="onoffswitch-title">Global Font Size <small>(RESETS ON REFRESH)</small> </span>
						<div class="btn-group btn-group-sm btn-group-toggle my-2" data-toggle="buttons">
							<label class="btn btn-default btn-sm" data-action="toggle-swap" data-class="root-text-sm" data-target="html">
								<input type="radio" name="changeFrontSize"> SM
							</label>
							<label class="btn btn-default btn-sm" data-action="toggle-swap" data-class="root-text" data-target="html">
								<input type="radio" name="changeFrontSize" checked=""> MD
							</label>
							<label class="btn btn-default btn-sm" data-action="toggle-swap" data-class="root-text-lg" data-target="html">
								<input type="radio" name="changeFrontSize"> LG
							</label>
							<label class="btn btn-default btn-sm" data-action="toggle-swap" data-class="root-text-xl" data-target="html">
								<input type="radio" name="changeFrontSize"> XL
							</label>
						</div>
						<span class="onoffswitch-title-desc d-block mb-0">Change <strong>root</strong> font size to effect rem
                                    values</span>
					</div>
					<hr class="mb-0 mt-4">
					<div class="mt-2 d-table w-100 pl-5 pr-3">
						<div class="fs-xs text-muted p-2 alert alert-warning mt-3 mb-2">
							<i class="fal fa-exclamation-triangle text-warning mr-2"></i>The settings below uses localStorage to load
							the external CSS file as an overlap to the base css. Due to network latency and CPU utilization, you may
							experience a brief flickering effect on page load which may show the intial applied theme for a split
							second. Setting the prefered style/theme in the header will prevent this from happening.
						</div>
					</div>
					<hr class="mb-0 mt-4">
					</div>
					<div class="pl-5 pr-3 py-3 bg-faded">
						<div class="row no-gutters">
							<div class="col-6 pr-1">
								<a href="#" class="btn btn-outline-danger fw-500 btn-block" data-action="app-reset">Reset Settings</a>
							</div>
							<div class="col-6 pl-1">
								<a href="#" class="btn btn-danger fw-500 btn-block" data-action="factory-reset">Factory Reset</a>
							</div>
						</div>
					</div>
				</div> <span id="saving"></span>
			</div>
		</div>
	</div>
</div>
<!-- END Page Settings -->
<!-- base vendor bundle:
	 DOC: if you remove pace.min.js from core please note on Internet Explorer some CSS animations may execute before a page is fully loaded, resulting 'jump' animations
				+ pace.min.js (recommended)
				+ jquery.min.js (core)
				+ jquery-ui-cust.min.js (core)
				+ popper.min.js (core)
				+ bootstrap.min.js (core)
				+ slimscroll.min.js (extension)
				+ app.navigation.min.js (core)
				+ ba-throttle-debounce.min.js (core)
				+ waves.min.js (extension)
				+ smartpanels.min.js (extension)
				+ src/../jquery-snippets.min.js (core) -->
<script src="<?=$sia?>assets/xdfxcd/js/jquery.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/app.bundle.min.js"></script>
<!-- datatble responsive bundle contains:
+ jquery.dataTables.min.js
+ dataTables.bootstrap4.min.js
+ dataTables.autofill.min.js
+ dataTables.buttons.min.js
+ buttons.bootstrap4.min.js
+ buttons.html5.min.js
+ buttons.print.min.js
+ buttons.colVis.min.js
+ dataTables.colreorder.min.js
+ dataTables.fixedcolumns.min.js
+ dataTables.fixedheader.min.js
+ dataTables.keytable.min.js
+ dataTables.responsive.min.js
+ dataTables.rowgroup.min.js
+ dataTables.rowreorder.min.js
+ dataTables.scroller.min.js
+ dataTables.select.min.js
+ datatables.styles.app.min.js
+ datatables.styles.buttons.app.min.js -->
<script src="<?=$sia?>assets/xdfxcd/js/datagrid/datatables/datatables.bundle.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/notifications/sweetalert2/sweetalert2.bundle.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/formplugins/select2/select2.bundle.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/number-format-v0.0.2.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/vendor/print-this.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/formplugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/vendor/jquery-datatables-checkboxes-1.2.12/js/dataTables.checkboxes.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/dependency/moment/moment.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/datagrid/datatables/datatables.export.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/statistics/chartjs/chartjs.bundle.min.js"></script>
<!--<script src="<?/*=$si*/?>/assets/xdfxcd/js/dataTables.rowSpan.min.js"></script>-->
<script src="<?=$si?>assets/xdfxcd/vendor/ckeditor/ckeditor.js?v000.1"></script>
<script src="<?=$sia?>assets/xdfxcd/vendor/chartjs.min.js"></script>
<?php include APPPATH . 'views/admin/_partials/script-footer.php'; ?>
</body>
</html>
