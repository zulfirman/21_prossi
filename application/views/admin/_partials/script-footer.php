<script>
	var si='<?=$si?>';
	var sia='<?=$sia?>';
	var sa='<?=$sa?>';
	var cu='<?=$cu?>/';
	var urlUpload=si+'assets/uploads/';
	var usBro='';
	var kcFinderUrl="<?=$kcFinder?>";
	/* demo scripts for change table color */
	/* change background */
	fixBrokenImages = function( url ){
		var img = document.getElementsByTagName('img');
		var i=0, l=img.length;
		for(;i<l;i++){
			var t = img[i];
			if(t.naturalWidth === 0){
				//this image is broken
				t.src = url;
			}
		}
	};
	function ckChoose(input){
		return window.open(kcFinderUrl+'?CKEditor='+input+'&CKEditorFuncNum=3&langCode=en&mybrowse=1','popup','width=700,height=700');
	}
	function numKeypad(){
		var selector=$('.mcur');
		selector.attr("inputmode", "numeric");
		//selector.attr("pattern", "[0-9]*");
		selector.on('focus', function(e) {
			$(this).select();
		});
	}
	$(document).ready(function()
	{
		<?php if($focusMenu!==''){?>
		setTimeout(function () {
			$('#js-primary-nav').animate({ scrollTop: $('#<?=$focusMenu?>').offset().top }, 300);
		}, 334);
		<?php } ?>
		//$(document).on('focus', "input[class='mcur']", function () {
		$(".mcur").on('focus', function () {
			$(".mcur").on({
				keyup: function () {
					formatCurrency($(this));
				},
				blur: function () {
					formatCurrency($(this), "blur");
				}
			});
		});
		setTimeout(function () {
			$('.kt-select2').select2({
				placeholder: "Select-",
				allowClear: true,
			});
		}, 334);
		numKeypad();
		//fixBrokenImages('<?=$linkUpload?>default.jpg');
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {//cek user browser
			usBro='M';
		} else {
			usBro='K';
		}
		var element = new Image();
		Object.defineProperty(element, 'id', {
			get: function () {
				/* TODO */
				if(usBro==="K") {
					<?php
					if($GLOBALS['env']['projectType']==="production"){
					?>
					window.location.replace("<?=site_url('login/logout')?>");
					<?php
					}
					?>
				}
			}
		});
		console.log('%cHello', element);
		$("input").attr("autocomplete", "off");
		getCsr();
		setTimeout(function () {
			$('a').on('click', function(e) {
				if($(this).attr('href')!==undefined) {
					if ($(this).attr('href').indexOf("#") === -1) {
						if (!$(this).hasClass('ddsb')) {
							$(this).addClass('disabled');
						}
					}
					if (e.ctrlKey) {
						//is ctrl + click
					} else {
						//normal click
					}
				}
			});
		}, 500);
		$("form").submit(function (e) {
			// prevent duplicate form submissions
			var btn=$(this).find(":submit").hasClass('bsubmit');
			if(!btn) {
				$(this).find(":submit").attr('disabled', 'disabled');
				$(this).find(":submit").html('Menyimpan...');
			}
		});
		getNotif();
	});
	var controlsDate = {
		leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
		rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
	};
	function doDelete(namaTabel,namaId,id,isactive=1,gambar='') {
		Swal.fire({
			title: "Yakin Akan Hapus ?",
			type: "warning",
			reverseButtons: true,
			confirmButtonColor: '#428bca',
			showCancelButton: true,
			confirmButtonText: "Ya !",
			cancelButtonText: "Tidak !",
		}).then(function (result) {
			if (result.value) {
				$.post("<?=$si?>delete", {namaTabel:namaTabel,namaId:namaId,id:id,gambar:gambar,isactive:isactive},
					function (data, status) {
						window.location.reload();
					}
				);
			}
		});
	}
	function adjustTb(){ setTimeout(function(){$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();}, 100);}//adjust datatable
	function getCsr() {
		var csName='<?=$this->security->get_csrf_token_name()?>';//csfrn
		var csValue='<?=$this->security->get_csrf_hash()?>';//csrfv
		$.ajaxPrefilter(function (options, originalOptions, jqXHR) {//setting ajax
			if (originalOptions.data instanceof FormData) {
				originalOptions.data.append(csName, csValue);
			}
			else if (options.type.toLowerCase() === 'post') {
				options.data += '&'+csName+'='+csValue;
				if (options.data.charAt(0) === '&') {
					options.data = options.data.substr(1);
				}
			}
		});
		$.ajaxSetup({
			ajaxStart: function(){},
			beforeSend: function (result, status, xhr) {// sebelum send ajax
				Pace.restart();
				let bsubmit=$(".bsubmit");
				bsubmit.prop('disabled', true);
				if(bsubmit.text()==="Simpan") {
					bsubmit.html('Menyimpan...');
				}
			},
			complete: function (result, status, xhr) {// complete ajax
				let bsubmit=$(".bsubmit");
				bsubmit.prop('disabled', false);
				if(bsubmit.text()==="Menyimpan...") {
					bsubmit.html('Simpan');
				}
			},
			error: function (result, status, xhr) { //global error
				$('.modal').modal('hide');
				let bsubmit=$(".bsubmit");
				bsubmit.prop('disabled', false);
				if(bsubmit.text()==="Menyimpan...") {
					bsubmit.html('Simpan');
				}
				if(xhr!=='abort') {
					willRefresh(xhr);
				}
			}
		});
	}
	function willRefresh(xhr) {
		Swal.fire(
			{
				type: "error",
				title: "Oops...",
				html : "Terjadi Kesalahan! <br>"+xhr.toString(),
				confirmButtonText: 'Oke',
				reverseButtons: true,confirmButtonColor: '#428bca',
			}).then(function(result){
			<?php
			if($GLOBALS['env']['projectType']==="production"){
			?>
			location.reload();
			<?php
			}
			?>
		});
	}
	function limitKeypress(event, value, maxLength) {//limit panjang input
		if (value !== undefined && value.toString().length >= maxLength) {
			event.preventDefault();
		}
	}
	function tsep(b) {
		if(b!==null&&b!==undefined){
			return b.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}
		else{
			return 0;
		}
	}
	function isEmpty(str){// cek string kosong
		if(str===undefined) {
			return false;
		}
		if(str!==null) {
			str = str.toString();
		}
		return str === null || str.match(/^\s*$/) !== null;
	}
	var intVal = function ( i ) { //convert ke number
		return typeof i === 'string' ?
			parseInt(i.replace(/[^\d]/g, '')) :
			typeof i === 'number' ?
				i : 0;
	};
	var delayFunction = (function () {//delay function
		var timer = 0;
		return function (callback, ms) {
			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
	})();
	function customSelect() {
		$('.cs2').select2({
			placeholder: "-Pilih",
			allowClear: true,
		});
	}
	function customSelectTb() {
		$('.cs2tb').select2({
			placeholder: "Filter",
			allowClear: true,
		});
	}
	function cetakDiv() {
		document.getElementById("printDiv").style.display = "block";
		$("#printDiv").printThis({
			importCSS: false,            // import parent page css
			importStyle: false,         // import style tags,
		});
		setTimeout(function () {
			$('#printDiv').css('display', 'none');
		}, 334);
	}
	function relogNih(){
		$.ajax({
			url: si+'login/proses',
			type: "post",
			data: {
				relog: 1
			},
			success: function (data) {

			}
		});
	}
	function getNotif(){
		$.ajax({
			url: si + 'comment/getNotif',
			type: 'get',
			dataType: 'json',
			success: function (data) {
				var appendiv=$("#pNotification");
				appendiv.empty();
				var pTotalIsRead=0;
				$.each(data, function (index, val) {
					let backgroundColor='';
					if(val.IsOpen==="0"){
						backgroundColor='unread';
					}else{
						backgroundColor='';
					}
					if(val.IsRead==="0"){
						pTotalIsRead++;
					}
					var isihref='#';
					if(val.Type==="1"){
						isihref = si+'artikel/'+val.Idnya;
					}
					var urlImage=sia+'assets/xdfxcd/img/demo/avatars/avatar-m.png';
					var rowdata=`<li class="${backgroundColor}">
												<a href="${isihref}" class="d-flex align-items-center" target="_blank">
                                                            <span class="status mr-2">
                                                                <span class="profile-image rounded-circle d-inline-block" style="background-image:url('${urlImage}')"></span>
                                                            </span>
													<span class="d-flex flex-column flex-1 ml-1">
                                                                <span class="name">Komentar</span>
                                                                <span class="msg-a fs-sm">${val.Content}</span>
                                                                <span class="fs-nano text-muted mt-1">${val.DateAdded}</span>
                                                            </span>
												</a>
											</li>`;
					appendiv.append(rowdata);
				});
				if(pTotalIsRead===0){
					//$('#pTotalIsRead').css('display','none');
					$('#pCommentIcon').css('display','none');
				}
				else {
					$('#pTotalIsRead').css('display','block');
					$('#pCommentIcon').css('display','block');
				}
				$('#pTotalIsRead').html(`${pTotalIsRead}&nbspNew
									<small class="mb-0 opacity-80">User Notifications</small>`);
				$('#totalCommentIcon').html(pTotalIsRead);
			}
		});
	}
	function readNotif() {
		$.ajax({
			url: si + 'comment/readNotif',
			type: 'get',
			dataType: 'json',
			success: function (data) {
				getNotif();
			}
		});
	}
	function ckEditorOn(){
		return {
			instanceReady: function() {
				this.dataProcessor.htmlFilter.addRules( {
					elements: {
						img: function( el ) {
							// Add an attribute.
							/*if ( !el.attributes.alt )
								el.attributes.alt = 'An image';*/
							el.addClass( 'img-responsive lazy' );
							el.attributes.loading = 'lazy';
						}
					}
				} );
			}
		};
	}
</script>
