<?php
$focusMenu='';
foreach (listNavigasi() as $nav){
	if($nav->Url!==""){
		$liActive1 = '';
		if ($sa.$this->uri->segment(1).'/'.$this->uri->segment(2).'/' == $sa.$nav->Url.'/') {
			$liActive1 = 'active';
			$focusMenu='parentNav'.$nav->KodeNavigasi;
		}
		?>
		<li id="parentNav<?=$nav->KodeNavigasi?>" class="<?=$liActive1?>">
			<a href="<?=$si.$nav->Url?>" title="<?=$nav->NamaNavigasi?>" data-filter-tags="<?=strtolower($nav->NamaNavigasi)?>">
				<i class="<?=$nav->Icon?>"></i>
				<span class="nav-link-text" data-i18n="nav.ui_components"><?=$nav->NamaNavigasi?></span>
			</a>
		</li>
	<?php } else {?>
		<li id="parentNav<?=$nav->KodeNavigasi?>">
			<a href="#" title="<?=$nav->NamaNavigasi?>" data-filter-tags="<?=strtolower($nav->NamaNavigasi)?>">
				<i class="<?=$nav->Icon?>"></i>
				<span class="nav-link-text"><?=$nav->NamaNavigasi?></span>
			</a>
			<ul>
				<?php
				foreach ($nav->subNav as $navSub){
					$liActive2='';
					$parentOpen=0;
					if ($sa.$this->uri->segment(1).'/'.$this->uri->segment(2).'/' == $sa.$navSub->Url.'/') {
						$liActive2 = 'active';
						$focusMenu='parentSubNav'.$navSub->KodeNavigasi;
						$parentOpen = 1;
					}
					?>
					<li class="<?=$liActive2?>" id="parentSubNav<?=$navSub->KodeNavigasi?>">
						<a href="<?=$si.$navSub->Url?>" title="<?=$navSub->NamaNavigasi?>" data-filter-tags="<?=strtolower($nav->NamaNavigasi.' '.$navSub->NamaNavigasi)?>">
							<span class="nav-link-text"><?=$navSub->NamaNavigasi?></span>
						</a>
						<?php if($parentOpen===1){?>
							<script>
								document.getElementById('parentNav<?=$nav->KodeNavigasi?>').classList.add('active','open');
							</script>
						<?php } ?>
					</li>
				<?php } ?>
			</ul>
		</li>
	<?php } } ?>
