<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=ucwords(str_replace('-',' ',$this->uri->segment(3)))?>
					</h2>
					<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content">
						<form class="kt-form" method="post" action="<?=$cu?>" enctype="multipart/form-data">
							<?=inCsrf()?>
							<div class="row">
								<input hidden type="text" value="<?=$val->KodeArtikel?>" id="KodeArtikel" name="KodeArtikel" class="form-control">
								<input hidden type="text" value="<?=$val->MainImage?>" class="form-control" id="OldImage" name="OldImage">
								<div class="col-md-12 mb-6">
									<label for="MainImage">Gambar Utama (Rekomendasi Pixel 1440 x 810)</label>
									<br>
									<img class="mb-4" src="<?=$linkUpload.$val->MainImage?>" style="height: 238px; width:400px">
									<input type="file" value="" id="MainImage" name="MainImage" class="form-control mb-1">
									<span class="help-block font-weight-bold color-primary-800">Max File Size 2MB.</span>
								</div>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="JudulId">Judul Id<span class="text-danger">*</span></label>
									<input type="text" value="<?=$val->JudulId?>" id="JudulId" name="JudulId" required class="form-control">
								</div>
								<div class="col-md-6 mb-6" hidden>
									<label class="form-label" for="JudulEn">Judul En<span class="text-danger">*</span></label>
									<input type="text" value="<?=$val->JudulEn?>" id="JudulEn" name="JudulEn" class="form-control">
								</div>
								<div class="col-md-12 mb-6">
									<label class="form-label" for="ContentId">Konten Id<span class="text-danger">*</span></label>
									<textarea id="ContentId" name="ContentId" rows="4" required class=" form-control"><?=$val->ContentId?></textarea>
								</div>
								<div class="col-md-12 mb-6" hidden>
									<label class="form-label" for="ContentEn">Konten En<span class="text-danger">*</span></label>
									<textarea id="ContentEn" name="ContentEn" rows="4" class=" form-control"><?=$val->ContentEn?></textarea>
								</div>
								<div class="col-md-6 mb-6">
									<label class="form-label" for="KodeKategori">Kategori<span class="text-danger">*</span></label>
									<select id="KodeKategori" name="KodeKategori[]" required class="form-control  kt-select2" multiple="multiple">
										<?php foreach ($listKategori as $m){?>
											<option value="<?=$m->KodeKategori?>"><?=$m->NamaKategoriId?> | <?=$m->NamaKategoriEn?></option>
										<?php }?>
									</select>
								</div>
								<div class="col-md-6 mb-6" hidden>
									<label class="form-label" for="EnableComment">Aktifkan Komentar<span class="text-danger">*</span></label>
									<select id="EnableComment" name="EnableComment" required class="form-control">
										<option value="1">Ya</option>
										<option value="0">Tidak</option>
									</select>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-4" style="text-align: center;">
									<a href="../" type="button" class="btn btn-default waves-effect waves-themed">Kembali</a>&nbsp
									<button type="submit" class="btn btn-primary waves-effect waves-themed">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	$(document).ready(function () {
		CKEDITOR.replace('ContentId',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		CKEDITOR.replace('ContentEn',{
			filebrowserImageBrowseUrl : '<?=$kcFinder?>',
			on: ckEditorOn()
		});
		var KodeKategori=$('#KodeKategori');
		KodeKategori.val("<?=$val->KodeKategori?>");
		$('#EnableComment').val("<?=$val->EnableComment?>");
		var selectedValue=[];
		until = "<?=$val->KodeKategori?>".split(',');
		for(i=-1;i<until.length;i++) {
			selectedValue[i] = until[i];
		}
		KodeKategori.val(selectedValue);
		KodeKategori.select2().trigger("change");
	});
</script>
