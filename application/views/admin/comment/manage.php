<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<!-- the #js-page-content id is needed for some plugins to initialize -->
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
		<h1 class="subheader-title">
			<i class='subheader-icon fal fa-table'></i><?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?>
		</h1>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div id="panel-1" class="panel">
				<div class="panel-hdr">
					<h2>
						<?=ucwords(str_replace('-',' ',$this->uri->segment(2)??''))?> - <?=$artikel->JudulId?>
					</h2>
				<div class="panel-toolbar">
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
						<button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
					</div>
				</div>
				<div class="panel-container show">
					<div class="panel-content table-responsive">
						<?php
						if(isset($_SESSION['fnotif'])){notifSukses($_SESSION['fnotif'],'success');}
						?>
						<!-- datatable start -->
						<p class="text-align-right" style="text-align: right">
							<a href="<?=$cu."/tambah"?>" type="button" class="btn btn-primary waves-effect waves-themed">Tambah</a>
						</p>
						<table id="table1" class="table table-bordered table-hover table-striped w-100">
							<thead>
							<tr>
								<th>Nama</th>
								<th>Isi Komentar</th>
								<th>Tanggal</th>
								<th>Aksi</th>
							</tr>
							</thead>
							<tbody>
							<?php
							foreach ($listKomen as $val){
							?>
							<tr>
								<td><?=$val->Nama?></td>
								<td><?=$val->IsiKomen?></td>
								<td><?=$val->Tanggal?></td>
								<td>
									<button onclick="doDelete('<?=$val->tabel?>','<?=$val->kolom?>','<?=$val->KodeKomen?>', 0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
											class="btn btn-danger btn-icon rounded-circle waves-effect waves-themed">
										<i class="fal fa-trash"></i>
									</button>
								</td>
							</tr>
							<?php } ?>
							</tbody>
						</table>
						<!-- datatable end -->
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
<script>
	$('#table1').DataTable({
			responsive: true,
			'pageLength': 50,
			'order': [[0, 'desc']],
		});
</script>
