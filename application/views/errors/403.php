<?php
header("HTTP/1.0 403 No Acces");
?>
<?php include APPPATH . 'views/admin/_partials/header.php'; ?>
<main id="js-page-content" role="main" class="page-content">
	<div class="subheader">
	</div>
	<div class="h-alt-hf d-flex flex-column align-items-center justify-content-center text-center">
		<h1 class="page-error color-fusion-500">
			ERROR <span class="text-gradient">403</span>
			<small class="fw-500">
				Anda Tidak Mempunyai Akses Untuk Halaman Ini.
			</small>
		</h1>
		<h3 class="fw-500 mb-5">
			Hubungi Pemilik / Administrator Anda.
		</h3>
	</div>
</main>
<?php include APPPATH . 'views/admin/_partials/footer.php'; ?>
