<?php
$pe=getPengaturan();
$si=site_url();
$sia = $GLOBALS['env']['assetsUrl'];
$linkUpload=$si.'assets/uploads/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>
		<?=$pe->NamaWebsite?> - Login
	</title>
	<meta name="description" content="Login">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
	<!-- Call App Mode on ios devices -->
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<!-- Remove Tap Highlight on Windows Phone IE -->
	<meta name="msapplication-tap-highlight" content="no">
	<!-- base css -->
	<link rel="stylesheet" media="screen, print" href="<?=$sia?>assets/xdfxcd/css/vendors.bundle.min.css">
	<link rel="stylesheet" media="screen, print" href="<?=$sia?>assets/xdfxcd/css/app.bundle.min.css">
	<!-- Place favicon.ico in the root directory -->
	<link rel="apple-touch-icon" sizes="180x180" href="<?=$sia?>assets/xdfxcd/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?=$linkUpload.$pe->Icon?>">
	<link rel="mask-icon" href="<?=$sia?>assets/xdfxcd/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<!-- Optional: page related CSS-->
	<link rel="stylesheet" media="screen, print" href="<?=$sia?>assets/xdfxcd/css/fa-brands.min.css">
	<link rel="stylesheet" media="screen, print" href="	<?=$si?>assets/xdfxcd/css/cust-theme-9.min.css">
</head>
<body>
<div class="page-wrapper">
	<div class="page-inner bg-brand-gradient">
		<div class="page-content-wrapper bg-transparent m-0">
			<div class="height-10 w-100 shadow-lg px-4 bg-brand-gradient">
				<div class="d-flex align-items-center container p-0">
					<div class="page-logo width-mobile-auto m-0 align-items-center justify-content-center p-0 bg-transparent bg-img-none shadow-0 height-9">
						<a href="./" class="page-logo-link press-scale-down d-flex align-items-center">
							<img src="<?=$linkUpload.$pe->Logo?>" style="height: 50px" alt="SmartAdmin WebApp"
								 aria-roledescription="logo">
							<span class="page-logo-text mr-1"><?=$pe->NamaWebsite?></span>
							<h2 style="color:white;"></h2>
						</a>
					</div>
					<span class="text-white opacity-50 ml-auto mr-2 hidden-sm-down" hidden>
                                Belum Punya Akun ? Daftar Disini
                            </span>
					<a href="daftar" class="btn-link text-white ml-auto ml-sm-0" hidden>
						Daftar
					</a>
				</div>
			</div>
			<div class="flex-1"
				 style="background: url(assets/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;">
				<div class="container py-4 py-lg-5 my-lg-5 px-4 px-sm-0">
					<div class="row">
						<div class="col-xl-12">
							<h2 class="fs-xxl fw-500 mt-4 text-white text-center">
								Login
							</h2>
						</div>
						<div class="col-xl-6 ml-auto mr-auto">
							<div class="card p-4 rounded-plus bg-faded">
								<form id="js-login" role="form" action="<?=$si?>login/proses" method="post" novalidate>
									<?=inCsrf()?>
									<?php
									if(isset($_SESSION['fnotif'])){notifSukses($_SESSION['fnotif'],'danger');}
									?>
									<div class="row">
										<div class="form-group col-md-12">
											<label for="Username"
												   class="col-form-label" >Username</label>
											<input class="form-control" id="Username" name="Username" type="text" value="<?=$_SESSION['input']['Username']??''?>" required>
											<?php if(isset($_GET['KodeMeja'])){?>
											<input class="form-control" id="KodeMeja" name="KodeMeja" type="text" value="<?=$_GET['KodeMeja'];?>" hidden>
											<?php } ?>
										</div>
										<div class="form-group col-md-12">
											<label for="Password"
												   class="col-form-label">Password</label>
											<input class="form-control" id="Password" name="Password" type="Password" value="<?=$_SESSION['input']['Password']??''?>" required>
											<?php
											if(isset($_GET['redirect'])){
											?>
											<input class="form-control" id="redirect" name="redirect" type="text" value="<?=$_GET['redirect']?>" hidden readonly>
										<?php } ?>
										</div>
										<div class="form-group text-left col-md-12">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="rememberMe" name="rememberMe">
												<label class="custom-control-label" for="rememberMe"> Remember Me</label>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<button id="js-login-btn" type="submit"
												class="btn btn-block btn-danger btn-lg mt-3">Login
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="position-absolute pos-bottom pos-left pos-right p-3 text-center text-white">
					<?=date('Y')?> © <?=$pe->NamaWebsite?>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?=$sia?>assets/xdfxcd/js/jquery.min.js"></script>
<script src="<?=$sia?>assets/xdfxcd/js/app.bundle.min.js"></script>
<script>
	$(document).ready(function () {
		$('#Username').focus();
	});
	$("#js-login-btn").click(function (event) {
		// Fetch form to apply custom Bootstrap validation
		var form = $("#js-login");
		if (form[0].checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();
		}
		form.addClass('was-validated');
		// Perform ajax submit here...
	});
</script>
</body>
</html>
